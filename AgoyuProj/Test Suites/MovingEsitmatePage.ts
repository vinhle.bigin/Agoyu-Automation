<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>MovingEsitmatePage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T10:19:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>58aa855e-28cc-42c3-9003-bf849695e4fb</testSuiteGuid>
   <testCaseLink>
      <guid>def2398a-938a-4212-8a50-db82b4812053</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_001_Moving Estimate - Address View Mode - Value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8075124-2cb8-4524-b6e5-cbb4716acbdc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_002_Moving Estimate - Edit Address From - Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8d6a7531-7d65-40e1-8c5d-aab9bee43b35</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_003_Moving Estimate - Edit Address To - Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c8685980-2f49-420d-9b86-bc6469424c1e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_005 - Moving Estimate - Edit Address - Cancel button behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>421a53c3-1065-48fc-b18d-bad3ec256c6d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_006 - Moving Estimate - Edit Address - Submit is successful</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>51307bb2-6504-4a99-b37d-c15a89710a4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_007_Moving Estimate - Address View mode  - Edit link - behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>078a0e1d-f56c-4f8a-9cdd-a2e052cc32e1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_008_Moving Estimate - Address Edit mode  - Edit link - behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>21176a23-63c1-4958-99bd-1d5d9f2c2f0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_009_Moving Estimate - Address View mode  - Expand link - behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>eaa9c790-d328-4597-958d-95be65c1c18a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_010_Moving Estimate - Address Edit mode  - Expand link - behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>08dc854d-1d38-45d9-b4fa-ec2a2772de4a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_011_Moving Estimate - Address View mode  - Collapse link - behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0b1f2fe3-02c1-4859-8404-66f297b92454</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_012_Moving Estimate - Address Edit mode  - Collapse link - behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e421e46b-c10f-412e-99b6-69f8d768e4d1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_013_Moving Estimate - Weight View Mode - Value of Exact Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f81ec785-7429-4bec-9d86-5dc1b9151802</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_014_Moving Estimate - Weight View Mode - Value of Custom Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>372feefe-43d6-49c0-a4b5-2f6108e6b838</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_015_Moving Estimate - Weight Edit Mode - Value of Exact Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>17e361d0-a386-4446-9c8f-238bd8675e64</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_016_Moving Estimate - Weight Edit Mode - Value of Custom Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>db149c03-d8fb-4b3b-bd4e-cfd6bf403ab9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_017_Edit Moving Plan - Validation - Exact Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5ea91815-73a8-40d7-af30-1df4f74a2427</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_018_Edit Moving Plan - Exact Weight option- Behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc2c7d91-2c26-4278-8caf-5b5e36ae3c6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_019_Edit Moving Plan - Room List - Default value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8f92ec0f-5e72-41ca-984a-1ebf98ccda38</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/MovingEstimatePage/MET_020_Edit Moving Plan - Room List - Validation</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
