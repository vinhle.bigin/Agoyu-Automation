<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LoginLogoutAdmin</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T10:19:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>5d559fab-a499-4511-a23c-260b79f75589</testSuiteGuid>
   <testCaseLink>
      <guid>a90e124f-bc92-4770-b886-34d1fbaf2113</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Agoyu Portal/LoginLogoutAdmin/AA_001Verify admin can login successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>65c1891f-52b1-41c0-8915-61b535a812c6</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Agoyu Portal/LoginLogoutAdmin/AA_002_Verify Admin can logout from Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cab7a55-2d8d-49a0-92c1-c5f9f1a2bf74</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Agoyu Portal/LoginLogoutAdmin/AA_003_Verify user can go to Admin Profile after clicking Agoyu Portal menu</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0abd34c8-06f3-4cbb-a773-44c7767c49a7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/Agoyu Portal/LoginLogoutAdmin/AA_004_Verify Admin can logout from Agogy Portal page</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
