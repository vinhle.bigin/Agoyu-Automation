<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>ManageUser</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T10:19:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e0734136-23db-45f9-b76a-7dd82f8401e1</testSuiteGuid>
   <testCaseLink>
      <guid>0c763895-8e6a-4406-b2b0-c3f8ad560e00</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU001_AdminUser_Verify_Add_new_User_Func_works_correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7073a507-712f-46e3-a402-58f362cf5e34</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU002_AdminUser_Verify_Search_User_Func_works_correctly</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>5dd22b64-dfed-4c71-9aac-f96638e80aed</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>3a4b4af0-759a-43da-8b66-fb24d5842525</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU003_AdminUser_Verify_Edit_new_User_Func_works_correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>966868bd-5fe6-4149-bfc3-4bafaab00eca</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU004_AdminUser_Verify_Delete_User_Func_works_correctly</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f26fcef2-ea46-4686-a40c-4e590191dbc6</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>90854d5f-db54-45aa-a631-535c1f780728</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU005_AdminUser_Verify_Lock_Unlock_User_Func_works_correctly</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f42b5c98-49e1-421c-b96b-1c485b7f037b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e4ebd15f-5b3f-4089-89b9-a99c9800e5d9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU005_AdminUser_Verify_Lock_User_Func_works_correctly</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>432a6d88-3d7e-4a2b-bf7f-ac70de467c0f</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>77a2983b-d09f-491b-876e-389491d9e1de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU005_AdminUser_Verify_Unlock_User_Func_works_correctly</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>f42b5c98-49e1-421c-b96b-1c485b7f037b</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>4c4c81ee-c8a7-4c95-91ef-ba67cdba1615</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU006_AdminUser_Verify_User_Reset_2FA_Code_Func_works_correctly</testCaseId>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>3e2e7139-8b03-4239-b899-5be68fac814a</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>33ea60f1-dafd-4197-9603-3635c27e19b9</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/ManageUser/TC_MU011_AdminUser_Verify_Add_Price_functions_works_correctly</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
