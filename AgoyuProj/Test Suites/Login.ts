<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>Login</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T10:19:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>e9858e4c-b2f5-498d-b1cc-325a4ca973ff</testSuiteGuid>
   <testCaseLink>
      <guid>4056a516-802a-4886-a024-95dcc830c8c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_001_Validation - UserName</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cbab6e53-14da-40d9-907d-88d7af61a96b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_002_Validaton  - Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>acbb740f-f9a8-4f53-a7f6-2368f299757b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_003_Verify user can login successfully with valid account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f9a58d44-3cd3-468c-9fab-25efefd6720b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_004_Verify user can login with facebook account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4750d82e-c154-403a-a4de-1b121c5d7db0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_005_Verify user can login with gmail account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d08731eb-b0aa-4e58-9996-be5708c53356</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_006_Verify Menu for Personal Consumer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>13fce3d4-4803-4a35-9900-3e7dc91e94ee</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_007_Verify Menu for Business Consumer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9653b2f0-80e0-4aa3-8bfe-b6fa44e9acb7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_008_Verify Message after submit provide email for Forgot Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0069bae9-cb73-4f9b-9097-29a3e65cef90</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_009_Verify Email is sent for  Forgot Password</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8e0c659-3ab0-424b-bfa8-dacc137486c3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_010_Verify Reset Password  form shown when access with Reset link</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8309b5a6-0660-4c75-a2e1-bdc8cbda5432</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_011_Verify Reset Password  func work correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1390621d-a4a7-4eb5-a36d-3321dc3404fa</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/LG_012_Verify user cannot login with old pass after reset with new one</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c4bf1e8-758b-49af-8f81-76edb8b23750</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/TC_LG005_Normal User_Verify can login successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>95db4d2c-2320-4a1c-949f-deb93813737b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/Login/TC_LG006_Fortgot Password - Validation - Email</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
