<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>SignUp</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T10:19:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>9a41014a-4836-4475-aeac-19fdf7ad90ea</testSuiteGuid>
   <testCaseLink>
      <guid>b9341764-0da6-45a7-a07b-15bf11ebe57a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_001_Personal SignUp - Verify  Email validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>46cbfbda-cd43-4215-aab5-3fabdadaa002</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_002_Personal SignUp - Verify  Password validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e8b9489c-2d07-4f35-9c94-4b41bef0d26d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_003_Personal SignUp - Verify  Contact Name validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>04161a5b-c16e-494f-87e1-c6dab0799724</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_004_Personal SignUp - Verify  TermCondition validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>390b6246-0351-418f-934a-14ea87c4fa0d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_005 - Business SignUp - Verify  Email validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6b1e5580-b567-43ff-ab06-e6772c15af6c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_006_Business SignUp - Verify  Password validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8c613d61-c836-4e83-9aa5-c1e794f5ef5b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_007_Business SignUp - Verify  Contact Name validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>bc329ea1-ddac-42ce-8f22-dbef152965ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_008_Business SignUp - Verify  Business Name validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7ad5f93e-9280-495f-ae2b-737360e6101f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_009_Business SignUp - Verify  Logo validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ae8844dc-d953-4beb-b93c-4286bcddc0e5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_010_Business SignUp - Verify  TermCondition validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>00a5ea02-e56b-45f3-b5fb-e9a5e6e76084</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_011_Verify user can signup with facebook account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>4e0d7f11-cc33-4d4f-a361-c3016c6c7654</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_012_Verify user can signup with gmail account</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>b64269e1-0440-49d3-bb17-97c5a61fe197</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_013_Verify user can signup as personal consumer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5813c3fd-f603-43c8-9c8e-8eb45230b0ea</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_014_Verify user can signup as business consumer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>58412236-b1e3-4221-a348-3bf5abf220d4</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_015_Verify Activate Email is sent to Personal consumer when sign-up successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>23e27633-18b1-46cf-90ec-cd1b5155fafb</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_016_Verify success msg is shown after account is activated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>71cb8fa5-9272-4a51-a025-19b9fa50f277</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_017_Verify Congratulation  email is sent after account is activated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>20279d5d-f944-4455-937e-7677c6a41245</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_018_Verify user cannot login when account is not activated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d5c0e50b-0450-47d1-ad5e-26402c29d390</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_019_Verify user can login after account is activated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>40309eaa-f9d5-4d35-a5a1-906b19f938de</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_020_Verify msg is shown when user try to activate account again</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43da6da1-a619-45cf-8f78-5e4388c8d510</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_021_Verify activate link is expired after 72hrs of Signup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>605b976b-2695-4598-b5f9-05efcfeb1067</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_023_Verify Activate Email is sent to Business consumer when sign-up successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8a231d45-d67b-424a-939b-1085388fe9a5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_024_Verify Activate Email is sent to consumer when Facebook sign-up successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cd74c289-4cf5-47de-9373-16562c04a266</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/SU_025_Verify Activate Email is sent to consumer when Gmail sign-up successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>fb69bf85-4f13-4144-b937-6f3cce3cd7f0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Consumer/TC_SU001_SignUpGlobalUser</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>43373475-4734-4afd-8fc3-c768625d4cef</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_006_Business SignUp - Verify  Password validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8bd9c838-6b10-4c53-a117-576c1319759b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_007_Business SignUp - Verify  Contact Name validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>a024b4dc-d234-485e-9065-d6fb6ee01339</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_008_Business SignUp - Verify  Business Name validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>80d0493c-957c-437a-a8f2-e92669c15e20</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_010_Business SignUp - Verify  TermCondition validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>cf0b12f4-eaf0-4e1a-8f28-fcf5b47d3be7</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_014_Verify user can signup as business consumer</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1ddd3e9e-5ae5-4faf-8c91-3e70d5d40792</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_016_Verify success msg is shown after account is activated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e6989f9-9bdb-4d1d-89f8-baace2c3592d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_017_Verify Congratulation  email is sent after account is activated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>8cfe5a68-debd-4a4d-be32-e0d840b09b7c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_018_Verify user cannot login when account is not activated</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>c7f50c31-c0a7-4634-b679-e4aa37426a0a</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_019_Verify user can login after account is activated successfully</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>66ec556c-42f9-441a-8d1c-d5c1fed1506b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_020_Verify msg is shown when user try to activate account again</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>05a1caee-de76-416d-82fc-5413ea90e413</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_021_Verify activate link is expired after 72hrs of Signup</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>faa48e99-13a9-4a9f-8dd2-551be989e447</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>false</isRun>
      <testCaseId>Test Cases/TestCasesList/SignUp/Mover/SU_023_Verify Activate Email is sent to Business consumer when sign-up successfully</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
