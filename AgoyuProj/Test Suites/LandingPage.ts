<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description></description>
   <name>LandingPage</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <lastRun>2018-11-30T10:19:26</lastRun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <testSuiteGuid>6e2be8c4-54d3-487e-abaa-1847520ed57c</testSuiteGuid>
   <testCaseLink>
      <guid>4120189e-d00a-4ebd-8852-ef4416cc4507</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_001_Moving Plan - Validation - Address From</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>dbd65385-4f01-410f-9879-9c8cf9c71524</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_002_Moving Plan - Validation - Address To</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>ff9341c3-2800-41db-93a3-6de39902e1fc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_003_Moving Plan - Validation - Exact Weight</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0061b1e2-6a6d-4aff-a971-91ef3ad22a0c</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_004_Moving Plan - Exact Weight - Behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>894bfb32-8e19-452f-82e6-13d01b60b604</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_005_Moving Plan - Agoyu Calculator button - Behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d6297c8-cebd-4f0d-b81d-1f6f8d30aced</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_006_Moving Plan - Room List modal - Default value</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2830f4dc-ab6a-4055-b5cc-4e3b950f8b10</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_007_Moving Plan - Room List modal - Validation</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>7deb7bf2-9f32-4679-bf1f-39545311627f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_008_Moving Plan - Room List modal - Verify weight for 1 bedroom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>e1612123-1060-41dc-a9c5-ec662ed6530d</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_009_Moving Plan - Room List modal - Verify weight for Bathroom</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>24993f38-3c5d-47f4-a4f9-327d5daeb522</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_010_Moving Plan - Room List modal - Verify weight for 1 Living Room</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>36a0c422-71d6-4300-bf76-b2930f4a684e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_011_Moving Plan - Room List modal - Verify weight for Dining Room</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>d51751b4-147c-4c20-8927-6a946caa0330</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_012_Moving Plan - Room List modal - Verify weight for Kitchen</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>976b3cda-2dae-449b-9275-3f6b514ce4ba</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_013_Moving Plan - Room List modal - Verify weight for Laundry Room</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>18c76bb8-49a1-46a1-a5d4-b83c40b384bd</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_014_Moving Plan - Room List modal - Verify weight for Garage Room</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>f247a0aa-2e8e-4da0-bfa4-fb8ce6789927</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_015_Moving Plan - Room List modal - Verify weight for Additional Storage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>78d74e57-3cd3-43d5-862f-5a0e58b5c602</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_016_Moving Plan - Room List modal - Verify weight for Basement</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9921c63b-83d3-4c99-b29c-dd53233af811</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_017_Moving Plan - Room List modal - Verify weight for Storage Shed</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>1979d009-1a3d-45bd-b21a-34dda0cc9a19</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_018_Moving Plan - Room List modal - Verify weight for Patio</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>9451990a-1486-4eec-8ac5-a2a5b06a39b1</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_019_Moving Plan - Room List modal - Verify Approx Weight is calculated correctly</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>6d5d5f30-0905-48d1-9ced-ed2c5eae5f02</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_020_Moving Plan - Room List modal - Verify Behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>63929ef6-fe67-4f25-b268-5b1f4a2ee690</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_021_Moving Plan - Total Weight - Verify value remained when change between ExactWeight and RoomList</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b954a50-efca-4749-9251-4a2ca7be7a08</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_022_Moving Plan - Room List weight - Verify Approx Weight shown on Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5b790048-433a-4119-af5c-2551c62028a3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_023_Moving Plan - INVALID - Exact weight - Verify Approx Weight shown on Landing Page</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>0c048250-4949-45a4-8006-e27b3a528261</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_024_Moving Plan - Submit Plan at first time</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>823d6456-ef06-4249-a5d6-9df65321cda5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_025_Moving Plan - Submit Plan at second time</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>2e30ae12-d0a8-4623-8e16-ada67bb7552e</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_026_Moving Plan - Confirmation Plan modal - close modal</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>5df5a88f-397d-4776-9537-de6ded750492</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_027_Landing page - Continue previous plan link shown after submit plan and access landingpage</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>56ddf327-a5a0-4380-9d80-b98a91fb53cc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_028_Moving Plan - Continue previous plan link - Behavior</testCaseId>
   </testCaseLink>
   <testCaseLink>
      <guid>909f2a3b-03df-4082-a2de-044db514dcc3</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/TestCasesList/LandingPage/LP_029_Moving Plan - Submit Plan at second time with same previous plan info</testCaseId>
   </testCaseLink>
</TestSuiteEntity>
