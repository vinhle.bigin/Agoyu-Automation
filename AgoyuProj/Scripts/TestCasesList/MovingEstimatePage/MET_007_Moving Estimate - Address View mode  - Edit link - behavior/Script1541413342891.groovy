import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common
import functions.DateTimeFuncs

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LandingPage as LandingPage
import pages_elements.LeftMenu as LeftMenu
import objects.MovingPlan as MovingPlan
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal as SignUpModal
import pages_elements.LoginPage as LoginPage
import pages_elements.MovingEstimatePage

WebUI.openBrowser(GlobalVariable.glb_URL)
User user = new User()
WebUI.delay(5)

MovingEstimatePage currentpage = new MovingEstimatePage()

user.ProvideMoveInfo_func(user.plan, "Yes")

WebUI.comment('CLICK ON EDIT LINK')
user.plan.GetAddress(100)
DateTimeFuncs dt = new DateTimeFuncs()
dt.GetServerDateTime()
dt.currentDate = dt.IncreaseDay(dt.currentDate, 1)

user.plan.MovingDate = dt.FormatDate(dt.currentDate,"MM/dd/yyyy")

currentpage.AddressEdit_lnk().click()

WebUI.comment('VERIFY FIELDS ARE EDITABLE')
Common.VerifyFieldEditable_func("Address From", currentpage.AddrFrom_txt(),user.plan.AddrFrom,"in Edit mode")

Common.VerifyFieldEditable_func("Address To", currentpage.AddrTo_txt(),user.plan.AddrTo,"in Edit mode")

Common.VerifyFieldEditable_func("MoveDate", currentpage.MoveDate_txt(),user.plan.MovingDate,"in Edit mode")

Common.VerifyFieldDisplayed_func("Update Button", currentpage.MoveUpdate_btn(),"in Edit mode")

Common.VerifyFieldDisplayed_func("Cancel Button", currentpage.MoveCancel_btn(),"in Edit mode")

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()
}

