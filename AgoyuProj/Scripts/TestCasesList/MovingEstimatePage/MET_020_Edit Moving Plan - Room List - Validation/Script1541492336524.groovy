import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import functions.Common as Common
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.Inventorymodal as Inventorymodal
import pages_elements.LandingPage as LandingPage
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.MovingPlan as MovingPlan
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal as SignUpModal
import pages_elements.LoginPage as LoginPage
import pages_elements.MovingEstimatePage

WebUI.openBrowser(GlobalVariable.glb_URL)
User user = new User()
WebUI.delay(5)

MovingEstimatePage currentpage = new MovingEstimatePage()

user.ProvideMoveInfo_func(user.plan, "Yes")

WebUI.comment('GO TO WEIGHT EDIT MODE')
currentpage.WeightEdit_lnk().click()


WebUI.comment('UNCHECK THE EXACT WEIGHT OPTION')
Boolean status = currentpage.ExactWeight_opt().isSelected()

if(status)
{
	Common cm = new Common()
	cm.HandleElementClick(currentpage.ExactWeight_opt())
}

WebUI.delay(1)

WebUI.comment('VERIFY VALIDATION FOR INPUT <1000')

input = '0'

user.plan.Bathroom = 0

user.plan.Bedroom = 0

validation_msg = 'The custom weights must be at least 1000 pounds.'

user.ProvideCustomWeight_func(user.plan)

currentpage.TestValidationInventoryWeight_func(input, validation_msg)

WebUI.delay(2)
WebUI.comment('VERIFY VALIDATION FOR INPUT')

input = '51000'

user.plan.Bedroom = 51000

validation_msg = 'The moving weight should not exceed 50,000 pounds.'

user.ProvideCustomWeight_func(user.plan)

currentpage.TestValidationInventoryWeight_func(input, validation_msg)

WebUI.delay(2)
WebUI.comment('VERIFY CANNOT INPUT NEGATIVE VALUE')
Inventorymodal modal = new Inventorymodal()
int negative_num = -1

Common.ActionSendkey(modal.Bedroom_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Bedroom',  modal.Bedroom_txt(), negative_num.toString())

//Bathroom
Common.ActionSendkey(modal.Bathroom_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Bathroom_txt',  modal.Bathroom_txt(), negative_num.toString())

//LivingRoom
Common.ActionSendkey(modal.LivingRoom_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Bathroom_txt',  modal.LivingRoom_txt(), negative_num.toString())

//DiningRoom_txt
Common.ActionSendkey(modal.DiningRoom_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('DiningRoom_txt',  modal.DiningRoom_txt(), negative_num.toString())

//Kitchen_txt
Common.ActionSendkey(modal.Kitchen_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Kitchen_txt',  modal.Kitchen_txt(), negative_num.toString())

//Laundry_txt
Common.ActionSendkey(modal.Laundry_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Laundry_txt',  modal.Laundry_txt(), negative_num.toString())

//CarGarage_txt
Common.ActionSendkey(modal.CarGarage_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('CarGarage_txt',  modal.CarGarage_txt(), negative_num.toString())

//AdditionStorage_txt
Common.ActionSendkey(modal.AdditionStorage_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('AdditionStorage_txt',  modal.AdditionStorage_txt(), negative_num.toString())

//Basement_txt
Common.ActionSendkey(modal.Basement_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Basement_txt',  modal.Basement_txt(), negative_num.toString())

//StorageShed_txt
Common.ActionSendkey(modal.StorageShed_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('StorageShed_txt',  modal.StorageShed_txt(), negative_num.toString())

//Patio_txt
Common.ActionSendkey(modal.Patio_txt(), negative_num.toString())

Common.VerifyFieldTextNotEqual_func('Patio_txt',  modal.Patio_txt(), negative_num.toString())


WebUI.comment('VERIFY VALIDATION FOR VALID INPUT')

input = '1'

user.plan.Bedroom = 1

validation_msg = ''

user.ProvideCustomWeight_func(user.plan)

currentpage.TestValidationInventoryWeight_func(input, validation_msg)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
}

