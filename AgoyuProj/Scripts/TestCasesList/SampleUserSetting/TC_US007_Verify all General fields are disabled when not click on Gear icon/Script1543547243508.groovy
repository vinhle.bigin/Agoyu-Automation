import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User
import pages_elements.UserProfilePage 
import org.junit.Assert as Assert


User user = new User()
WebUI.comment('STEP#1: lOG INTO SITE')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : GlobalVariable.glb_username, ('password_str') : GlobalVariable.glb_password], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#2: GO TO USER PROFILE PAGE')

WebUI.callTestCase(findTestCase('Method/Navigation/GotoUserProfile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#3: VERIFY USERID BEFORE CLICK ON EDIT BUTTON')
UserProfilePage currentpage = new UserProfilePage()

WebUI.delay(3)


WebUI.comment('VERIFY FIRSTNAME IS DISABLE')
status = currentpage.IsTextFieldEditable(currentpage.Firstname_txt(), "abc")
if(status == true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Firstname should not enable.\n"
}

WebUI.comment('VERIFY LASTNAME IS DISABLE')
status = currentpage.IsTextFieldEditable(currentpage.Lastname_txt(), "abc")
if(status == true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Lastname_txt should not enable.\n"
}

WebUI.comment('VERIFY EMAIL IS DISABLE')
status = currentpage.IsTextFieldEditable(currentpage.Email_txt(), "abc")
if(status == true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Email should not enable.\n"
}

WebUI.comment('VERIFY PHONE IS DISABLE')
status = currentpage.IsTextFieldEditable(currentpage.Phone_txt(), "1507")
if(status == true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Phone should not enable.\n"
}

WebUI.comment('VERIFY BIRTHDAY IS DISABLE')
status = currentpage.IsTextFieldEditable(currentpage.Birthday_txt(), "2018-06-13")
if(status == true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Birthday should not enable.\n"
}

WebUI.comment('VERIFY PASSPORT IS DISABLE')
status = currentpage.IsTextFieldEditable(currentpage.Passpord_txt(), "2018-06-13")
if(status == true)
{
GlobalVariable.glb_TCStatus = false
GlobalVariable.glb_TCFailedMessage += "The Passpord should not enable.\n"
}


WebUI.closeBrowser()

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}
