import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

User user = new User()

WebUI.comment('STEP#1: lOG INTO SITE')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : GlobalVariable.glb_username, ('password_str') : GlobalVariable.glb_password], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#2: GO TO USER PROFILE PAGE')

WebUI.callTestCase(findTestCase('Method/Navigation/GotoUserProfile'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

UserProfilePage currentpage = new UserProfilePage()


WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

user.phone = ''

validation_msg = ''

WebUI.callTestCase(findTestCase('Method/UserSetting/TestValidationPhone'), [('input_str') : user.phone, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('VERIFY VALIDATION FOR INPUT: ALPHA CHARS')

user.phone = 'abc'

validation_msg = 'The Phone must not contain the alphabet characters.'

WebUI.callTestCase(findTestCase('Method/UserSetting/TestValidationPhone'), [('input_str') : user.phone, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('VERIFY INPUT = VALID VALUE')

user.phone = '123456789'
validation_msg = ''

WebUI.callTestCase(findTestCase('Method/UserSetting/TestValidationPhone'), [('input_str') : user.phone, ('Expected_msg') : validation_msg], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.closeBrowser()

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}
