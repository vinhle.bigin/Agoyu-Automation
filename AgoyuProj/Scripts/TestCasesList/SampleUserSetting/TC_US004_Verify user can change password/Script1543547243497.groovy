import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User as User
import org.junit.Assert as Assert
import functions.DateTimeFuncs

User user = new User()

WebUI.callTestCase(findTestCase('null'), [('referal_url') : '', ('firstname') : user.firstname, ('lastname') : user.lastname
	,('username'):user.username, ('email') : user.email, ('password') : user.password, ('phone') : user.phone], FailureHandling.STOP_ON_FAILURE)

user.UpdateActiveSttFromDB('TRUE')

newpassword_str = 'Password2'

WebUI.comment('STEP#1: lOG INTO SITE')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : user.username, ('password_str') : user.password], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#2: GO TO SETTING PAGE')

WebUI.callTestCase(findTestCase('Method/Navigation/GotoSettingPage'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('STEP#3: UPDATE THE PASSWORD INFO')

WebUI.callTestCase(findTestCase('Method/UserSetting/UpdatePassword'), [('Button') : 'Update', ('newpassord_str') : newpassword_str
        , ('oldpassword_str') : user.password], FailureHandling.STOP_ON_FAILURE)

WebUI.delay(5)

WebUI.comment('STEP#4: VERIFY USER CAN LOGIN WITH NEW PASSWORD')

WebUI.callTestCase(findTestCase('Method/Login/LoginBack'), [('username_str') : user.username, ('password_str') : newpassword_str], 
    FailureHandling.STOP_ON_FAILURE)


LeftMenu leftmenu = new LeftMenu()

if(leftmenu.logo().isDisplayed()!=true)
{
	GlobalVariable.glb_TCStatus = false
	GlobalVariable.glb_TCFailedMessage += "User cannot login successfully with new password. "
}

WebUI.closeBrowser()
Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
	DateTimeFuncs dt = new DateTimeFuncs()

	dt.RollBackServerTime()
	
}
