import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.LeftMenu as LeftMenu
import objects.MovingPlan
import objects.User as User
import static org.junit.Assert.*
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import functions.DateTimeFuncs as DateTimeFuncs
import functions.Navigation
import functions.Common
import functions.Convert as Convert
import functions.DBCONNECT

import java.text.DecimalFormat as DecimalFormat
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys as Keys
import java.util.Arrays as Arrays
import java.util.Date as Date
import java.text.SimpleDateFormat as SimpleDateFormat


import java.io.File;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

//WebElement element_tmp = element

//MovingPlan plan = new MovingPlan()
//println(plan.MovingDate)

//DateTimeFuncs dt = new DateTimeFuncs()
//dt.IncreaseServerHour(72)

User user = new User()
user.userType= "Business"
user.email = "testuser17135464@bg.vn"

user.Login_func()

WebUI.comment('GO TO Business PROFILE')

Navigation.GotoUserProfile_func()

ConsumerProfilePage.BsinessProfile_Tab().click()

WebUI.delay(2)

WebUI.comment('OPEN EDIT MODE FOR PROFILE INFO')

ConsumerProfilePage.BsinessEdit_lnk().click()

WebUI.delay(2)

WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

String logo_file  = 'invalidlogo.txt'

validation_msg = 'The file must be an image.'

ConsumerProfilePage.TestValidationLogo_func(logo_file, validation_msg)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    //WebUI.closeBrowser()

  //  DateTimeFuncs dt = new DateTimeFuncs()

  //  dt.RollBackServerTime()
}



