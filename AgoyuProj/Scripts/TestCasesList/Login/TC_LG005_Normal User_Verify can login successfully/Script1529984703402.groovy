import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu
import com.kms.katalon.core.util.KeywordUtil
import pages_elements.SignUpModal
import org.junit.Assert as Assert
User user = new User()


WebUI.comment('2. Sign in with member default user')

user.username = GlobalVariable.glb_username
user.password = GlobalVariable.glb_password

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : user.username, ('password_str') : user.password],
	FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('Step3. Verify User logs in successfully')
LeftMenu leftmenu = new LeftMenu()


if(leftmenu.logo()==null)
{
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage+="DEFAULT MEMBER User cannot login successfully"		
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)
WebUI.closeBrowser()

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}
