import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.EmailConnect
import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.Mail
import objects.User as User
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.SignUpModal as SignUpModal
import org.junit.Assert as Assert

User user = new User()
user.email= GlobalVariable.glb_usermail



Mail email = new Mail()
email.Subject = "Reset Password"//"Welcome to Agoyu."
email.contactname = user.contactName
WebUI.comment('PROVIDE THE EMAIL TO RESET PASSWORD')
Navigation.OpenSite_func()
user.ProvideEmailResetPass_func()

WebUI.comment('VERIFY ACTIVATE EMAIL IS SENT TO USER')
EmailConnect mailconnect = new EmailConnect("Gmail")
mailconnect.fetch()

mailconnect.VerifyEmailExist(email,"Reset")
 
Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	User user = new User()
	user.email = GlobalVariable.glb_usermail
	
	if(!user.IsUserExisted())
	{
		user.SignUpUser_func()
		user.UpdateActiveSttFromDB("TRUE")
	WebUI.closeBrowser()
	}
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
}

