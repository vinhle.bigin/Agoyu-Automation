import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LandingPage
import pages_elements.LeftMenu
import pages_elements.LoginPage

import com.kms.katalon.core.util.KeywordUtil
import pages_elements.SignUpModal
import org.junit.Assert as Assert
import pages_elements.Usersetting_menu
User user = new User()


WebUI.comment('PRECONDITION: CREATE')

user.email = GlobalVariable.glb_usermail
user.password = GlobalVariable.glb_gmailPass
user.GetUserInfoFromDB()

WebUI.openBrowser(GlobalVariable.glb_URL)
WebUI.delay(5)



if(!user.IsUserExisted())
{
	user.SignUpUser_func()
}



user.UpdateActiveSttFromDB("TRUE")


LandingPage landing = new LandingPage()
landing.Login_lnk().click()
WebUI.delay(2)

LoginPage lginpage = new LoginPage()

lginpage.FacebookLogin_btn().click()
WebUI.delay(2)

user.LoginFacebook_func(user.email, user.password)

WebUI.comment('Step3. Verify User logs in successfully')
Usersetting_menu settingmenu = new Usersetting_menu()


String hiuser_text = "Hi, "+user.contactName

if(settingmenu.UserSetting_dropdown()==null)
{
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage+="User cannot login successfully"
}
else
	{
		String Observed = settingmenu.UserSetting_dropdown().getText()
		if(Observed!=hiuser_text){
			GlobalVariable.glb_TCStatus= false
			GlobalVariable.glb_TCFailedMessage+="Incorrect topbar menu text.[Observed: "+Observed+"Expected:"+hiuser_text+"]."
		}
	}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
		
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()
}
