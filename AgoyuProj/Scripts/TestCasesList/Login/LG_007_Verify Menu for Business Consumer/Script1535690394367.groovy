import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.SignUpModal as SignUpModal
import pages_elements.Usersetting_menu
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.By

import org.junit.Assert as Assert

User user = new User()
user.userType = "Business"
WebUI.comment('1. Go to Sign Up via Signup modal')

user.SignUpUser_func()

user.UpdateActiveSttFromDB("TRUE")

user.Login_func()

int count =3

Usersetting_menu menu = new Usersetting_menu()

menu.UserSetting_dropdown().click()

String dropdown_xpath = "//div[@class ='dropdown-menu show']/a"

List<WebElement> temp_element
WebDriver webDriver = DriverFactory.getWebDriver()
try{
	
	temp_element = webDriver.findElements(By.xpath(dropdown_xpath));
	return temp_element;
}
catch(NoSuchElementException e) {
	temp_element = null
}

if(temp_element==null)
{
	GlobalVariable.glb_TCStatus = false
	
	GlobalVariable.glb_TCFailedMessage = 'No menu item for Personal Consumer login.'
}
else
{
	int size = temp_element.size()
	if(size==count)
	{
		if(menu.Profile_mnuitem()==null)
		{
			GlobalVariable.glb_TCStatus = false
			
			GlobalVariable.glb_TCFailedMessage = 'No Profile menu item for Personal Consumer login.'
		}
		
		if(menu.ManageUser_mnuitem()==null)
		{
			GlobalVariable.glb_TCStatus = false
			
			GlobalVariable.glb_TCFailedMessage = 'No ManageUser menu item for Personal Consumer login.'
		}
		
		if(menu.LogOut_mnuitem()==null)
		{
			GlobalVariable.glb_TCStatus = false
			
			GlobalVariable.glb_TCFailedMessage = 'No Logout menu item for Personal Consumer login.'
		}
		
		
	}
	else {
		GlobalVariable.glb_TCStatus = false
		
		GlobalVariable.glb_TCFailedMessage = "Personal Consumer-Incorrect menu item number .[Observed: "+size+"-Expected: "+count+"]"
	}
}




Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
}

