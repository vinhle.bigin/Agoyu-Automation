import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu
import org.junit.Assert as Assert

WebUI.comment('PRE-CONDITON: SIGNUP NEW USER')

	User newuser = new User()

	WebUI.callTestCase(findTestCase('null'), [('referal_url') : '', ('firstname') : newuser.firstname, ('lastname') : newuser.lastname
		, ('username') : newuser.username, ('email') : newuser.email, ('password') : newuser.password, ('phone') : newuser.phone],
	FailureHandling.STOP_ON_FAILURE)
	newuser.UpdateActiveSttFromDB('TRUE')

WebUI.comment('Log into site')

	WebUI.callTestCase(findTestCase('Method/Login/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser],
		FailureHandling.OPTIONAL)

WebUI.comment('NAVIGATE TO MANAGE USER LANDING PAGE')
	
	WebUI.callTestCase(findTestCase('Method/Navigation/GotoLandingManageUser'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Method/ManageUser/ChangeLock_UnlockUser'), [('username_str') : newuser.username, ('newstatus_str') : 'Lock'], 
    FailureHandling.STOP_ON_FAILURE)


WebUI.comment('Log into site')

	WebUI.callTestCase(findTestCase('Method/Login/LoginBack'), [('password_str') : newuser.password , ('username_str') : newuser.username],
		FailureHandling.CONTINUE_ON_FAILURE)
	
LeftMenu menu = new LeftMenu() 

if(menu.username_lbl()!=null)
{
	GlobalVariable.glb_TCStatus = false

    GlobalVariable.glb_TCFailedMessage += 'Locked user should not be login successfully.'
	
}

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {

    WebUI.closeBrowser()
}

