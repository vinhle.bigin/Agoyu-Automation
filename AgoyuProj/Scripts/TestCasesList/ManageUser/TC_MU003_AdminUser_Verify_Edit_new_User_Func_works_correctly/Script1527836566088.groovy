import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.ManageUserLanding as ManageUserLanding
import pages_elements.EditUser as EditUser
import org.junit.Assert as Assert

oops_msg = 'Whoops, looks like something went wrong.'

message_str = ''

WebUI.comment('PRE-CONDITON: SIGNUP NEW USER')

User newuser = new User()

WebUI.callTestCase(findTestCase('null'), [('referal_url') : '', ('firstname') : newuser.firstname, ('lastname') : newuser.lastname
        , ('username') : newuser.username, ('email') : newuser.email, ('password') : newuser.password, ('phone') : newuser.phone], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Log into site')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser], 
    FailureHandling.OPTIONAL)

WebUI.comment('GO TO MANAGE USER LANDING PAGE')

ManageUserLanding landingpage = new ManageUserLanding()

WebUI.callTestCase(findTestCase('Method/Navigation/GotoLandingManageUser'), [:], FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('UPDATE USER')

newuser.ChangeUserInfo()

WebUI.callTestCase(findTestCase('Method/ManageUser/GotoUserEditMode'), [('username_str') : newuser.username], FailureHandling.STOP_ON_FAILURE)


WebUI.callTestCase(findTestCase('Method/ManageUser/UpdateUserOnManageUser'), [('firstname') : newuser.firstname, ('lastname') : newuser.lastname
        , ('email') : newuser.email, ('phone') : newuser.phone, ('birthday') : newuser.birthday, ('passport') : newuser.passpord
        , ('address1') : newuser.address1, ('address2') : newuser.address2, ('city') : newuser.city, ('state') : newuser.state
        , ('postalcode') : newuser.postalcode, ('password') : ''], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('VERIFY NO ERROR MESSAGE IS SHOWN')

EditUser editpage = new EditUser()

if (editpage.ErrorMessage() != null) {
    Observed_str = editpage.ErrorMessage().getText()

    if (oops_msg == Observed_str) {
        GlobalVariable.glb_TCStatus = false

        GlobalVariable.glb_TCFailedMessage += 'Error message should not displayed after edit user. '
    }
}

WebUI.comment('SEARCH THE UPDATED USER')

//WebUI.callTestCase(findTestCase('Method/ManageUser/SearchUser'), [('email_par') : '', ('username_par') : GlobalVariable.glb_username],FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Method/ManageUser/GotoUserEditMode'), [('username_str') : newuser.username], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('VERIFY UPDATED USER SHOWN CORRECTLY')

WebUI.callTestCase(findTestCase('Method/ManageUser/VerifyUserInfoCorrect'), [('username_str') : newuser.username, ('firstname_str') : newuser.firstname
        , ('lastname_str') : newuser.lastname, ('email_str') : newuser.email, ('phone_str') : newuser.phone, ('birthday_str') : newuser.birthday
        , ('passport_str') : newuser.passpord, ('address1_str') : newuser.address1, ('address2_str') : newuser.address2, ('city_str') : newuser.city
        , ('state_str') : newuser.state, ('postalcode_str') : newuser.postalcode, ('button'): 'Update'], FailureHandling.STOP_ON_FAILURE)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus) //teardown

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
   WebUI.closeBrowser()
}

