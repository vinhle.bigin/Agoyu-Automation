import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ManageUserLanding

import org.openqa.selenium.Keys as Keys
import org.junit.Assert as Assert

WebUI.comment('Log into site')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser], 
    FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('Method/Navigation/GotoLandingManageUser'), [:], FailureHandling.STOP_ON_FAILURE)
ManageUserLanding currentpage = new ManageUserLanding() 
if(currentpage.SearchUserName_txt()==null)
{
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage+="Search field does not shown. "
}

else
{
	WebUI.callTestCase(findTestCase('Method/ManageUser/SearchUser'), [('email_par') : '', ('username_par') : GlobalVariable.glb_username],
		FailureHandling.STOP_ON_FAILURE)
	
	result = WebUI.callTestCase(findTestCase('Method/ManageUser/IsUserFound'), [('Expected_str') : GlobalVariable.glb_username],
		FailureHandling.STOP_ON_FAILURE)
	
	
	if(result == false)
	{
		GlobalVariable.glb_TCStatus= false
		GlobalVariable.glb_TCFailedMessage+="User cannot be found. "
	}
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)
@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()

}//teardown


