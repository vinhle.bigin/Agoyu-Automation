import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User

import org.junit.Assert as Assert

WebUI.comment('===Scenario #1: Verify Lock User works correctly')

WebUI.comment('Log into site')

WebUI.callTestCase(findTestCase('Method/Navigation/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser], 
    FailureHandling.OPTIONAL)

WebUI.callTestCase(findTestCase('Method/Navigation/GotoLandingManageUser'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.callTestCase(findTestCase('Method/ManageUser/SearchUser'), [('email_par') : '', ('username_par') : GlobalVariable.glb_username], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.delay(3)

WebUI.callTestCase(findTestCase('Method/ManageUser/ChangeLock_UnlockUser'), [('newstatus_str') : 'Unlock'], FailureHandling.STOP_ON_FAILURE)

WebUI.comment('SEARCH FOR USER: glb_username')

WebUI.callTestCase(findTestCase('Method/ManageUser/SearchUser'), [('email_par') : '', ('username_par') : GlobalVariable.glb_username], 
    FailureHandling.STOP_ON_FAILURE)

WebUI.comment('Verify the Lock button changed to Unlock. ')

Observed_str = WebUI.getText(findTestObject('ManageUserLanding_screen/Lock_unLock_btn'), FailureHandling.STOP_ON_FAILURE)

WebUI.delay(1)

if (Observed_str != 'Unlock') {
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage += 'User cannot be locked successfully.The button not changed to Unclock.'
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	User newuser = new User()
	newuser.username = GlobalVariable.glb_username
	newuser.UpdateActiveSttFromDB('TRUE')
	WebUI.closeBrowser()
}


