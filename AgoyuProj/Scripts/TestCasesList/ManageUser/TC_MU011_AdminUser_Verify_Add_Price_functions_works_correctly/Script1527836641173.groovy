import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ManagePrice

import com.kms.katalon.core.webui.keyword.internal.WebUIKeywordMain as WebUIKeywordMain
import functions.DateTimeFuncs as DateTimeFuncs
import java.lang.String as String
import org.junit.Assert as Assert

	inputratetype_str = GlobalVariable.glb_CoinName+'/USD'

	inputprice_str = '20'

	success_message_str = 'Success\nThe price has been added successfully.'

WebUI.comment('Log into site')

	WebUI.callTestCase(findTestCase('Method/Login/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser], 
	    FailureHandling.OPTIONAL)
	
	WebUI.callTestCase(findTestCase('Method/Navigation/GotoManagePrice'), [:], FailureHandling.STOP_ON_FAILURE)
	
	WebUI.callTestCase(findTestCase('Method/ManagePrice/CreatePrice'), [('RateType_str') : inputratetype_str, ('Price_str') : inputprice_str], 
	    FailureHandling.STOP_ON_FAILURE)
	
	DateTimeFuncs dt = new DateTimeFuncs()
	
	tempcreatedat_str = dt.GetcurrentDatetime()

WebUI.comment('Verify message SUCCESS is shown')


	WebUI.callTestCase(findTestCase('Method/CoreMethod/VerifyPopUpMessageDisplay'), [('Expected_msg') : success_message_str], FailureHandling.CONTINUE_ON_FAILURE)

	ManagePrice currentpage = new ManagePrice()
	if(currentpage.BackPriceList_link()!=null)
	{
		GlobalVariable.glb_TCStatus = false
		GlobalVariable.glb_TCFailedMessage +="The Manage Price Landing page should redirected after add price success. "
		currentpage.BackPriceList_link().click()
	}

	WebUI.callTestCase(findTestCase('Method/ManagePrice/VerifyAddedPriceLogged'), [('ratetype_str') : inputratetype_str, ('price_str') : inputprice_str
	, ('createdat_str') : tempcreatedat_str], FailureHandling.STOP_ON_FAILURE)

	Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
}

