import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu
import pages_elements.Messages
import com.kms.katalon.core.util.KeywordUtil
import pages_elements.EditUser
import org.junit.Assert as Assert
oops_msg = "Whoops, looks like something went wrong."


WebUI.comment('Log into site')

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('password_str') : GlobalVariable.glb_adminpassword, ('username_str') : GlobalVariable.glb_adminuser], 
    FailureHandling.OPTIONAL)

User newuser = new User()

WebUI.comment('CREATE NEW USER')
WebUI.callTestCase(findTestCase('Method/ManageUser/CreateUser'), [('username_str') : newuser.username, ('email_str') : newuser.email
        , ('password_str') : newuser.password, ('userrole_str') : newuser.role], FailureHandling.STOP_ON_FAILURE)

newuser.UpdateActiveSttFromDB('TRUE')

WebUI.comment('VERIFY NO ERROR MESSAGE IS SHOWN')
EditUser editpage = new EditUser() 
Observed_str = editpage.ErrorMessage().getText()

if (oops_msg == Observed_str) {
	GlobalVariable.glb_TCStatus= false
GlobalVariable.glb_TCFailedMessage+="Error message should not displayed after create new user. "

}

WebUI.comment('VERIFY NEW USER CAN LOGIN SUCCESSFULLY')

WebUI.closeBrowser()

WebUI.callTestCase(findTestCase('Method/Login/Login'), [('username_str') : newuser.username, ('password_str') : newuser.password], 
    FailureHandling.CONTINUE_ON_FAILURE)

WebUI.comment('Step3. Verify New User logs in successfully')

LeftMenu leftmenu = new LeftMenu()

if (leftmenu.logo()== null) {
    GlobalVariable.glb_TCStatus= false
GlobalVariable.glb_TCFailedMessage+='New User cannot login successfully. '
	
} else 
{
    WebUI.comment('Step4. Verify username,email is shown on left side correctlys')
	if (leftmenu.username_lbl().getText() != newuser.username) {
		GlobalVariable.glb_TCStatus= false
GlobalVariable.glb_TCFailedMessage += ((('Username does not match.Observed: ' + leftmenu.username_lbl().getText()) + '- Expected:') + newuser.username)
	
		
	}
	
	if (leftmenu.email_lbl().getText() != newuser.email) {
		GlobalVariable.glb_TCStatus= false
GlobalVariable.glb_TCFailedMessage += ((('User email does not match.Observed: ' + leftmenu.email_lbl().getText()) + '- Expected:') + newuser.email)
	
	}
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()

}//teardown
