import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.EmailConnect

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.Mail
import objects.User as User
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.SignUpModal as SignUpModal
import pages_elements.Usersetting_menu

import org.junit.Assert as Assert

User user = new User()
user.email= GlobalVariable.glb_usermail
user.userType = 'Mover'
Mail email = new Mail()
email.Subject = "Agoyu Confirmation"
email.contactname = user.contactName
WebUI.comment('1. Go to Sign Up via Signup modal')

user.SignUpUser_func(true)

WebUI.comment('VERIFY ACTIVATE EMAIL IS SENT TO USER')
EmailConnect mailconnect = new EmailConnect("Gmail")
mailconnect.fetch()

email = mailconnect.GetExistEmail(email)

email.GetActivateLink()
	
WebUI.navigateToUrl(email.activatelink)
WebUI.delay(3)
WebUI.comment('Step3. Verify User logs in successfully')
Usersetting_menu settingmenu = new Usersetting_menu() 


String hiuser_text = "Hi, "+user.contactName

if(settingmenu.UserSetting_dropdown()==null)
{
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage+="User cannot auto login successfully"
}
else
	{
		String temp = settingmenu.UserSetting_dropdown().getText()
		if(temp!=hiuser_text){
			GlobalVariable.glb_TCStatus= false
			GlobalVariable.glb_TCFailedMessage+="Incorrect topbar menu text.[Observed: "+temp+"Expected:"+hiuser_text+"]."
		}
	}
 
 
Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    
	User user = new User()
	user.email= GlobalVariable.glb_usermail
	if(user.IsUserExisted())
	{
		user.UpdateEmailFromDB()
	}
	GlobalVariable.glb_TCStatus = true

	GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
}

