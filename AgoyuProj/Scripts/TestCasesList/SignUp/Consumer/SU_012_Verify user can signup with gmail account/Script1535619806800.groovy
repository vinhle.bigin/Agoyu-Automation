import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common
import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.Facebook
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.SignUpModal as SignUpModal
import org.junit.Assert as Assert

User user = new User()
user.email= GlobalVariable.glb_usermail
user.password= GlobalVariable.glb_gmailPass

WebUI.comment('1. Go to Sign Up via Signup modal')

Navigation.OpenSignUpModal()

SignUpModal.PsnalGmail_btn().click()

user.LoginGmail_func(user.email, user.password)

user.SignUpUser_func(false)

WebUI.comment('Verify success message is displayed correctly')

success_msg = 'Register account sucess. Please confirm your email to complete process.'

Messages.VerifyNotificationMessageDisplay_func(success_msg)

Boolean status = user.IsUserExisted()

if(!status)
{
	GlobalVariable.glb_TCStatus = false
	
	GlobalVariable.glb_TCFailedMessage += 'Gmail Signup user cannot be found under DB.'
	
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	
	User user = new User()
	user.email= GlobalVariable.glb_usermail
	if(user.IsUserExisted())
	{
		user.UpdateEmailFromDB()
	}
	
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
	
}

