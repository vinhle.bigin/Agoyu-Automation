import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Navigation as Navigation 
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LandingPage as LandingPage
import pages_elements.LeftMenu as LeftMenu
import pages_elements.UserProfilePage as UserProfilePage
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal as SignUpModal
import pages_elements.LoginPage as LoginPage

User user = new User()

user.contactName = ''

user.email = ''

user.password = ''

user.phone = ''

user.logoname = ''

WebUI.openBrowser(GlobalVariable.glb_URL)

SignUpModal signup_modal = new SignUpModal() 

Navigation.OpenSignUpModal()

WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

user.email = ''

validation_msg = 'The email field is required.'

user.SignUpUser_func(false)

signup_modal.TestValidationPsnalEmail_func(user.email, validation_msg)

WebUI.comment('VERIFY VALIDATION FOR INPUT: invalidformat')

user.email = 'testingemail@com'

validation_msg = 'The email must be a valid email address.'

user.SignUpUser_func(false)

signup_modal.TestValidationPsnalEmail_func(user.email, validation_msg)

WebUI.comment('VERIFY VALIDATION FOR INPUT: Existing user')

user.email = GlobalVariable.glb_usermail

validation_msg = 'The email has already been taken.'

user.SignUpUser_func(false)

signup_modal.TestValidationPsnalEmail_func(user.email, validation_msg)

WebUI.comment('VERIFY INPUT = VALID VALUE')

user.email = 'testingusername@gmail.com'
validation_msg = ''

user.SignUpUser_func(false)

signup_modal.TestValidationPsnalEmail_func(user.email, validation_msg)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}
@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()
}

