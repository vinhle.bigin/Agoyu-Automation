import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Navigation as Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.LeftMenu as LeftMenu
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal
import pages_elements.LoginPage
User user = new User()
user.userType= "Business"


String valid_str = user.businessName

user.SignUpUser_func(true)

user.UpdateActiveSttFromDB("TRUE")

user.Login_func()

WebUI.comment('GO TO Business PROFILE')

Navigation.GotoUserProfile_func()

ConsumerProfilePage.BsinessProfile_Tab().click()

WebUI.delay(2)

WebUI.comment('OPEN EDIT MODE FOR PROFILE INFO')

ConsumerProfilePage.BsinessEdit_lnk().click()

WebUI.delay(2)

WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

user.businessName = ''

validation_msg = 'The Business Name field is required.'

ConsumerProfilePage.TestValidationBsinessName_func(user.businessName,validation_msg)

WebUI.comment('VERIFY VALIDATION FOR INPUT: >50 CHARS')

user.businessName = '51characterssssssssssssssssssssssssssssssssssssssss'

validation_msg = 'The Business Name may not be greater than 50 characters.'

ConsumerProfilePage.TestValidationBsinessName_func(user.businessName,validation_msg)

WebUI.comment('VERIFY INPUT = VALID VALUE')

user.businessName = valid_str+" Updated"

validation_msg = ''

ConsumerProfilePage.TestValidationBsinessName_func(user.businessName,validation_msg)

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}
@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	//WebUI.closeBrowser()
}
