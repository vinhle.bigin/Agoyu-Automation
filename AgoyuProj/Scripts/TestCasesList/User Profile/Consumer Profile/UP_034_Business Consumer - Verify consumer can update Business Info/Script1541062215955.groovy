import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common
import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.LeftMenu as LeftMenu
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal
import pages_elements.LoginPage

User user = new User()

user.SignUpUser_func(true)

user.UpdateActiveSttFromDB("TRUE")

user.Login_func()

WebUI.comment('GO TO BUSINESS PROFILE')

Navigation.GotoUserProfile_func()

ConsumerProfilePage.BsinessProfile_Tab().click()

WebUI.delay(2)

WebUI.comment('OPEN EDIT MODE FOR PROFILE INFO')

ConsumerProfilePage.BsinessEdit_lnk().click()

WebUI.delay(2)

user.UpdateBusinessProfile_func(true)

WebUI.comment('VERIFY PROFILE INFO ARE CORRECT ON VIEW MODE')

Common.VerifyFieldValueEqual_func("BsinessName", ConsumerProfilePage.BsinessName_txt(),user.businessName,"in View mode")

Common.VerifyFieldValueEqual_func("BsinessMail", ConsumerProfilePage.BsinessEmail_txt(),user.bsinessEmail,"in View mode")

Common.VerifyFieldValueEqual_func("BsinessPhone", ConsumerProfilePage.BsinessPhone_txt(),user.bsinessPhone,"in View mode")

Common.VerifyFieldValueEqual_func("BsinessWeb", ConsumerProfilePage.BsinessWeb_txt(),user.bsinessWeb,"in View mode")

Common.VerifyFieldValueEqual_func("BsinessDescr", ConsumerProfilePage.BsinessDescr_txt(),user.bsinessDescr,"in View mode")

Common.VerifyFieldValueEqual_func("Address", ConsumerProfilePage.Address_txt(),user.address1,"in View mode")

Common.VerifyFieldValueEqual_func("City", ConsumerProfilePage.City_txt(),user.city,"in View mode")

Common.VerifyFieldValueEqual_func("State", ConsumerProfilePage.State_txt(),user.state,"in View mode")

Common.VerifyFieldValueEqual_func("Zip", ConsumerProfilePage.Zip_txt(),user.postalcode,"in View mode")

WebUI.comment('VERIFY PROFILE INFO ARE CORRECT ON EDIT MODE')
ConsumerProfilePage.BsinessEdit_lnk().click()

WebUI.delay(2)

Common.VerifyFieldValueEqual_func("BsinessName", ConsumerProfilePage.BsinessName_txt(),user.businessName,"in Edit mode")

Common.VerifyFieldValueEqual_func("BsinessMail", ConsumerProfilePage.BsinessEmail_txt(),user.bsinessEmail,"in Edit mode")

Common.VerifyFieldValueEqual_func("BsinessPhone", ConsumerProfilePage.BsinessPhone_txt(),user.bsinessPhone,"in Edit mode")

Common.VerifyFieldValueEqual_func("BsinessWeb", ConsumerProfilePage.BsinessWeb_txt(),user.bsinessWeb,"in Edit mode")

Common.VerifyFieldValueEqual_func("BsinessDescr", ConsumerProfilePage.BsinessDescr_txt(),user.bsinessDescr,"in Edit mode")

Common.VerifyFieldValueEqual_func("Address", ConsumerProfilePage.Address_txt(),user.address1,"in Edit mode")

Common.VerifyFieldValueEqual_func("City", ConsumerProfilePage.City_txt(),user.city,"in Edit mode")

Common.VerifyFieldValueEqual_func("State", ConsumerProfilePage.State_txt(),user.state,"in Edit mode")

Common.VerifyFieldValueEqual_func("Zip", ConsumerProfilePage.Zip_txt(),user.postalcode,"in Edit mode")

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	//WebUI.closeBrowser()
}

