import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common
import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.LeftMenu as LeftMenu
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal
import pages_elements.LoginPage

User user = new User()
user.userType = "Business"

user.SignUpUser_func(true)

user.UpdateActiveSttFromDB("TRUE")

user.Login_func()

WebUI.comment('GO TO USER PROFILE')

Navigation.GotoUserProfile_func()

ConsumerProfilePage.BsinessProfile_Tab().click()

WebUI.delay(2)


WebUI.comment('VERIFY DEFAULT STATE OF EXPANDING')

Common.VerifyFieldDisplayed_func("ContactName", ConsumerProfilePage.BsinessName_txt(), "in Default Expand mode")

Common.VerifyFieldDisplayed_func("ContactMail", ConsumerProfilePage.BsinessEmail_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("BsinessPhone_txt", ConsumerProfilePage.BsinessPhone_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("BsinessDescr_txt", ConsumerProfilePage.BsinessDescr_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("BsinessWeb_txt", ConsumerProfilePage.BsinessWeb_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("Address_txt", ConsumerProfilePage.Address_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("City_txt", ConsumerProfilePage.City_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("State_txt", ConsumerProfilePage.State_txt(),"in Default Expand mode")

Common.VerifyFieldDisplayed_func("Zip_txt", ConsumerProfilePage.Zip_txt(),"in Default Expand mode")


WebUI.comment('CLICK ON COLLAPSE ICON')

ConsumerProfilePage.BsinessExpand_icon().click()

WebUI.delay(2)

WebUI.comment('VERIFY ALL FIELDS ARE HIDDEN')

Common.VerifyFieldNotDisplayed_func("ContactName", ConsumerProfilePage.BsinessName_txt(), "in Collapse mode")

Common.VerifyFieldNotDisplayed_func("ContactMail", ConsumerProfilePage.BsinessEmail_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("BsinessPhone_txt", ConsumerProfilePage.BsinessPhone_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("BsinessDescr_txt", ConsumerProfilePage.BsinessDescr_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("BsinessWeb_txt", ConsumerProfilePage.BsinessWeb_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("Address_txt", ConsumerProfilePage.Address_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("City_txt", ConsumerProfilePage.City_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("State_txt", ConsumerProfilePage.State_txt(),"in Collapse mode")

Common.VerifyFieldNotDisplayed_func("Zip_txt", ConsumerProfilePage.Zip_txt(),"in Collapse mode")


WebUI.comment('CLICK ON EXPAND ICON')

ConsumerProfilePage.BsinessExpand_icon().click()

WebUI.delay(2)

WebUI.comment('VERIFY STATE OF EXPANDING')

Common.VerifyFieldDisplayed_func("ContactName", ConsumerProfilePage.BsinessName_txt(), "in Expand mode")

Common.VerifyFieldDisplayed_func("ContactMail", ConsumerProfilePage.BsinessEmail_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("BsinessPhone_txt", ConsumerProfilePage.BsinessPhone_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("BsinessDescr_txt", ConsumerProfilePage.BsinessDescr_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("BsinessWeb_txt", ConsumerProfilePage.BsinessWeb_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("Address_txt", ConsumerProfilePage.Address_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("City_txt", ConsumerProfilePage.City_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("State_txt", ConsumerProfilePage.State_txt(),"in Expand mode")

Common.VerifyFieldDisplayed_func("Zip_txt", ConsumerProfilePage.Zip_txt(),"in Expand mode")



Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	//WebUI.closeBrowser()
}

