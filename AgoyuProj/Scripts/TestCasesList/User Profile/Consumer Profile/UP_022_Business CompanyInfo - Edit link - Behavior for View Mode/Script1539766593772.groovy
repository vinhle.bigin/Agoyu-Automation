import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common
import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.LeftMenu as LeftMenu
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal
import pages_elements.LoginPage

User user = new User()
user.userType = "Business"

user.SignUpUser_func(true)

user.UpdateActiveSttFromDB("TRUE")

user.Login_func()

WebUI.comment('GO TO USER PROFILE - Company Tab')

Navigation.GotoUserProfile_func()

ConsumerProfilePage.BsinessProfile_Tab().click()

WebUI.delay(2)

WebUI.comment('OPEN EDIT MODE FOR PROFILE INFO')

ConsumerProfilePage.BsinessEdit_lnk().click()

WebUI.delay(2)

WebUI.comment('CLICK EDIT LINK AGAIN')

ConsumerProfilePage.BsinessEdit_lnk().click()

WebUI.delay(2)


WebUI.comment('VERIFY ALL FIELDS ARE DISABLE')

Common.VerifyFieldNotEditable_func("BsinessName_txt", ConsumerProfilePage.BsinessName_txt(), "name for testing", "in View mode")

Common.VerifyFieldNotEditable_func("BsinessEmail_txt", ConsumerProfilePage.BsinessEmail_txt(), "fortetsing@email.com", "in View mode")

Common.VerifyFieldNotEditable_func("ContactPhone", ConsumerProfilePage.BsinessPhone_txt(), "(123) 456-789", "in View mode")

Common.VerifyFieldNotEditable_func("ContactName", ConsumerProfilePage.BsinessDescr_txt(), "descr for testing", "in View mode")

Common.VerifyFieldNotEditable_func("ContactName", ConsumerProfilePage.BsinessWeb_txt(), "www.testing.com", "in View mode")

Common.VerifyFieldNotEditable_func("ContactName", ConsumerProfilePage.Address_txt(), "add1234", "in View mode")

Common.VerifyFieldNotEditable_func("ContactName", ConsumerProfilePage.City_txt(), "NY", "in View mode")

Common.VerifyFieldNotEditable_func("ContactName", ConsumerProfilePage.State_txt(), "Alabama", "in View mode")

Common.VerifyFieldNotEditable_func("ContactName", ConsumerProfilePage.Zip_txt(), "5555", "in View mode")

Common.VerifyFieldNotDisplayed_func("Update button", ConsumerProfilePage.BsinessUpdate_btn())

Common.VerifyFieldNotDisplayed_func("Cancel button", ConsumerProfilePage.BsinessCancel_btn())



Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	//WebUI.closeBrowser()
}

