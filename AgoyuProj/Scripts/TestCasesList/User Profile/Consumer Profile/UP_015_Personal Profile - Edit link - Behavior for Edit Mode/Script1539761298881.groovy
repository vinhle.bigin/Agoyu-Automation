import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common
import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.LeftMenu as LeftMenu
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal
import pages_elements.LoginPage

User user = new User()

user.SignUpUser_func(true)

user.UpdateActiveSttFromDB("TRUE")

user.Login_func()

WebUI.comment('GO TO USER PROFILE')

Navigation.GotoUserProfile_func()

WebUI.comment('OPEN EDIT MODE FOR PROFILE INFO')

ConsumerProfilePage.GeneralEditGear_icon().click()

WebUI.delay(2)

WebUI.comment('VERIFY ALL FIELDS ARE ENABLE')

Common.VerifyFieldEditable_func("ContactName", ConsumerProfilePage.ContactName_txt(), "name for testing", "in Edit mode")

Common.VerifyFieldEditable_func("ContactMail", ConsumerProfilePage.ContactEmail_txt(), "fortetsing@email.com", "in Edit mode")

Common.VerifyFieldEditable_func("ContactPhone", ConsumerProfilePage.ContactPhone_txt(), "(123) 456-789", "in Edit mode")

Common.VerifyFieldDisplayed_func("Update button", ConsumerProfilePage.GeneralUpdate_btn())

Common.VerifyFieldDisplayed_func("Cancel button", ConsumerProfilePage.GeneralCancel_btn())



Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
	
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	//WebUI.closeBrowser()
}

