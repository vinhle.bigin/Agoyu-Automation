import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.Inventorymodal
import pages_elements.LandingPage as LandingPage
import pages_elements.LeftMenu as LeftMenu

import objects.MovingPlan as MovingPlan
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal as SignUpModal
import pages_elements.LoginPage as LoginPage

WebUI.openBrowser(GlobalVariable.glb_URL)

WebUI.delay(5)

LandingPage currentpage = new LandingPage()

MovingPlan plan = new MovingPlan()

WebUI.comment('VERIFY Room list modal is shown')

Boolean status = currentpage.ExactWeight_opt().isSelected()

if(status)
{
	Common cm = new Common()
	cm.HandleElementClick(currentpage.ExactWeight_opt())
}

WebUI.comment('VERIFY WHEN UNCHECKED EXACT WEIGHT OPT: BUTTON "Use Agoyu Calculator" NOT shown')

currentpage.InventoryWeight_btn().click()

WebUI.delay(1)

Inventorymodal modal = new Inventorymodal()
 
if(modal.Bedroom_txt() ==null)
{
	GlobalVariable.glb_TCStatus = false
	
	GlobalVariable.glb_TCFailedMessage += "Inventory modal NOT shown when clicking on button Use Agoyu Calculator."
}

Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)


@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
    WebUI.closeBrowser()
}

