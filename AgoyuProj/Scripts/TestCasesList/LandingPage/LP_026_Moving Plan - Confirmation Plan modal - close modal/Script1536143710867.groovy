import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Common

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LandingPage as LandingPage
import pages_elements.LeftMenu as LeftMenu

import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal as SignUpModal
import pages_elements.LoginPage as LoginPage
import pages_elements.Messages
import pages_elements.MovingEstimatePage

WebUI.openBrowser(GlobalVariable.glb_URL)
User user = new User()
WebUI.delay(5)

MovingEstimatePage currentpage = new MovingEstimatePage()
LandingPage landing = new LandingPage()


WebUI.comment('CREATE 1ST PLAN')
user.ProvideMoveInfo_func(user.plan,"Yes")

WebUI.comment('VERIFY MOVING ESITMATE PAGE IS SHOWN AFTER SUBMIT')

Common common_func = new Common()

common_func.WaitForElementDisplay(currentpage.Step1_lbl())

	WebUI.delay(5)
	landing.Logo_img().click()
	WebUI.delay(5)
	
	user.plan.AddrFrom = "2nd St, Indian Rocks Beach, FL 33785, USA"
	user.plan.AddrTo = "123 Commonwealth Avenue, Boston, MA, USA"
	user.plan.TotalWeight = 2000
	
	user.ProvideMoveInfo_func(user.plan,"Yes")
	
	Messages confrm_modal = new Messages()
	 if(confrm_modal.ContLastMove_lnk()!=null)
	 {
		 confrm_modal.Close_btn().click()
		 WebElement element = landing.FromAddress_txt()
		 
		 common_func.VerifyFieldValueEqual_func("Address From", element,user.plan.AddrFrom)
	
		 element = landing.ToAddress_txt()
		 
		 common_func.VerifyFieldValueEqual_func("Address From", element,user.plan.AddrTo)
		 
		 
		 }
	 else
	 {
		 GlobalVariable.glb_TCStatus = false
		 
		 GlobalVariable.glb_TCFailedMessage += "Confirmation Modal does not shown."
	 }
	



Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
    GlobalVariable.glb_TCStatus = true

    GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
	WebUI.closeBrowser()
}

