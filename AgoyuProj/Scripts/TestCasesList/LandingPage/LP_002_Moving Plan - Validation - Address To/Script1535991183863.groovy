import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords

import functions.Navigation

import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LandingPage as LandingPage
import pages_elements.LeftMenu as LeftMenu

import objects.MovingPlan as MovingPlan
import objects.User as User
import org.junit.After as After
import org.junit.Assert as Assert
import org.openqa.selenium.interactions.Actions as Actions
import org.openqa.selenium.interactions.Action as Action
import org.openqa.selenium.WebDriver as WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import pages_elements.SignUpModal as SignUpModal
import pages_elements.LoginPage as LoginPage


Navigation.OpenSite_func()

User user = new User()
user.plan.TotalWeight = 0
WebUI.delay(5)

LandingPage currentpage = new LandingPage()

WebUI.comment('VERIFY VALIDATION FOR INPUT: EMPTY')

String input = ''

user.plan.AddrTo = input

validation_msg = 'The To Address field is required.'

user.ProvideMoveInfo_func(user.plan)

currentpage.TestValidationTo_func(input, validation_msg)


WebUI.comment('VERIFY VALIDATION FOR INPUT: Not Existing username')

input = '123invalid address'

user.plan.AddrFrom = "123 California Street, San Francisco, CA, USA"

user.plan.AddrTo = input

user.plan.TotalWeight = 1000


validation_msg = "Hmm, we can't seem to find that address. Please double check and resubmit"

user.ProvideMoveInfo_func(user.plan)

currentpage.TestValidationTo_func(input, validation_msg)

WebUI.comment('VERIFY VALIDATION FOR INPUT: Valid value: 123 California Street, San Francisco, CA, USA')

input = '123 California Street, San Francisco, CA, USA'

user.plan.AddrFrom = "255 California Street, San Francisco, CA, USA"

validation_msg = ''

user.plan.AddrTo = input

user.ProvideMoveInfo_func(user.plan)

currentpage.TestValidationTo_func(input, validation_msg)


Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus = true

	GlobalVariable.glb_TCFailedMessage = ''
}

@com.kms.katalon.core.annotation.TearDown
void AfterTest() {
   WebUI.closeBrowser()
}

