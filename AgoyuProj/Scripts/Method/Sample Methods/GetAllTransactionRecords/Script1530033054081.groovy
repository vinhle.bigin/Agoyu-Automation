import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import pages_elements.Wallet as Wallet
import java.util.Arrays as Arrays

/* ====Sample of work in table element
WebUI.openBrowser('D:\\\\Katalon Tutorial\\\\Katalon Tutorial\\\\WebTable_Handling_Scenario1.html')

WebUI.maximizeWindow()

WebDriver driver = DriverFactory.getWebDriver()
'Expected value from Table'
String ExpectedValue = "Pay Talk";
'To locate table'
WebElement Table = driver.findElement(By.xpath("//table/tbody"))

'To locate rows of table it will Capture all the rows available in the table'
List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

'Loop will execute for all the rows of the table'
Loop:
for (int row = 0; row < rows_count; row++) {
'To locate columns(cells) of that specific row'
List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

'To calculate no of columns(cells) In that specific row'
int columns_count = Columns_row.size()

println((('Number of cells In Row ' + row) + ' are ') + columns_count)

'Loop will execute till the last cell of that specific row'
for (int column = 0; column < columns_count; column++) {
'It will retrieve text from each cell'
String celltext = Columns_row.get(column).getText()

println((((('Cell Value Of row number ' + row) + ' and column number ') + column) + ' Is ') + celltext)

'Checking if Cell text is matching with the expected value'
if (celltext == ExpectedValue) {
'Getting the Country Name if cell text i.e Company name matches with Expected value'
println('Text present in row number 3 is: ' + Columns_row.get(2).getText())

'After getting the Expected value from Table we will Terminate the loop'
break Loop;
}
}
}
*/
String[] TotalRecordArray = new String[0]

List<WebElement> indexbuttons = null

List<WebElement> rows_table = null

'To locate rows of table it will Capture all the rows available in the table'
Wallet wallet = new Wallet()

wallet.RecordEntries_slct().selectByValue('100')

WebUI.delay(2)

indexbuttons = wallet.PagingIndexes_btns()
int total_index = indexbuttons.size()
println(total_index)

    for(int index =0;index < total_index;index++)
	{
		String []TotalRecordArray_temp = TotalRecordArray
		
		//Get record per one paging
		String[] currentrecordarray = WebUI.callTestCase(findTestCase('Method/Wallets/GetTransactionPerPaging'), [:], FailureHandling.STOP_ON_FAILURE)
		
		//==add records to total list array
		//encrease size of totalarray
		int currentarray_lgth = currentrecordarray.length
		int totaltemp_lgth = TotalRecordArray_temp.length
		
		TotalRecordArray = new String[totaltemp_lgth + currentarray_lgth]
	
		//add the current total temp records
		System.arraycopy(TotalRecordArray_temp, 0, TotalRecordArray, 0, totaltemp_lgth)
	
		//add the total records on current paging to the TotalRecordArray
		System.arraycopy(currentrecordarray, 0, TotalRecordArray, totaltemp_lgth, currentarray_lgth)
	
		int next_idx = index + 1
		if(next_idx!=total_index)
		'Click on paging index button'
		indexbuttons.get(next_idx).click()
	
		WebUI.delay(5) //end for click paging index button	
		}
  
    ///======================
	
    println(TotalRecordArray.length)



