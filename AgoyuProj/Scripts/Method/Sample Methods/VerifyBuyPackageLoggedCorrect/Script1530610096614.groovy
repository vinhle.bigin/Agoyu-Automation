import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import org.junit.After as After
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import pages_elements.Wallet as Wallet
import functions.Convert

/* ====Sample of work in table element
WebUI.openBrowser('D:\\\\Katalon Tutorial\\\\Katalon Tutorial\\\\WebTable_Handling_Scenario1.html')

WebUI.maximizeWindow()

WebDriver driver = DriverFactory.getWebDriver()
'Expected value from Table'
String ExpectedValue = "Pay Talk";
'To locate table'
WebElement Table = driver.findElement(By.xpath("//table/tbody"))

'To locate rows of table it will Capture all the rows available in the table'
List<WebElement> rows_table = Table.findElements(By.tagName('tr'))

'To calculate no of rows In table'
int rows_count = rows_table.size()

'Loop will execute for all the rows of the table'
Loop:
for (int row = 0; row < rows_count; row++) {
'To locate columns(cells) of that specific row'
List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

'To calculate no of columns(cells) In that specific row'
int columns_count = Columns_row.size()

println((('Number of cells In Row ' + row) + ' are ') + columns_count)

'Loop will execute till the last cell of that specific row'
for (int column = 0; column < columns_count; column++) {
'It will retrieve text from each cell'
String celltext = Columns_row.get(column).getText()

println((((('Cell Value Of row number ' + row) + ' and column number ') + column) + ' Is ') + celltext)

'Checking if Cell text is matching with the expected value'
if (celltext == ExpectedValue) {
'Getting the Country Name if cell text i.e Company name matches with Expected value'
println('Text present in row number 3 is: ' + Columns_row.get(2).getText())

'After getting the Expected value from Table we will Terminate the loop'
break Loop;
}
}
}
*/
//cointype_str = BTC, ETH, VEC, USD,BEC
status = false
expected_type_str = ""
expected_details = ""
message_str = ""
String usd_amount = '0.00'

String btc_amount = '0.00000000'

String eth_amount = '0.00000000'

String token_amount = '0.00000000'

if(packageOrder_type=="Buy")
{
	expected_type_str = 'BUY PACKAGE'
	expected_details = "Buy New Package"
}

else
{
	expected_type_str = 'UPGRADE PACKAGE'
	expected_details = "Upgrade Package"
}



String type = order_type

switch (wallet_type_str) {
    case 'BTC':
        btc_amount = new Convert().FormatStringWithDecimal(amount_str, '#0.00000000')

        break
    case 'ETH':
        eth_amount = new Convert().FormatStringWithDecimal(amount_str, '#0.00000000')

        break
    case 'USD':
        usd_amount = new Convert().FormatStringWithDecimal(amount_str, '#0.00')

        break
    default:
        token_amount = new Convert().FormatStringWithDecimal(amount_str, '#0.00000000')

        break
}

amount = "-"+amount_str

String expected_result = ((((((createdAt_str + '|') + expected_type_str) + '|') + amount) + '|') + expected_details)

String []Observed_result = WebUI.callTestCase(findTestCase('Method/Wallets/GetTransactionPerPaging'), [:], FailureHandling.STOP_ON_FAILURE)

for(int i =0;i<Observed_result.length;i++)
{
	if(Observed_result[i]==expected_result)
	{
		status = true
		break
	}	 
}

if (status == false) {
	GlobalVariable.glb_TCStatus = false
	message_str += ((('Wallet - Incorrect Buy/Upgrade Package Log. ObservedResult: ' + Observed_result) + 'ExpectedResult:') + expected_result)
    GlobalVariable.glb_TCFailedMessage += message_str
}

