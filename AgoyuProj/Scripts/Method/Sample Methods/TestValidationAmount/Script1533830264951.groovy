import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User as User
import pages_elements.LeftMenu as LeftMenu
import pages_elements.Messages as Messages
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import pages_elements.ManageCoin
import pages_elements.UserProfilePage
import pages_elements.Wallet
import org.openqa.selenium.WebDriver
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.webui.driver.DriverFactory


Wallet currentpage = new Wallet()



currentpage.TransferAmount_txt().clear()
currentpage.TransferAmount_txt().sendKeys(input_str)

WebDriver driver = DriverFactory.getWebDriver()
((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", currentpage.SubmitTransfer_btn());
WebUI.delay(1)
currentpage.SubmitTransfer_btn().click()
WebUI.delay(2)

if(Expected_msg!="")
{
	if(currentpage.AmountValidation_msg()==null)
	{
		GlobalVariable.glb_TCStatus=false
		GlobalVariable.glb_TCFailedMessage +="[Phone='"+input_str+"']. Validation Amount msg does not displays."
	}
	
	observed_msg = currentpage.AmountValidation_msg().getText()
	
	if(observed_msg!=Expected_msg)
	{
		GlobalVariable.glb_TCStatus=false
		GlobalVariable.glb_TCFailedMessage +="[Amount='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "
		
	}
}

else
{
	if(currentpage.AmountValidation_msg()!=null)
	{
		GlobalVariable.glb_TCStatus=false
		GlobalVariable.glb_TCFailedMessage +="Validation Amount message should not display when input valid amount."		
	}
}

