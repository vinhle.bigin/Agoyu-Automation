import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User
import pages_elements.LandingPage
import pages_elements.Usersetting_menu
import org.junit.Assert as Assert

import org.openqa.selenium.By


User user = new User()

user.Login_func(GlobalVariable.glb_adminuser, GlobalVariable.glb_adminpassword)

Usersetting_menu settingmenu = new Usersetting_menu()

settingmenu.UserSetting_dropdown().click()
settingmenu.AgoyuPortal_mnuitem().click()
WebUI.delay(5)

settingmenu.AdminSetting_dropdown().click()
settingmenu.LogOutAdmin_mnuitem().click()

String login_Text = "Login"

if(settingmenu.UserSetting_dropdown()==null)
{
	GlobalVariable.glb_TCStatus= true
	GlobalVariable.glb_TCFailedMessage+="User has logged out"
}
else
	{
		String Observed = settingmenu.LogIn_mnuitem.getText()
		if(Observed!=login_Text){
			GlobalVariable.glb_TCStatus= false
			GlobalVariable.glb_TCFailedMessage+="  Incorrect topbar menu text.[Observed: "+Observed+"  Expected:  "+login_Text+"]."
		}
	}
	
	Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.TearDown
	void AfterTest() {
	WebUI.closeBrowser()
}


