import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory as CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as MobileBuiltInKeywords
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WSBuiltInKeywords
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUiBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import objects.User
import pages_elements.Usersetting_menu
import org.junit.Assert as Assert

User user = new User()
user.Login_func(GlobalVariable.glb_adminuser, GlobalVariable.glb_adminpassword)

Usersetting_menu settingmenu = new Usersetting_menu()

String AgoyuPortal_menu = 'Agoyu Portal'

if(settingmenu.UserSetting_dropdown()==null)
{
	GlobalVariable.glb_TCStatus= false
	GlobalVariable.glb_TCFailedMessage+="User cannot login successfully"
}
else
	{
		settingmenu.UserSetting_dropdown().click()
		String Observed = settingmenu.AgoyuPortal_mnuitem().getText()
		if(Observed!=AgoyuPortal_menu){
			GlobalVariable.glb_TCStatus= false
			GlobalVariable.glb_TCFailedMessage+="   Incorrect item menu text. [Observed: "+Observed+"   Expected: "+ AgoyuPortal_menu +"]."
		}
	}
	
Assert.assertTrue(GlobalVariable.glb_TCFailedMessage, GlobalVariable.glb_TCStatus)

@com.kms.katalon.core.annotation.SetUp
void SetUp() {
	GlobalVariable.glb_TCStatus=true
	GlobalVariable.glb_TCFailedMessage=""
}

@com.kms.katalon.core.annotation.TearDown
	void AfterTest() {
	WebUI.closeBrowser()
}
