import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.junit.After
import org.testng.Assert

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ManageAgoyuUser

WebUI.callTestCase(findTestCase("Test Cases/Agoyu Portal/ManageAgoyuUser/MU_001_ManageUser page is opened"), [:], FailureHandling.CONTINUE_ON_FAILURE)

ManageAgoyuUser.ResetPasswordIcon().click()

String contentConfirmPopup = "Are you sure you want to reset this password?"
String verifyConfirmPopup = ManageAgoyuUser.confirmPopupStatus().getText()
WebUI.verifyMatch(verifyConfirmPopup, contentConfirmPopup, false)
ManageAgoyuUser.confirmStatus().click()


@com.kms.katalon.core.annotation.TearDown
	void AfterTest() {
	WebUI.closeBrowser()
}
