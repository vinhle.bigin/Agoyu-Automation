import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.ManageAgoyuUser
import org.openqa.selenium.support.ui.Select
import org.testng.Assert

WebUI.callTestCase(findTestCase("Test Cases/Agoyu Portal/ManageAgoyuUser/MU_001_ManageUser page is opened"), [:], FailureHandling.CONTINUE_ON_FAILURE)

ManageAgoyuUser.searchContactName().clear()
ManageAgoyuUser.searchContactName().sendKeys("No data")

Select selectStatus = new Select(ManageAgoyuUser.selectStatus())
selectStatus.selectByVisibleText("Active")

Select selectRole = new Select(ManageAgoyuUser.selectType())
selectRole.selectByVisibleText("AGOYU USER")

ManageAgoyuUser.clickClearbtn().click()

WebUI.delay(5)
String verifyContactNameEmpty = ManageAgoyuUser.searchContactName().getText()
WebUI.verifyMatch(verifyContactNameEmpty, "", true)

String verifyStatusEmty = selectStatus.getFirstSelectedOption().getText()
WebUI.verifyMatch(verifyStatusEmty, "All", true)

String verifyRoleEmty = selectRole.getFirstSelectedOption().getText()
WebUI.verifyMatch(verifyRoleEmty, "Select role", true)



@com.kms.katalon.core.annotation.TearDown
	void AfterTest() {
	WebUI.closeBrowser()
}