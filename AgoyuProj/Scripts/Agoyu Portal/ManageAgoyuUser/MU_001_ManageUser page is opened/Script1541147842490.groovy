import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver

import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import internal.GlobalVariable as GlobalVariable
import pages_elements.LeftMenuAgoyuPortal
import pages_elements.ManageAgoyuUser

WebUI.callTestCase(findTestCase("Test Cases/Agoyu Portal/LoginLogoutAdmin/AA_003_Verify user can go to Admin Profile after clicking Agoyu Portal menu"), [:], FailureHandling.CONTINUE_ON_FAILURE)
WebUI.maximizeWindow()
LeftMenuAgoyuPortal LeftMenuAgoyuPortal = new LeftMenuAgoyuPortal()
LeftMenuAgoyuPortal.openManageAccount().click()

WebUI.delay(2)
LeftMenuAgoyuPortal.openManageAgoyuUsers().click()

WebUI.delay(3)

String verifyAgoyuUserTitle = ManageAgoyuUser.verifyManageAgoyuUserTitle().getText()
WebUI.verifyMatch(verifyAgoyuUserTitle, "Manage Agoyu User", true)

//@com.kms.katalon.core.annotation.TearDown
//	void AfterTest() {
//	WebUI.closeBrowser()
//}