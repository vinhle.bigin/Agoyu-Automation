package functions

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import java.lang.String as String
import java.util.concurrent.TimeoutException
import java.lang.Boolean as Boolen
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.ExpectedCondition
import java.lang.NullPointerException

import pages_elements.LandingPage
import pages_elements.LoginPage
import pages_elements.Messages
import pages_elements.SignUpModal
import pages_elements.Usersetting_menu
import functions.DBCONNECT
import functions.DateTimeFuncs
import groovyjarjarasm.asm.tree.TryCatchBlockNode
import internal.GlobalVariable

import java.sql.ResultSet
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.Channel
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Session.GlobalRequestReply
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Alert
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys

import java.lang.Math as Math

import javax.swing.KeyStroke

import java.lang.Boolean
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.server.handler.SendKeys
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import java.awt.FileDialog
import java.awt.Frame
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.io.File;
import java.io.IOException;
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

public class Navigation {

	public Navigation() {
	}

	
	
	public static void GotoUserProfile_func(){
		Usersetting_menu.UserSetting_dropdown().click()
		WebUI.delay(1)
		Usersetting_menu.Profile_mnuitem().click()
		WebUI.delay(5)
	}
	
	public static void OpenSite_func()
	{
		WebUI.openBrowser(GlobalVariable.glb_URL)
		
		LoginPage.Acceptalert()
		
		WebUI.delay(5)
		
		if(LandingPage.ContinueCookie_btn()!=null)
		{
			LandingPage.ContinueCookie_btn().click()
			WebUI.delay(1)
		}
	}
	
	public static void OpenSignUpModal(String user_type = "Personal") {
		LandingPage currentpage = new LandingPage()

		LoginPage loginpage = new LoginPage()
		WebUI.delay(5)
		currentpage.Login_lnk().click()
		WebUI.delay(2)
		loginpage.Signup_link().click()

		SignUpModal modal = new SignUpModal()

		WebUI.delay(5)

		if(user_type=="Personal") {
			modal.Personal_opt().click()
		}
		else if(user_type=="Business") {
			modal.Business_opt().click()
		}

		else {
			modal.Mover_opt().click()
		}
		WebUI.delay(5)
	}
	
	public static void GotoForgotPass()
	{
		this.OpenSite_func()
		
		LandingPage.Login_lnk().click()
		
		LoginPage.ForgotPass_lnk().click()
		WebUI.delay(5)
	}
}
