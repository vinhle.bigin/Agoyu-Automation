package functions

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import com.kms.katalon.core.util.KeywordUtil
import WebUiBuiltInKeywords as WebUI

import java.time.LocalDateTime
import java.time.ZoneId
import java.time.format.DateTimeFormatter as DateTimeFormatter
import java.lang.String
import java.lang.Boolean as Boolen
import java.util.Date
import java.lang.Math as Math
import java.text.SimpleDateFormat
import java.text.DateFormat
import java.util.TimeZone as TimeZone
import java.lang.Double
import java.text.NumberFormat
import java.text.DecimalFormat
public class Convert {
	public Convert()
	{
		
	}

	public static Double ConvertStringToDouble(String input_str) {

		String temp_str = input_str.replace(",", "")

		Double f = Double.parseDouble(temp_str);

		return f
	}
	
	public static String FormatString(String input_str,String format = "#,###") {
		
				
		
			Double output_str = Double.parseDouble(input_str);
			DecimalFormat formatter = new DecimalFormat(format);
			String result = formatter.format(output_str)
			
				
			}


	public static String FormatStringWithDecimal(String input_str,String decimal_numb) {
		Double d = ConvertStringToDouble(input_str)

		DecimalFormat df = new DecimalFormat(decimal_numb);
		String result = df.format(d)
		return result
	}
}