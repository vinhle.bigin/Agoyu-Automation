
package functions
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import static org.junit.Assert.assertFalse as AssertFalse
import org.junit.Assert as Assert

import internal.GlobalVariable
import net.bytebuddy.implementation.bytecode.Throw
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.WebDriverWait
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import java.lang.String as String
import java.util.concurrent.TimeoutException
import java.lang.Boolean as Boolen
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.Keys
import org.openqa.selenium.support.ui.ExpectedConditions
import org.openqa.selenium.support.ui.ExpectedCondition
import java.lang.NullPointerException
import pages_elements.Messages
import functions.DBCONNECT
import functions.DateTimeFuncs
import groovyjarjarasm.asm.tree.TryCatchBlockNode

import java.sql.ResultSet
import com.jcraft.jsch.ChannelExec
import com.jcraft.jsch.Channel
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.Session;
import com.jcraft.jsch.Session.GlobalRequestReply
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import org.openqa.selenium.Alert
import org.openqa.selenium.By as By
import org.openqa.selenium.Keys

import java.lang.Math as Math

import javax.swing.KeyStroke

import java.lang.Boolean
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.remote.server.handler.SendKeys
import org.openqa.selenium.interactions.Action
import org.openqa.selenium.WebDriver
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory

import java.awt.FileDialog
import java.awt.Frame
import java.awt.Robot
import java.awt.Toolkit
import java.awt.datatransfer.Clipboard
import java.awt.datatransfer.StringSelection
import java.awt.event.KeyEvent
import java.io.File;
import java.io.IOException;
import org.openqa.selenium.Capabilities
import org.openqa.selenium.remote.RemoteWebDriver



public class Common {

	public Common() {
	}


	public static void VerifyWeightFieldText(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		expected_str = Convert.FormatString(expected_str)
		expected_str+=" lbs"

		this.VerifyFieldTextEqual_func(fieldname_str, element_temp,expected_str, note_str)
	}

	public static void VerifyFieldValidation_func(String fieldname_str, WebElement element_temp,String input_str, String Expected_msg) {
		if (Expected_msg != '') {
			if (element_temp == null || element_temp.getText()=="") {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += "[Input: "+input_str+"]Validation message does not displays."
			}

			else {
				this.VerifyFieldTextEqual_func(fieldname_str, element_temp, Expected_msg)
			}
		} else {
			if (element_temp != null && element_temp.getText()!="") {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += "[Input: "+input_str+"]"+fieldname_str+ "Validation message should not display."
			}
		}
	}

	public static void VerifyFieldDisplayed_func(String fieldname_str,WebElement element,String note = "") {

		if(element==null||!element.isDisplayed()){
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "The "+fieldname_str+" Should Displayed "+ note+"."
		}
	}


	public static void VerifyFieldNotDisplayed_func(String fieldname_str,WebElement element,String note = "") {

		if(element!=null && element.isDisplayed()){
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "The "+fieldname_str+" Should NOT Displayed "+ note+"."
		}
	}

	public static void VerifyFieldTextEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {

		String observed = element_temp.getText()

		println(observed)

		if(observed!=expected_str) {
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+" Text should EQUAL.[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+ "."
		}
	}
	public static void VerifyFieldTextNotEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		String observed = element_temp.getText()

		println(observed)

		if(observed==expected_str) {
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+" Text should NOT EQUAL.[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+ "."
		}
	}


	public static void VerifyFieldValueEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		String observed = element_temp.getAttribute("value")

		println(observed)

		if(observed!=expected_str) {
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+" Value should EQUAL.[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+"."
		}
	}
	public static void VerifyFieldValueNotEqual_func(String fieldname_str, WebElement element_temp,String expected_str,String note_str= "") {
		String observed = element_temp.getAttribute("value")

		println(observed)

		if(observed!=expected_str) {
			GlobalVariable.glb_TCStatus = false

			GlobalVariable.glb_TCFailedMessage += fieldname_str+".[Observed: "+observed+"- Expected:"+expected_str+"] "+note_str+"."
		}
	}


	public static void MacUploadFile_func(String file_path) {

		String exec_str =""

		exec_str += "tell application \"Firefox\"\n"
		exec_str += "activate \"FireFox\"\n"
		exec_str +="end tell\n"
		exec_str +="tell application \"System Events\"\n"
		exec_str +="keystroke \"G\" using {command down, shift down}\n"
		exec_str +="keystroke \""+file_path+"\"\n"
		//exec_str +="keystroke \"/Users/bigin1/Documents/Agoyu-Automation/AgoyuProj/Data Files/logo4M.jpg\"\n"

		exec_str +="keystroke return\n"
		exec_str +="delay 2\n"
		exec_str +="keystroke return\n"
		exec_str +="delay 2\n"
		exec_str +="end tell\n"

		println(exec_str)

		String []Run_str = new String[3]

		Run_str[0] = "osascript"
		Run_str[1] = "-e"
		Run_str[2] = exec_str

		println ("Start to upload image in MAC")

		Runtime rtime = Runtime.getRuntime();
		Process prcess = rtime.exec(Run_str)

		println ("Completed to upload image in MAC")

	}

	public static void UploadFile_func(WebElement upload_btn, String file_path) {
		WebElement element_tmp = upload_btn

		String file = file_path

		String container = File.separator+ "Data Files"+File.separator

		String workingDirectory = System.getProperty("user.dir")+container+file

		element_tmp.click()
		Thread.sleep(2000)
		Clipboard clipboard = Toolkit.getDefaultToolkit().getSystemClipboard()

		StringSelection string_slect = new StringSelection( workingDirectory )

		clipboard.setContents(string_slect, string_slect)
		WebDriver driver = DriverFactory.getWebDriver()

		if(this.GetOS_func().toString().contains("mac")) {

			MacUploadFile_func(workingDirectory)

			/*
			 println ("Start to upload image in MAC")
			 Runtime.getRuntime().exec("osascript "+"/Users/bigin1/Documents/macupload_valid.scpt")
			 Thread.sleep(2000)
			 println ("Completed to upload image in MAC")
			 */
		}
		else{
			Robot rbot = new Robot()
			rbot.keyPress(KeyEvent.VK_CONTROL)
			rbot.keyPress(KeyEvent.VK_V)
			Thread.sleep(1000)
			rbot.keyRelease(KeyEvent.VK_V);
			rbot.keyRelease(KeyEvent.VK_CONTROL);
			Thread.sleep(2000)

			rbot.keyPress(KeyEvent.VK_ENTER);
			rbot.keyRelease(KeyEvent.VK_ENTER);
			Thread.sleep(2000)
		}


		//KeyStroke keystroke = KeyStroke.getKeyStroke(KeyEvent.VK_V)


	}

	public List<String > GetTableRecordsPerPaging_func(WebElement TableElement) {
		List<WebElement> rows_table = null;
		WebElement table = TableElement
		'To locate rows of table it will Capture all the rows available in the table'

		WebUI.delay(2)

		rows_table = table.findElements(By.tagName('tr'))

		int rows_count = rows_table.size()

		println(rows_table.size())
		if(rows_count==0) {
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Package Report: No record found."
			return
		}

		//for per one row
		List<String > list = new ArrayList<>()

		for (int row = 0; row < rows_count; row++) {
			'To locate columns(cells) of that specific row'
			List<WebElement> Columns_row = rows_table.get(row).findElements(By.tagName('td'))

			int column_size = Columns_row.size()

			String cur_record =""
			for(int column = 0; column <column_size;column++)
			{
				String text = Columns_row.get(column).getText()

				cur_record+=Columns_row.get(column).getText()

				if(column<column_size-1)
					cur_record+="|"


			}//end for column

			list.add(cur_record)

		}//end for get row record

		//==add records to total list array
		return list
	}

	public static void FilterTableRecord_func(WebElement Filter_txt,WebElement submit_btn,String input_str){
		WebElement textfield = Filter_txt
		WebElement submit = submit_btn

		String input_temp = input_str

		textfield.sendKeys(input_temp)

		if(submit!=null)
		{
			submit.click()
		}
		WebUI.delay(2)
	}

	public static void waitForLoad() {

		WebDriver driver = DriverFactory.getWebDriver()
		ExpectedCondition<Boolean> pageLoadCondition = new
				ExpectedCondition<Boolean>() {
					public Boolean apply(WebDriver driver_temp) {
						return ((JavascriptExecutor)driver_temp).executeScript("return document.readyState").equals("complete");
					}
				};
		WebDriverWait wait = new WebDriverWait(driver, 30)
		wait.until(pageLoadCondition)
	}

	public static void WaitForElementDisplay(WebElement element) {
		WebDriver driver = DriverFactory.getWebDriver()
		WebDriverWait wait = new WebDriverWait(driver, 30);  // timeout of 15 seconds

		for(int i =0;i<10;i++)
		{
			try {
				wait.until(ExpectedConditions.visibilityOf(element))
				if(element.isDisplayed())
					break
				else
					WebUI.delay(2)
			}
			catch (NullPointerException t) {
				WebUI.delay(2)
			}
		}
	}

	public static void ClearServercache() {
		Messages msg = new Messages()

		if(msg.Servercache_msg()!=null)
		{

			String pathfolder = "cd ../"+GlobalVariable.glb_Serverhomefolder
			//====================================================
			WebUI.comment('Run BE JOB')
			DBCONNECT dbconnection = new DBCONNECT()
			dbconnection.OpenSSHSessionWithKey()
			int index=0
			ChannelExec channel= (ChannelExec)dbconnection.session.openChannel("exec");
			InputStream in_temp = channel.getInputStream();
			channel.setCommand(pathfolder)
			channel.connect();
			channel.disconnect()

			channel= (ChannelExec)dbconnection.session.openChannel("exec");
			in_temp = channel.getInputStream();
			channel.setCommand("er")
			channel.connect();
			channel.disconnect()

			dbconnection.session.disconnect()
			WebUI.delay(60)
			WebUI.refresh()

		}//end if
	}

	public static void HandleElementClick(WebElement element)
	{
		try{
			element.click()
		}
		catch (Exception e)
		{
			WebDriver driver = DriverFactory.getWebDriver()
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			((driver) as JavascriptExecutor).executeScript("arguments[0].click();", element)
		}//end catch

	}

	public static void ActionSendkey(WebElement element,String input_str)
	{
		WebDriver webDriver = DriverFactory.getWebDriver();
		Actions actions = new Actions(webDriver);
		((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", element);
		Action action
		for(int i =0;i<6;i++)
		{
			action = actions.click(element).doubleClick(element).sendKeys(Keys.BACK_SPACE).build();
			action.perform()
		}

		action = actions.sendKeys(element,input_str).build();
		action.perform()

	}

	public TestData GetFileData(String filename)
	{
		TestData  data = TestDataFactory.findTestData("Data Files/"+filename)
		return data
	}

	public static Boolean IsTextFieldEditable_func(WebElement e,String value_str) {
		WebDriver webDriver = DriverFactory.getWebDriver();

		//	((JavascriptExecutor) webDriver).executeScript("arguments[0].scrollIntoView(true);", e);

		Actions actions = new Actions(webDriver);

		this.ActionSendkey(e,value_str)

		if(e.getAttribute("value").contains(value_str)) {
			return true
		}
		return false
	}

	public static void VerifyFieldEditable_func(String fieldname, WebElement e,String value_str,String note="")
	{
		Boolean status = this.IsTextFieldEditable_func(e,value_str)

		if(!status)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += fieldname+" SHOULD EDITABLE "+note+"."
		}

	}

	public static void VerifyFieldNotEditable_func(String fieldname, WebElement e,String value_str,String note="")
	{
		Boolean status = this.IsTextFieldEditable_func(e,value_str)

		if(status)
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += fieldname+" SHOULD NOT EDITABLE "+note+"."
		}

	}

	public static String GetOS_func()
	{
		String os = System.getProperty("os.name").toLowerCase()
		println(os)
		return os
	}



}