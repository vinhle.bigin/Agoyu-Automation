package objects
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import functions.DateTimeFuncs
import java.sql.ResultSet
import functions.DBCONNECT
import functions.Convert
import functions.DateTimeFuncs


class Mail {

	public String AddressFrom
	public List<String> AddressTo
	public String Subject
	public String Content
	public String activatelink
	public String resetlink
	public Date sentDate
	public String contactname

	public Mail(){
		AddressFrom = "[Agoyu Support <support@agoyu.com>]"
		AddressTo = new ArrayList<>()
		Subject = ""
		Content = ""

		DateTimeFuncs dt = new DateTimeFuncs()
		sentDate = dt.GetLocalDatetime()
	}

	//METHOD
	public void GetActivateLink()
	{
		//<a href="https://agoyu-dev.bigin.top/auth/activate/6/5BlMOqMItoOw5008NfgQkt0Pb4Jiqrf0" style="color: #ffffff; text-decoration: none;">Active your email
		String temp_1 = GlobalVariable.glb_URL+ "/auth/activate"
		String temp_2 = "Active your email"

		int index_1 = this.Content.indexOf(temp_1)
		int index_2 = this.Content.indexOf(temp_2)

		String temp_str =  this.Content.substring(index_1, index_2)


		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")
		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")

		//OR can use this regex temp_str = temp_str.replaceAll(" style.+\$","")
		int index3 =  temp_str.indexOf(" style")
		temp_str =  temp_str.substring(0, index3)
		this.activatelink = temp_str

	}


	public void GetResetPassLink()
	{
		//<a href="https://agoyu-dev.bigin.top/auth/activate/6/5BlMOqMItoOw5008NfgQkt0Pb4Jiqrf0" style="color: #ffffff; text-decoration: none;">Active your email
		String temp_1 = GlobalVariable.glb_URL+ "/auth/reset"
		String temp_2 = "Reset Your Password"

		int index_1 = this.Content.indexOf(temp_1)
		int index_2 = this.Content.indexOf(temp_2)

		String temp_str =  this.Content.substring(index_1, index_2)


		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")
		temp_str = temp_str.replace("<a href=\"","")
		temp_str = temp_str.replace("\"","")

		//OR can use this regex temp_str = temp_str.replaceAll(" style.+\$","")
		int index3 =  temp_str.indexOf(" style")
		temp_str =  temp_str.substring(0, index3)
		this.activatelink = temp_str

	}

	public void VerifyActivateEmail()
	{

		String heading_content = "Agoyu Confirmation"

		String body_content = "In order to finish your registration you need to confirm your email address. It's easy - just click the Confirm Your Email link below."

		String active_lnk = GlobalVariable.glb_URL+ "/auth/activate"

		String active_lbl ="Active your email"


		if(!this.Content.contains(heading_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Confirmation text.[Observed:"+this.Content +"-Expected:"+heading_content+"]."
		}

		if(!this.Content.contains(body_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"]."
		}

		if(!this.Content.contains(active_lnk))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Activate link.[Observed:"+this.Content +"-Expected:"+active_lnk+"]."
		}


		if(!this.Content.contains(active_lbl))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Activate text.[Observed:"+this.Content +"-Expected:"+active_lbl+"]."
		}

	}

	public void VerifyWelcomeEmail()
	{

		String heading_content = "Congratulations!"

		String body_content = "You’re now part of a community that connects regular people like you and me, with the world’s most recognized moving companies across the world. Find a moving estimate that suits your budget. And discover techniques to save money during the process."


		if(!this.Content.contains(heading_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage = "Incorrect Welcome text.[Observed:"+this.Content +"-Expected:"+heading_content+"]."
		}

		if(!this.Content.contains(body_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage = "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"]."
		}


	}


	public void VerifyResetAccountEmail()
	{
		String heading_content = "Agoyu Confirmation"

		String body_content = "We've received a request to reset your password. You can reset your password by using the link below. If you didn't request this, you can safely ignore this email."

		String reset_lnk = GlobalVariable.glb_URL+ "/auth/reset"


		String reset_lbl ="Reset your password"


		if(!this.Content.contains(heading_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Confirmation text.[Observed:"+this.Content +"-Expected:"+heading_content+"]."
		}

		if(!this.Content.contains(body_content))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect main text.[Observed:"+this.Content +"-Expected:"+body_content+"]."
		}

		if(!this.Content.contains(reset_lnk))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Reset link.[Observed:"+this.Content +"-Expected:"+reset_lnk+"]."
		}


		if(!this.Content.contains(reset_lbl))
		{
			GlobalVariable.glb_TCStatus = false
			GlobalVariable.glb_TCFailedMessage += "Incorrect Reset text.[Observed:"+this.Content +"-Expected:"+reset_lbl+"]."
		}
	}


}