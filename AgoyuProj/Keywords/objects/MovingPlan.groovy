package objects
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable

import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil

import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import functions.DateTimeFuncs

import java.awt.image.DataBufferDouble
import java.sql.ResultSet
import functions.DBCONNECT
import functions.Convert
import objects.WeightConfig
import java.lang.Integer

class MovingPlan {
	public String AddrFrom
	public String AddrTo
	public String ZipcodeFrom
	public String ZipcodeTo
	public int TotalWeight
	public int Bedroom
	public Double Bathroom
	public int Livingroom
	public int Diningroom
	public int Kitchen
	public int Laundry
	public int Garage
	public int AddtnalStorage
	public int Basement
	public int StorageShed
	public int Patio
	public String MovingDate
	public Boolean ExactWeight = true
	public int TotalMile
	public int CWT
	public Double OLF
	public Double DLF
	public Double SH
	public Double TransportCharges
	public Double FuelSurcharge
	public Double FuelSurchargeRate
	public Double IRRSurcharge
	public Double OriginSurcharge
	public Double DestSurcharge
	public Double FinalCharge
	public int BTH
	public int ServiceCode

	/*
	public MovingPlan(){

		//AddrFrom = "123 California Street, San Francisco, CA, USA"
		//AddrTo = "2nd St, Indian Rocks Beach, FL 33785, USA"
		this.GetAddress(20)
		ZipcodeFrom = ""
		ZipcodeTo = ""
		TotalWeight = 1000
		Bedroom = 0
		Bathroom = 0
		Livingroom = 0
		Diningroom = 0
		Kitchen = 0
		Laundry = 0
		Garage = 0
		AddtnalStorage = 0
		Basement = 0
		StorageShed = 0
		Patio = 0
		DateTimeFuncs dt = new DateTimeFuncs()
		dt.GetServerDateTime()
		MovingDate = dt.FormatDate(dt.currentDate,"MM/dd/yyyy")
		ExactWeight = true
		//TotalMile = 20
		FuelSurchargeRate = 0.09
	}
	*/
	public MovingPlan(Boolean ExactWeight_temp = true){
		
				//AddrFrom = "123 California Street, San Francisco, CA, USA"
				//AddrTo = "2nd St, Indian Rocks Beach, FL 33785, USA"
				this.GetAddress(20)
				if(ExactWeight_temp==true)
				{
					ZipcodeFrom = ""
					ZipcodeTo = ""
					TotalWeight = 1000
					Bedroom = 0
					Bathroom = 0
					Livingroom = 0
					Diningroom = 0
					Kitchen = 0
					Laundry = 0
					Garage = 0
					AddtnalStorage = 0
					Basement = 0
					StorageShed = 0
					Patio = 0
					this.ExactWeight = true
				}
				
				else{
				//TotalWeight = 1000
				Bedroom = 2
				Bathroom = 1
				Livingroom = 1
				Diningroom = 0
				Kitchen = 0
				Laundry = 0
				Garage = 0
				AddtnalStorage = 0
				Basement = 0
				StorageShed = 0
				Patio = 0
				
				this.ExactWeight = false
				//TotalMile = 20
				
				
				}//else
				DateTimeFuncs dt = new DateTimeFuncs()
				dt.GetServerDateTime()
				MovingDate = dt.FormatDate(dt.currentDate,"MM/dd/yyyy")
				FuelSurchargeRate = 0.09
			}
	
	

	//METHOD
	public void CalculateTotalCustomWeight()
	{
		WeightConfig config = new WeightConfig()

		int t_Bed = this.Bedroom*config.Bed1
		Double t_Bath = this.Bathroom*config.Bath0_5/0.5
		int t_Livingroom = this.Livingroom*config.Living1
		int t_Diningroom = this.Diningroom*config.Dining1
		int t_Kitchen = this.Kitchen*config.Kitchen1
		int t_Laundry = this.Laundry*config.Laundry1
		int t_Garage = this.Garage*config.Garage1
		int t_AddtnalStorage = this.AddtnalStorage*config.AddtnalStorage1
		int t_Basement = this.Basement*config.Basement1
		int t_StorageShed = this.StorageShed*config.StorageShed1
		int t_Patio = this.Patio*config.Patio1

		this.TotalWeight = t_Bed+ t_Bath +t_Livingroom + t_Diningroom +t_Kitchen +t_Laundry +t_Garage +t_AddtnalStorage +t_Basement +t_StorageShed +t_Patio

	}

	public String GetServiceCode(String Zip) {

		String temp_service
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()

		ResultSet rs = dbconnection.stmt.executeQuery("SELECT service_area FROM eden_linehaul_city_reference where zip_included = '"+Zip+"'");

		while (rs.next()) {
			temp_service = rs.getString("service_area")

		}
		rs.close()
		dbconnection.CloseAllConnect()

	}

	public void GetBTH() {

		TestData data = TestDataFactory.findTestData("Data Files/LineHaul")

		int weight_colstart = 3
		int weight_colend  = 7
		int weight_rowstart = 1
		int weight_rowsend = 2
		int mile_colstart = 1
		int mile_colend  = 2
		int mile_rowstart = 3
		int mile_rowsend = 8

		int value_col = weight_colend
		int value_row = mile_rowsend

		int maxweight = int.parseInt(data.getValue(weight_colend,weight_rowsend))
		int maxmile = int.parseInt(data.getValue(mile_colend,mile_rowsend))
		//Get value column index

		for(int i =weight_colstart;i<=weight_colend;i++)
		{
			int temp_weightstart = int.parseInt(data.getValue(i,weight_rowstart))
			int temp_weightend = int.parseInt(data.getValue(i,weight_rowsend))

			if(	this.TotalWeight>=temp_weightstart
			&&this.TotalWeight<=temp_weightend)
			{
				value_col = i
				break
			}//end if

			//Get value row index
		}//end for

		for(i = mile_rowstart;i<=mile_rowsend;i++)
		{
			int temp_milestart = int.parseInt(data.getValue(mile_colstart,i))
			int temp_miletend = int.parseInt(data.getValue(mile_colend,i))

			if(	this.TotalWeight>= temp_milestart
			&&this.TotalWeight<=temp_miletend)
			{
				value_row = i
				break
			}//end if

		}//end for

		int temp_vlue = int.parseInt(data.getValue(value_col,value_row))

		if(this.TotalWeight>maxweight)
		{
			int add_vlue1 = int.parseInt(data.getValue(value_col+1,value_row))
			int dif_cwtperweight = (this.TotalWeight - maxweight)/100
			temp_vlue += (dif_cwtperweight*add_vlue1)
		}

		if(this.TotalMile>maxmile)
		{
			int add_vlue2 = int.parseInt(data.getValue(value_col,value_row+1))
			int dif_numpermile = (this.TotalMile - maxmile)/100
			temp_vlue += (dif_numpermile*add_vlue2)
		}

		this.BTH = temp_vlue
	}

	public void CalCWT() {

		int temp = this.TotalWeight/100
		this.CWT = temp
	}

	public Double GetOLF_DLF(String zip) {

		this.ServiceCode = this.GetServiceCode(zip)
		int col_refer = 3

		TestData data = TestDataFactory.findTestData("Data Files/Geographical Schedule")
		int rownumb = data.getRowNumbers()
		int from_col = 2
		String temp =""

		for(int i =1;i<=rownumb;i++)
		{
			if(this.ServiceCode==data.getValue(1,i))
			{
				temp = data.getValue(from_col,i )
				break
			}
		}//end for
		Double rateperCWT = Double.parseDouble(temp)
		return rateperCWT

	}

	public Double GetOLF_DLFSurcharge(String zip) {

		if(this.ServiceCode==0)
			this.ServiceCode = this.GetServiceCode(zip)
		int col_refer = 4

		TestData data = TestDataFactory.findTestData("Data Files/Geographical Schedule")
		int rownumb = data.getRowNumbers()
		int from_col = 2
		String temp =""

		for(int i =1;i<=rownumb;i++)
		{
			if(this.ServiceCode==data.getValue(1,i))
			{
				temp = data.getValue(from_col,i )
				break
			}
		}//end for
		Double rateperCWT = Double.parseDouble(temp)
		return rateperCWT

	}

	public void CalOLF() {

		Double rateperCWT = GetOLF_DLF(this.ZipcodeFrom)
		this.OLF = rateperCWT*this.CWT
	}

	public void CalDLF() {

		Double rateperCWT = GetOLF_DLF(this.ZipcodeTo)
		this.OLF = rateperCWT*this.CWT
	}

	public void GetSH() {

		Double expect_numb = this.CWT*this.TotalMile

		Double get_value
		TestData data = TestDataFactory.findTestData("Data Files/Additional Rates")
		int rownumb = data.getRowNumbers()

		int index_loop = 0

		int from_col = 2

		Double refer_temp = Double.parseDouble(data.getValue(2,(rownumb)))

		get_value =  Double.parseDouble(data.getValue(3,(rownumb+index_loop-1)))

		while(index_loop<rownumb)
		{

			if(expect_numb>refer_temp)
			{
				index_loop = rownumb
			}
			else{
				rownumb = rownumb/2
			}
			refer_temp = Double.parseDouble(data.getValue(2,(rownumb+index_loop)))

			get_value =  Double.parseDouble(data.getValue(3,(rownumb+index_loop)))
		}

		this.SH = get_value
	}

	public void CalTransportCharges() {

		Double temp = this.BTH + this.OLF + this.DLF + this.SH
		this.TransportCharges = temp
	}

	public void CalFuelCharge() {

		Double temp = this.TransportCharges * this.FuelSurchargeRate
		this.FuelSurcharge = temp
	}

	public void CalIRRSurcharge() {

		Double temp = this.TransportCharges * 0.04
		this.IRRSurcharge = temp
	}

	public void CalOriginSurcharge() {

		Double OriginServiceCharge = GetOLF_DLFSurcharge(this.ZipcodeFrom)
		Double temp = this.CWT * OriginServiceCharge
		this.OriginSurcharge = temp
	}

	public void CalDestSurcharge() {

		Double DestServiceCharge =GetOLF_DLFSurcharge(this.ZipcodeTo)
		Double temp = this.CWT * DestServiceCharge
		this.DestSurcharge = temp
	}

	public void getCalFinalCharge() {

		//1
		this.GetBTH()
		//2
		this.CalCWT()
		//3
		this.CalDLF()
		//4
		this.CalOLF()
		//5
		this.GetSH()
		//6
		this.CalTransportCharges()
		//7
		this.CalFuelCharge()
		//8
		this.CalIRRSurcharge()
		//9
		this.CalOriginSurcharge()
		//10
		this.CalDestSurcharge()

		this.FinalCharge = this.TransportCharges+this.FuelSurcharge+this.IRRSurcharge+this.OriginSurcharge+this.DestSurcharge
	}

	public void GetAddress(int miles)
	{
		TestData data = TestDataFactory.findTestData("Data Files/AddressList")
		int rownumb = data.getRowNumbers()
		int from_col = 2
		int to_col = 3
		int mile_col = 6


		for(int i =1;i<=rownumb;i++)
		{
			//println(data.getValue(6,i).toString())
			Integer mile_temp = Integer.parseInt(data.getValue(6,i).toString())

			if(miles<=mile_temp)
			{
				this.AddrFrom = data.getValue(from_col,i )
				this.AddrTo = data.getValue(to_col,i )
				this.TotalMile = Integer.parseInt(data.getValue(mile_col,i ))
				break
			}
		}//end for

	}//end func

}