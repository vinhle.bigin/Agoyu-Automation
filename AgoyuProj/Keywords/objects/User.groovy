package objects
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords

import internal.GlobalVariable
import pages_elements.ConsumerProfilePage
import pages_elements.Facebook
import pages_elements.GmailLogin
import pages_elements.Inventorymodal
import pages_elements.LandingPage
import pages_elements.LoginPage
import pages_elements.Messages
import pages_elements.MovingEstimatePage
import pages_elements.ResetPasswordPage
import pages_elements.SignUpModal
import pages_elements.Usersetting_menu
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.ElementClickInterceptedException
import org.openqa.selenium.JavascriptExecutor

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import com.kms.katalon.core.webui.exception.BrowserNotOpenedException
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import functions.DateTimeFuncs
import functions.Navigation

import java.sql.ResultSet
import functions.DBCONNECT
import functions.Common
import functions.Convert
import org.openqa.selenium.Keys

class User {
	public int generation =0
	public String Id = ""
	public String userId = ""
	public String contactName = ""
	public String businessName = ""
	public String username = ""
	public String email = ""
	public String password = ""
	public String phone = ""
	public String role = ""
	public String birthday = ""
	public String passpord = ""
	public String address1 = ""
	public String address2 = ""
	public String city = ""
	public String state = ""
	public String postalcode = ""
	DateTimeFuncs dt = new DateTimeFuncs()
	public String logoname = ""
	public String userType
	public MovingPlan plan
	public String bsinessPhone = ""
	public String bsinessEmail = ""
	public String bsinessWeb = ""
	public String bsinessDescr = ""


	public User(){
		Random r = new Random();
		int ret2 = r.nextInt(100)+1;
		int ret = r.nextInt(100)+1;
		userId = ''
		businessName = 'testuser'+ dt.getCurrentLocalTime()+(ret+ret2).toString()
		username = businessName
		contactName = "Contact "+dt.getCurrentLocalTime()+(ret+ret2).toString()
		//businessName = 'Moving Extream'
		password = 'Password1'
		phone = '1234567891'
		role = 'User'
		email = username+'@bg.vn'
		birthday = '1987-12-04'
		passpord = '358738'
		address1 = '20 Wall Street'
		address2 = '50 High Road'
		city = 'HCM'
		state = 'testing_state'
		postalcode = '3832'
		Id =0
		//logoname = "logo4M.jpg"
		logoname = ""
		userType = "Personal"
		plan = new MovingPlan()
	}

	//METHOD
	public void SignOut_func()
	{
		//Not Yet implement
	}

	public void ChangeAddrEstimatePage_func(MovingPlan plan, String submit = "Yes")
	{
		MovingEstimatePage.AddrFrom_txt().clear()
		
		MovingEstimatePage.AddrTo_txt().clear()
		
		MovingEstimatePage.AddrFrom_txt().sendKeys(plan.AddrFrom)
		
		MovingEstimatePage.AddrTo_txt().sendKeys(plan.AddrTo)
		
		if(submit =="Yes")
		{
			MovingEstimatePage.MoveUpdate_btn().click()
		}
		else if(submit =="No")
		{
			MovingEstimatePage.MoveCancel_btn().click()
		}
	}

	public void UpdateBusinessProfile_func(Boolean submit = true)
	{
		this.ChangeBusinessInfo_func()

		ConsumerProfilePage.BsinessName_txt().clear()
		ConsumerProfilePage.BsinessName_txt().sendKeys(this.businessName)

		ConsumerProfilePage.BsinessPhone_txt().clear()
		ConsumerProfilePage.BsinessPhone_txt().sendKeys(this.bsinessPhone)

		ConsumerProfilePage.BsinessEmail_txt().clear()
		ConsumerProfilePage.BsinessEmail_txt().sendKeys(this.bsinessEmail)

		ConsumerProfilePage.BsinessWeb_txt().clear()
		ConsumerProfilePage.BsinessWeb_txt().sendKeys(this.bsinessWeb)

		ConsumerProfilePage.BsinessDescr_txt().clear()
		ConsumerProfilePage.BsinessDescr_txt().sendKeys(this.bsinessDescr)


		ConsumerProfilePage.Address_txt().clear()
		ConsumerProfilePage.Address_txt().sendKeys(this.address1)

		ConsumerProfilePage.City_txt().clear()
		ConsumerProfilePage.City_txt().sendKeys(this.city)

		ConsumerProfilePage.State_txt().selectByValue(this.bsinessDescr)

		ConsumerProfilePage.Zip_txt().clear()
		ConsumerProfilePage.Zip_txt().sendKeys(this.postalcode)

		//Common.UploadFile_func(ConsumerProfilePage.AvatarUpload_btn(), this.logoname)

		if(submit == true)
			ConsumerProfilePage.BsinessUpdate_btn().click()
		else if(submit == false)
			ConsumerProfilePage.BsinessCancel_btn().click()
		WebUI.delay(2)
	}

	public void UpdateConsumerProfile_func(Boolean submit = true)
	{
		this.ChangeUserInfo()

		ConsumerProfilePage.ContactName_txt().clear()
		ConsumerProfilePage.ContactName_txt().sendKeys(this.contactName)

		ConsumerProfilePage.ContactPhone_txt().clear()
		ConsumerProfilePage.ContactPhone_txt().sendKeys(this.phone)

		ConsumerProfilePage.ContactEmail_txt().clear()
		ConsumerProfilePage.ContactEmail_txt().sendKeys(this.email)

		//Common.UploadFile_func(ConsumerProfilePage.AvatarUpload_btn(), this.logoname)

		if(submit == true)
			ConsumerProfilePage.GeneralUpdate_btn().click()
		else if(submit == false)
			ConsumerProfilePage.GeneralCancel_btn().click()
		WebUI.delay(2)
	}


	public static void LoginGmail_func(String email, String password_str)
	{
		GmailLogin.GmailLogin_func(email, password_str)
	}

	public static void LoginFacebook_func(String email, String password_str)
	{
		Facebook.LoginFacebook_func(email, password_str)
	}

	public void LoginBack_func(String username_str, String password_str)
	{
		Usersetting_menu usersettingmenu = new Usersetting_menu()
		LoginPage loginpage = new LoginPage()

		WebUI.comment('Logout\r\n')

		try{
			usersettingmenu.UserSetting_dropdown().click()
			WebUI.delay(3)
		}
		catch(ElementClickInterceptedException e) {
			WebDriver driver = DriverFactory.getWebDriver()

			((JavascriptExecutor) driver).executeScript("arguments[0].click();", usersettingmenu.UserSetting_dropdown());
			WebUI.delay(3)
		}
		WebUI.delay(3)

		//usersettingmenu.SettingGear_icon().click()
		//WebUI.click(findTestObject('Common/SettingGear_icon'))

		WebUI.delay(1)

		usersettingmenu.LogOut_mnuitem().click()
		//WebUI.click(findTestObject('Common/LogOut_mnuitem'))

		WebUI.delay(5)

		WebUI.comment('Signin back\r\n')

		loginpage.Username_txt().sendKeys(username_str)

		loginpage.Password_txt().sendKeys(password_str)

		loginpage.SignIn_btn().click()

		WebUI.delay(5)
	}

	public void Login_func(String email = this.email, String password = this.password)
	{
		LandingPage landingpage = new LandingPage()
		LoginPage loginpage = new LoginPage()

		if(loginpage.SignIn_btn()==null)
		{
			WebUI.openBrowser(GlobalVariable.glb_URL)
			WebUI.delay(5)
		}

		landingpage.Login_lnk().click()

		WebUI.delay(2)

		loginpage.Username_txt().sendKeys(email)

		loginpage.Password_txt().sendKeys(password)

		loginpage.SignIn_btn().click()

		loginpage.Acceptalert()

		WebUI.delay(5)
		//WebUI.delay(10)
		Usersetting_menu user_mnue = new Usersetting_menu()
		Common cm = new Common()
		Messages msg = new Messages()
		if(msg.Servercache_msg()!=null)
		{
			cm.ClearServercache()
			WebUI.closeBrowser()
			try{
				if(loginpage.SignIn_btn()==null)
					WebUI.openBrowser(GlobalVariable.glb_URL)
			}
			catch(BrowserNotOpenedException e)
			{
				WebUI.openBrowser(GlobalVariable.glb_URL)
			}
			loginpage.Username_txt().sendKeys(email)
			loginpage.Password_txt().sendKeys(password)
			loginpage.SignIn_btn().click()
			loginpage.Acceptalert()
		}
		cm.WaitForElementDisplay(user_mnue.UserSetting_dropdown())
	}

	public void SignUpUser_func(Boolean openbrowser = true)
	{
		Common cm

		LoginPage loginpage

		if (openbrowser == true) {
			WebUI.openBrowser(GlobalVariable.glb_URL)
			WebUI.delay(5)
			WebUI.maximizeWindow()
			WebUI.delay(1)
			LandingPage landing  = new LandingPage()

			if(landing.ContinueCookie_btn()!=null)
			{
				landing.ContinueCookie_btn().click()
				WebUI.delay(1)
			}
			cm = new Common()

			cm.ClearServercache()
		}

		SignUpModal signupmdal = new SignUpModal()

		if ((signupmdal.BsnesBack_lnk() == null) && (signupmdal.PsnalBack_lnk() == null)) {
			Navigation.OpenSignUpModal()
			WebUI.delay(5)

		}

		WebUI.comment('Fill user info')

		WebDriver driver = DriverFactory.getWebDriver()

		if (this.userType == 'Personal') {
			//signupmdal.Personal_opt().click()
			//WebUI.delay(2)
			signupmdal.PsnalContactName_txt().clear()

			signupmdal.PsnalContactName_txt().sendKeys(this.contactName)

			signupmdal.PsnalPhone_txt().clear()

			signupmdal.PsnalPhone_txt().sendKeys(this.phone)

			signupmdal.PsnalEmail_txt().clear()

			signupmdal.PsnalEmail_txt().sendKeys(this.email)

			signupmdal.PsnalPassword_txt().clear()

			signupmdal.PsnalPassword_txt().sendKeys(this.password)

			signupmdal.PsnalPasswordAgain_txt().clear()

			signupmdal.PsnalPasswordAgain_txt().sendKeys(this.password)

			((driver) as JavascriptExecutor).executeScript('arguments[0].click();', signupmdal.PsnalTermAccept_chk())

			//signupmdal.PsnalTermAccept_chk().click()
			signupmdal.PsnalSubmit_btn().click()

			WebUI.delay(3) //signupmdal.Business_opt().click()
			//WebUI.delay(2)
			//signupmdal.BsnesTermAccept_chk().click()
		} else if (this.userType == 'Business') {
			signupmdal.BusinessName_txt().clear()

			signupmdal.BusinessName_txt().sendKeys(this.businessName)

			signupmdal.BsnesContactName_txt().clear()

			signupmdal.BsnesContactName_txt().sendKeys(this.contactName)

			signupmdal.BsnesPhone_txt().clear()

			signupmdal.BsnesPhone_txt().sendKeys(this.phone)

			if (this.logoname != '') {
				Common.UploadFile_func(signupmdal.BsnesLogoUpload_btn(), this.logoname)
			
			}

			signupmdal.BsnesEmail_txt().clear()

			signupmdal.BsnesEmail_txt().sendKeys(this.email)

			signupmdal.BsnesPassword_txt().clear()

			signupmdal.BsnesPassword_txt().sendKeys(this.password)

			signupmdal.BsnesPasswordAgain_txt().clear()

			signupmdal.BsnesPasswordAgain_txt().sendKeys(this.password)

			((driver) as JavascriptExecutor).executeScript('arguments[0].click();', signupmdal.BsnesTermAccept_chk())

			signupmdal.BsnesSubmit_btn().click()

			WebUI.delay(5)
		} else {
			this.SignUpMover_func(false)
		}
	}

	public void SignUpMover_func(Boolean openbrowser = true)
	{
		Common cm
		if(openbrowser==true)
		{

			WebUI.openBrowser(GlobalVariable.glb_URL)
			WebUI.delay(5)
			cm = new Common()
			cm.ClearServercache()

		}

		SignUpModal signupmdal = new SignUpModal()
		if(signupmdal.MoverName_txt()==null)
			Navigation.OpenSignUpModal(this.userType)

		WebUI.comment('Fill user info')
		WebDriver driver = DriverFactory.getWebDriver()


		signupmdal.MoverName_txt().clear()
		signupmdal.MoverName_txt().sendKeys(this.businessName)

		signupmdal.MoverContactName_txt().clear()
		signupmdal.MoverContactName_txt().sendKeys(this.contactName)

		signupmdal.MoverPhone_txt().clear()
		signupmdal.MoverPhone_txt().sendKeys(this.phone)

		signupmdal.MoverEmail_txt().clear()
		signupmdal.MoverEmail_txt().sendKeys(this.email)

		signupmdal.MoverCompEmail().clear()
		signupmdal.MoverCompEmail().sendKeys(this.email)

		signupmdal.MoverPassword_txt().clear()
		signupmdal.MoverPassword_txt().sendKeys(this.password)

		signupmdal.MoverPasswordAgain_txt().clear()
		signupmdal.MoverPasswordAgain_txt().sendKeys(this.password)

		((driver) as JavascriptExecutor).executeScript("arguments[0].click();", signupmdal.MoverTermAccept_chk())

		signupmdal.MoverSubmit_btn().click()

		WebUI.delay(5)
	}


	public void ProvideMoveInfo_func(MovingPlan plan,String submit ="Yes")
	{
		LandingPage currentpage = new LandingPage()

		Common cm = new Common()

		MovingPlan plan_tmp = plan

		currentpage.FromAddress_txt().clear()

		currentpage.FromAddress_txt().sendKeys(plan_tmp.AddrFrom)

		currentpage.ToAddress_txt().clear()

		currentpage.ToAddress_txt().sendKeys(plan_tmp.AddrTo)
		println("ExactWeight = "+plan_tmp.ExactWeight)
		if (plan_tmp.ExactWeight == true) {
			Boolean status = currentpage.ExactWeight_opt().isSelected()

			if (!(status)) {
				cm.HandleElementClick(currentpage.ExactWeight_opt())
			}

			WebUI.delay(1)

			currentpage.ExactWeight_txt().clear()

			currentpage.ExactWeight_txt().sendKeys(plan_tmp.TotalWeight.toString())
		}//end if

		else {
			if(!Inventorymodal.Bedroom_txt().isDisplayed())
			{
				println("Click on Use My Inventory button.")
				cm.HandleElementClick(currentpage.InventoryWeight_btn())
				WebUI.delay(2)
			}
			this.ProvideCustomWeight_func(plan,"Yes")
			
		}

		if (submit == "Yes") {
			Common.HandleElementClick(currentpage.ViewMoveRate_btn())
			WebUI.delay(3)
		}

		boolean display = Messages.OppsOk_btn().isDisplayed()

		println("OK displayed: "+display)

		if(Messages.OppsOk_btn()!=null&&Messages.OppsOk_btn().isDisplayed())
		{
			Messages.OppsOk_btn().click()

		}

		WebUI.delay(5)
	}


	public void ProvideCustomWeight_func(MovingPlan plan,String submit ="Yes")
	{
		Common cm = new Common()
		MovingPlan plan_tmp = plan
		/*
		LandingPage currentpage = new LandingPage()
		
		if(currentpage.ExactWeight_opt().isSelected())
		{
			cm.HandleElementClick(currentpage.ExactWeight_opt())
			WebUI.delay(2)
		}

		*/
		
		cm.ActionSendkey(Inventorymodal.Bedroom_txt(),plan_tmp.Bedroom.toString())

		//for(int i =0;i<3;i++)
		//	cm.ActionSendkey(Inventorymodal.Bathroom_txt(),Keys.BACK_SPACE)

		cm.ActionSendkey(Inventorymodal.Bathroom_txt(),plan_tmp.Bathroom.toString())
		WebUI.delay(5)

		cm.ActionSendkey(Inventorymodal.LivingRoom_txt(),plan_tmp.Livingroom.toString())

		cm.ActionSendkey(Inventorymodal.DiningRoom_txt(),plan_tmp.Diningroom.toString())

		cm.ActionSendkey(Inventorymodal.Kitchen_txt(),plan_tmp.Kitchen.toString())

		cm.ActionSendkey(Inventorymodal.Laundry_txt(),plan_tmp.Laundry.toString())

		cm.ActionSendkey(Inventorymodal.CarGarage_txt(),plan_tmp.Garage.toString())

		cm.ActionSendkey(Inventorymodal.AdditionStorage_txt(),plan_tmp.AddtnalStorage.toString())

		cm.ActionSendkey(Inventorymodal.Basement_txt(),plan_tmp.Basement.toString())

		cm.ActionSendkey(Inventorymodal.StorageShed_txt(),plan_tmp.StorageShed.toString())

		cm.ActionSendkey(Inventorymodal.Patio_txt(),plan_tmp.Patio.toString())

		if(submit == "Yes")
		{
			Inventorymodal.Submit_btn().click()
		}

		WebUI.delay(2)
		this.plan.CalculateTotalCustomWeight()
	}

	public void ChangeUserInfo()
	{
		contactName = username+"updated"
		//businessName = 'vnupdated'
		password = 'Password2'
		phone = '(123) 456-1507'
		role = 'User'
		email = username+'updated'+'@bg.vn'
		birthday = '2001-12-04'
		passpord = '351507'
		//address1 = '20 Wall Street updated'
		//address2 = '50 High Road updated'
		//city = 'HCM updated'
		//state = 'testing_state_updated'
		//postalcode = '1507'
	}

	public void ChangeBusinessInfo_func()
	{
		businessName = 'vnupdated'
		address1 = '20 Wall Street updated'
		address2 = '50 High Road updated'
		city = 'HCM updated'
		state = "Alabama"
		bsinessContact = "Business Contact Update"
		bsinessEmail = "bsinessUpdate@email.com"
		bsinessWeb = "www.Webbusiness.com"
		bsinessDescr = "this is business description which changed"
		postalcode = "33333"

	}


	public Boolean IsUserExisted(){

		Boolean status = false
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()

		ResultSet rs = dbconnection.stmt.executeQuery("SELECT * FROM eden_users where email = '"+this.email+"'");
		status = rs.next()
		rs.close()
		dbconnection.CloseAllConnect()

		return status
	}

	public Boolean IsContactNameExisted(String contactname){

		Boolean status = false
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()

		ResultSet rs = dbconnection.stmt.executeQuery("SELECT * FROM eden_users where username = '"+contactname+"'");

		status = rs.next()
		rs.close()
		dbconnection.CloseAllConnect()

		return status
	}


	public String GetResetPassLinkFromDB() {


		String id = ""
		String link = ""
		String code = ""

		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()

		ResultSet rs = dbconnection.stmt.executeQuery("SELECT * FROM eden_reminders er join eden_users eu on er.user_id = eu.id AND eu.email = '"+this.email+"'");

		while (rs.next()) {
			id = rs.getString("user_id")
			code = rs.getString("code")

		}
		rs.close()
		dbconnection.CloseAllConnect()


		link = GlobalVariable.glb_URL+ "/auth/reset/"+id+"/"+code
		return link
	}



	public void GetUserInfoFromDB() {
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()


		ResultSet rs = dbconnection.stmt.executeQuery("SELECT * FROM eden_users where email = '"+this.email+"'");

		while (rs.next()) {
			this.Id = rs.getString("id")
			this.contactName = rs.getString("username")
			//this.userId = rs.getString("uid")
			this.email = rs.getString("email")
			this.phone = rs.getString("phone")
			//this.passpord = rs.getString("passport")
			//this.address1 = rs.getString("address")
			//this.address2 = rs.getString("address2")
			//this.city = rs.getString("city")
			//this.state = rs.getString("state")
			//this.count = rs.getString("country")
			//this.postalcode = rs.getString("postal_code")
			this.contactName = rs.getString("first_name")
			this.businessName = rs.getString("last_name")
		}
		rs.close()
		dbconnection.CloseAllConnect()
	}

	public void UpdateActiveSttFromDB(String active_stt) {
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()

		int i = dbconnection.stmt.executeUpdate("Update eden_activations set completed = '"+active_stt+"' where user_id = (select id from eden_users where email = '"+this.email+"')");
		if(i==0)
		{
			KeywordUtil.markFailedAndStop("Email '"+this.email+"' NOT found to update status.")
		}

		dbconnection.CloseAllConnect()
	}

	public void UpdatePassFromDB(String newpass = "Password@1") {

		//Use page https://bcrypt-generator.com/
		//with Rounds = 10 to check hash of password
		String hash_tmp
		if(newpass =="Password@1")
			hash_tmp = "\$2y\$10\$1T/VLNWFaUsm2Wa85rAs4eHb.t.Of7antEX8Lt1LiBfzsyYT1dpLq"
		else if(newpass =="Password@2")
			hash_tmp = "\$2y\$10\$j39UGZEFEnYdY.M2txntTOomBOIvLuben7JFTrOSeL3A1Y2os2zOC"

		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()

		int i = dbconnection.stmt.executeUpdate("Update eden_users set password = "+hash_tmp+" where email = '"+this.email+"'")

		if(i==0)
		{
			KeywordUtil.markFailedAndStop("Username '"+this.email+"' NOT found to update status.")
		}

		dbconnection.CloseAllConnect()
	}


	public void UpdateEmailFromDB(String email_str="") {
		String email_temp = this.username+'@bg.vn'
		println(email_temp)
		if(email_str!="")
		{
			email_temp = email_str

		}
		WebUI.comment('CONNECT DB')

		DBCONNECT dbconnection = new DBCONNECT()
		dbconnection.OpenSSHSessionWithKey()

		dbconnection.OpenDBConnection()
		println(this.email)
		println("Update eden_users set email = "+email_temp+" where email = '"+this.email+"'")
		int i = dbconnection.stmt.executeUpdate("Update eden_users set email = '"+email_temp+"' where email = '"+this.email+"'")
		if(i==0)
		{
			KeywordUtil.markFailedAndStop("Username '"+this.email+"' NOT found to update status.")
		}

		dbconnection.CloseAllConnect()
	}

	public void ChangeProfilePassword_func(String newPass_str)
	{
		if(this.userType == "Consumer")
		{
			if(ConsumerProfilePage.PassEditGear_icon()==null)
			{
				Navigation.GotoUserProfile_func()
			}

			ConsumerProfilePage.PassEditGear_icon().click()
			WebUI.delay(2)

			ConsumerProfilePage.OldPass_txt().sendKeys(this.password)

			ConsumerProfilePage.NewPass_txt().sendKeys(newPass_str)

			ConsumerProfilePage.ConfirmPass_txt().sendKeys(newPass_str)

			ConsumerProfilePage.PasswordUpdate_btn().click()

			WebUI.delay(3)

			this.password = newPass_str

		}

	}

 public void ProvideEmailResetPass_func()
 {
	 LandingPage.Login_lnk().click()
	 
	 LoginPage.ForgotPass_lnk().click()
	 WebUI.delay(3)
	 LoginPage.ResetEmail_txt().sendKeys(this.email)
	 LoginPage.SendPassReset_btn().click()
	 Thread.sleep(5000)
	 
 }
 
 public void ResetPassword_func(String newPass,Boolean submit = true)
 {
	
	 String link = this.GetResetPassLinkFromDB()
	
	 WebUI.navigateToUrl(link)
	 WebUI.delay(2)
	 
	 ResetPasswordPage.Password_txt().sendKeys(newPass)

	 ResetPasswordPage.ConfirmPass_txt().sendKeys(newPass)
	 
	 if(submit == true)
	 ResetPasswordPage.Submit_btn().click()
	/*
	  else if(submit == false)
		 ResetPasswordPage.PassCancel_btn().click()
	 WebUI.delay(2)
*/
 }
 

}
