package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import objects.User
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import functions.Common

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException


public class SignUpModal {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private static PsnalContactName_txt_xpath	= "//div[@id = 'personal']//input[@name = 'username']"

	private static PsnalEmail_txt_xpath	= "//div[@id = 'personal']//input[@name = 'email']"
	private static PsnalPassword_txt_xpath	= "//div[@id = 'personal']//input[@name = 'password']"
	private static PsnalPasswordAgain_txt_xpath	= "//div[@id = 'personal']//input[@name = 'password_confirmation']"
	private static PsnalPhone_txt_xpath	= "//div[@id = 'personal']//input[@name = 'phone']"
	private static PsnalTermAccept_chk_xpath	= "//div[@id = 'personal']//input[@name ='policy']"
	private static PsnalSubmit_btn_xpath	= "//div[@id = 'personal']//button[text() ='Sign up']"
	private static Psnalsuccess_message_xpath= "//div[@id = 'personal']//div[@class ='h4 m-0 text-white pb-4']"
	private static PsnalSignin_lnk_xpath = "//div[@id = 'personal']//a[contains(@href,'login')]"
	private static PsnalFacebook_btn_xpath= "//div[@id = 'personal']//button[contains(@onclick, 'redirect/facebook')]"
	private static PsnalGmail_btn_xpath= "//div[@id = 'personal']//button[contains(@onclick, 'redirect/google')]"
	private static PsnalMoverLogin_lnk_xpath= "//div[@id = 'personal']//a[contains(@href, 'login-mover')]"
	private static PsnalBack_lnk_xpath= "//div[@id = 'personal']//a[contains(text(),'Back')]"
	private static Mover_opt_xpath= "//a[contains(@href,'register-mover')]"
	private static PsnalLogin_lnk_xpath= "//div[@id = 'personal']//a[text() ='Log in Here']"
	private static PsnalContactNameValidation_xpath= "//div[@id = 'personal']//label[contains(text(), 'Contact Name')]/..//span[@class= 'invalid-feedback d-block']"
	private static PsnalEmailValidation_xpath= "//div[@id = 'personal']//label[text() = 'Email Address']/..//span[@class= 'invalid-feedback d-block']"
	private static PsnalPasswordValidation_xpath= "//div[@id = 'personal']//label[text() = 'Password']/..//span[@class= 'invalid-feedback d-block']"
	private static PsnalTermValidation_xpath= "//div[@id = 'personal']//div[contains(@class, 'checkbox-circle')]/..//span[@class= 'invalid-feedback d-block']"
	//BUSINESS FORM

	private static BsnesContactName_txt_xpath	= "//div[@id = 'business']//input[@name = 'username']"
	private static BsnesEmail_txt_xpath	= "//div[@id = 'business']//input[@name = 'email']"
	private static BsnesPassword_txt_xpath	= "//div[@id = 'business']//input[@name = 'password']"
	private static BsnesPasswordAgain_txt_xpath	= "//div[@id = 'business']//input[@name = 'password_confirmation']"
	private static BsnesPhone_txt_xpath	= "//div[@id = 'business']//input[@name = 'contact_number']"
	private static BsnesTermAccept_chk_xpath	= "//div[@id = 'business']//input[@name ='policy']"
	private static BsnesSubmit_btn_xpath	= "//div[@id = 'business']//button[text() ='Sign up']"
	private static Bsnessuccess_message_xpath= "//div[@id = 'business']//div[@class ='h4 m-0 text-white pb-4']"
	private static BsnesSignin_lnk_xpath = "//div[@id = 'business']//a[contains(@href,'login')]"
	private static BsnesFacebook_btn_xpath= "//div[@id = 'business']//button[contains(@onclick, 'redirect/facebook')]"
	private static BsnesGmail_btn_xpath= "//div[@id = 'business']//button[contains(@onclick, 'redirect/google')]"
	private static BsnesMoverLogin_lnk_xpath= "//div[@id = 'business']//a[contains(@href, 'login-mover')]"
	private static BsnesBack_lnk_xpath= "//div[@id = 'business']//a[contains(text(),'Back')]"
	private static Personal_opt_xpath= "//input[@id ='consumer_type_personal']"
	private static Business_opt_xpath= "//input[@id ='consumer_type_business']"
	private static BsnesLogin_lnk_xpath= "//div[@id = 'business']//a[text() ='Log in Here']"
	private static BsnesName_txt_xpath= "//div[@id = 'business']//input[@name ='business_name']"
	private static BsnesLogoUpload_btn_xpath= "//span[contains(@class,'filestyle')]//span"
	private static BsnesLogo_txt_xpath= "//div[@class ='bootstrap-filestyle input-group']/input"
	private static BsnesDescr_txt_xpath= "//textarea[@name = 'description']"
	private static BsnesNameValidation_xpath= "//label[text() = 'Business Name']/..//span[@class= 'invalid-feedback d-block']"
	private static BsnesContactNameValidation_xpath= "//div[@id = 'business']//label[contains(text(), 'Contact Name')]/..//span[@class= 'invalid-feedback d-block']"
	private static BsnesLogoValidation_xpath= "//div[@id = 'business']//label[text() = 'Logo Upload']/..//span[@class= 'invalid-feedback d-block']"
	private static BsnesEmailValidation_xpath= "//div[@id = 'business']//label[text() = 'Email Address']/..//span[@class= 'invalid-feedback d-block']"
	private static BsnesPasswordValidation_xpath= "//div[@id = 'business']//label[text() = 'Password']/..//span[@class= 'invalid-feedback d-block']"
	private static BsnesTermValidation_xpath= "//div[@id = 'business']//div[contains(@class, 'checkbox-circle')]/..//span[@class= 'invalid-feedback d-block']"


	//MOVER FORM

	private static MoverContactName_txt_xpath	= "//form[@id = 'form-mover-register']//input[@name = 'username']"
	private static MoverEmail_txt_xpath	= "//form[@id = 'form-mover-register']//input[@name = 'email']"
	private static MoverPassword_txt_xpath	= "//form[@id = 'form-mover-register']//input[@name = 'password']"
	private static MoverPasswordAgain_txt_xpath	= "//form[@id = 'form-mover-register']//input[@name = 'password_confirmation']"
	private static MoverPhone_txt_xpath	= "//form[@id = 'form-mover-register']//input[@name = 'phone']"
	private static MoverTermAccept_chk_xpath	= "//form[@id = 'form-mover-register']//input[@name ='policy']"
	private static MoverSubmit_btn_xpath	= "//form[@id = 'form-mover-register']//button[text() ='Sign up']"
	private static Moversuccess_message_xpath= "//form[@id = 'form-mover-register']//div[@class ='h4 m-0 text-white pb-4']"
	private static MoverLogin_lnk_xpath= "//a[text() ='Log in Here']"
	private static MoverName_txt_xpath= "//form[@id = 'form-mover-register']//input[@name ='company']"
	private static MoverDescr_txt_xpath= "//form[@id = 'form-mover-register']//textarea[@name = 'desc']"
	private static MoverNameValidation_xpath= "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-6'][3]//span[@class= 'invalid-feedback d-block']"
	private static MoverContactNameValidation_xpath= "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-6'][4]//span[@class= 'invalid-feedback d-block']"
	private static MoverEmailValidation_xpath=       "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-4'][4]//span[@class= 'invalid-feedback d-block']"
	private static YearValidation_xpath= "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-4'][6]//span[@class= 'invalid-feedback d-block']"
	private static MoverPasswordValidation_xpath= "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-12'][1]//span[@class= 'invalid-feedback d-block']"
	private static MoverTermValidation_xpath= "//form[@id = 'form-mover-register']//div[@class= 'checkbox checkbox-custom checkbox-circle']//span[@class= 'invalid-feedback d-block']"
	private static MoverPhoneValidation_xpath = "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-4'][5]//span[@class= 'invalid-feedback d-block']"
	private static MoverCompEmail_xpath = "//input[@name = 'email_company']"
	private static MoverCompEmailValidation_xpath = "//form[@id = 'form-mover-register']//div[@class ='form-group col-md-4'][10]//span[@class= 'invalid-feedback d-block']"




	public SignUpModal() {
	}
	//PERSONAL ELEMENT
	public static WebElement MoverCompEmail() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverCompEmail_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverCompEmailValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverCompEmailValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalContactNameValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalContactNameValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement PsnalEmailValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalPasswordValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalPasswordValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalTermValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalTermValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalTermValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalFacebook_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalFacebook_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalGmail_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalGmail_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalMoverLogin_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalMoverLogin_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalBack_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalBack_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Mover_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Mover_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement PsnalLogin_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalLogin_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}



	public static WebElement PsnalSignin_lnk() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalSignin_lnk_xpath));
			return temp_element;
		}		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement PsnalSucces_message_lbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Psnalsuccess_message_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalContactName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalContactName_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalEmail_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalEmail_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalPassword_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalPassword_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalPasswordAgain_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalPasswordAgain_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalPhone_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalPhone_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalTermAccept_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalTermAccept_chk_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PsnalSubmit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PsnalSubmit_btn_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}


	//BUSINESS ELEMENT
	public static WebElement BusinessNameValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesNameValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesContactNameValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesContactNameValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement LogoValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesLogoValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesEmailValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesEmailValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesPasswordValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesPasswordValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesTermValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesTermValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesFacebook_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesFacebook_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesGmail_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesGmail_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesMoverLogin_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesMoverLogin_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesBack_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesBack_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Personal_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Personal_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Business_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Business_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesLogin_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesLogin_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BusinessName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesLogoUpload_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesLogoUpload_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesLogo_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesLogo_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BusinessDescr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesDescr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BsnesSignin_lnk() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesSignin_lnk_xpath));
			return temp_element;
		}		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement BsnesSucces_message_lbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Bsnessuccess_message_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesContactName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesContactName_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesEmail_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesEmail_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesPassword_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesPassword_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesPasswordAgain_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesPasswordAgain_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesPhone_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesPhone_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesTermAccept_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesTermAccept_chk_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsnesSubmit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsnesSubmit_btn_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	//MOVER ELEMENT
	public static WebElement MoverNameValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverNameValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverContactNameValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverContactNameValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverEmailValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverEmailValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverPasswordValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverPasswordValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverTermValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverTermValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}



	public static WebElement MoverLogin_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverLogin_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverBsnesDescr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverDescr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverSucces_message_lbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Moversuccess_message_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverContactName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverContactName_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverEmail_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverEmail_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverPassword_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverPassword_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverPasswordAgain_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverPasswordAgain_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverPhone_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverPhone_txt_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverTermAccept_chk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverTermAccept_chk_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverSubmit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverSubmit_btn_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverYearValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(YearValidation_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement MoverPhoneValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverPhoneValidation_xpath));
			return temp_element;
		}catch(NoSuchElementException e) {
			return null
		}
	}


	//=================FUNCTION==========================================================================
	//========================================
	//==============================
	public static void TestValidationPsnalTerm_func(Boolean checked_status, String Expected_msg)
	{
		Boolean status = this.PsnalTermAccept_chk().isSelected()
		WebDriver driver = DriverFactory.getWebDriver()
		if(checked_status != status)
		{
			((driver) as JavascriptExecutor).executeScript("arguments[0].click();", this.PsnalTermAccept_chk())

		}

		this.PsnalSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.PsnalTermValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.PsnalTermValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Term&Condition status ='"+checked_status+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.PsnalTermValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when check the checkbox. "
			}
		}
	}
	public static void TestValidationPsnalNewPass_func(String input_str, String Expected_msg)
	{
		this.PsnalPassword_txt().clear()
		this.PsnalPassword_txt().sendKeys(input_str)

		this.PsnalPasswordAgain_txt().clear()
		this.PsnalPasswordAgain_txt().sendKeys(GlobalVariable.glb_password)

		this.PsnalSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.PsnalPasswordValidation() ==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			String observed_msg = this.PsnalPasswordValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[NewPass='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.PsnalPasswordValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid NewPass"
			}
		}
	}
	
	
	public static void TestValidationPsnalEmail_func(String input_str, String Expected_msg)
	{
		if(Expected_msg!="")
		{
			if(this.PsnalEmailValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.PsnalEmailValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Email='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.PsnalEmailValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid Email"
			}
		}
	}
	public static void TestValidationPsnalContactName_func(String input_str, String Expected_msg)
	{
		if(Expected_msg!="")
		{
			if(this.PsnalContactNameValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.PsnalContactNameValidation.getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Contactname ='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.PsnalContactNameValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid ContactName"
			}
		}
	}
	public static void TestValidationMoverTerm_func(Boolean checked_status, String Expected_msg)
	{
		Boolean status = this.MoverTermAccept_chk().isSelected()
		println(status)
		WebDriver driver = DriverFactory.getWebDriver()
		if(checked_status != status)
		{
			((driver) as JavascriptExecutor).executeScript("arguments[0].click();", this.MoverTermAccept_chk())

		}


		this.MoverSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.MoverTermValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.MoverTermValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Term&Condition status ='"+checked_status+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.MoverTermValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when check the checkbox. "
			}
		}
	}
	public static void TestValidationMoverNewPass_func(String input_str, String Expected_msg)
	{
		this.MoverPassword_txt().clear()
		this.MoverPassword_txt().sendKeys(input_str)

		this.MoverPasswordAgain_txt().clear()
		this.MoverPasswordAgain_txt().sendKeys(GlobalVariable.glb_password)

		this.MoverSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.MoverPasswordValidation() ==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			String observed_msg = this.MoverPasswordValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[NewPass='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.MoverPasswordValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid NewPass"
			}
		}
	}
	public static void TestValidationLogoUpload_func(String input_str, String Expected_msg)
	{

		Common cm = new Common()
		cm.UploadFile_func(this.BsnesLogoUpload_btn(), input_str)

		WebDriver driver = DriverFactory.getWebDriver()

		WebUI.delay(3)
		
		this.BsnesSubmit_btn().click()

		WebUI.delay(3)

		if (Expected_msg != '') {
			if (this.LogoValidation() == null) {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += 'Validation message does not displays.'
				return
			}

			String observed_msg = this.LogoValidation().getText()

			if (observed_msg != Expected_msg) {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += (((((('[LogoUpload =\'' + input_str) + '\']. Observed: ') + observed_msg) +
						'-Expected: ') + Expected_msg) + '. ')
			}
		} else {
			if (this.LogoValidation() != null) {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += 'Validation message should not display when upload valid Logo'
			}
		}
	}

	public static void TestValidationBusinessName_func(String input_str, String Expected_msg)
	{
		WebUI.delay(2)

		if (Expected_msg != '') {
			if (this.BusinessNameValidation() == null) {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += 'Validation message does not displays.'
				return
			}

			String observed_msg = this.BusinessNameValidation().getText()

			if (observed_msg != Expected_msg) {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += (((((('[BusinessName =\'' + input_str) + '\']. Observed: ') + observed_msg) +
						'-Expected: ') + Expected_msg) + '. ')
			}
		} else {
			if (this.BusinessNameValidation() != null) {
				GlobalVariable.glb_TCStatus = false

				GlobalVariable.glb_TCFailedMessage += 'Validation message should not display when input valid BusinessName'
			}
		}
	}

	public static void TestValidationBsnesTerm_func(Boolean checked_status, String Expected_msg)
	{
		Boolean status = this.BsnesTermAccept_chk().isSelected()

		WebDriver driver = DriverFactory.getWebDriver()
		if(checked_status != status)
		{
			((driver) as JavascriptExecutor).executeScript("arguments[0].click();", this.BsnesTermAccept_chk())

		}


		this.BsnesSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.BsnesTermValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.BsnesTermValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Term&Condition status ='"+checked_status+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.BsnesTermValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when check the checkbox. "
			}
		}
	}

	public static void TestValidationBsnesNewPass_func(String input_str, String Expected_msg)
	{
		this.BsnesPassword_txt().clear()
		this.BsnesPassword_txt().sendKeys(input_str)

		this.BsnesPasswordAgain_txt().clear()
		this.BsnesPasswordAgain_txt().sendKeys(GlobalVariable.glb_password)

		this.BsnesSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.BsnesPasswordValidation() ==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			String observed_msg = this.BsnesPasswordValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[NewPass='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.BsnesPasswordValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid NewPass."
			}
		}
	}

	public static void TestValidationBsnesEmail_func(String input_str, String Expected_msg)
	{
		this.BsnesEmail_txt().clear()
		this.BsnesEmail_txt().sendKeys(input_str)

		this.BsnesSubmit_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.BsnesEmailValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.BsnesEmailValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[BsnessEmail='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.BsnesEmailValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid Email."
			}
		}
	}

	public static void TestValidationBsnesContactName_func(String input_str, String Expected_msg){
		if(Expected_msg!="")
		{
			if(this.BsnesContactNameValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.BsnesContactNameValidation.getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Contactname ='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.BsnesContactNameValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid ContactName."
			}
		}
	}


}