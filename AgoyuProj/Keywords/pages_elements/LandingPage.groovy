package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import functions.Common

import org.openqa.selenium.WebElement
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.interactions.Action

public class LandingPage {
	/**
	 * Refresh browser
	 */

	private static WebDriver webDriver;

	private static String Login_lnk_xpath	= "//a[@data-target ='.login-form']/span"
	private static String Howitwork_lnk_xpath	= "//a[contains(text(),'how-it-works')]"
	private static String MovingResource_lnk_xpath	= "//a[text() ='Moving Resources']"
	private static String MovingPartner_lnk_xpath	= "//a[text() ='Moving Partners']"
	private static String AboutUs_lnk_xpath	= "//a[contains(@href,'about-us')]"
	private static String FAQ_lnk_xpath = "//a[contains(@href,'faq')]"
	private static String BecomeMover_lnk_xpath	= "//a[contains(@href,'register-mover')]"
	private static String Logo_img_xpath	= "//img[@alt ='Agoyu Logo']"
	private static String FromAddress_txt_xpath	= "//input[@id ='from_addr']"
	private static String ToAddress_txt_xpath	= "//input[@id ='to_addr']"
	private static String ExactWeight_opt_xpath	= "//div[@id ='check-input-weight']"
	private static String ExactWeight_txt_xpath	= "//input[@id ='custom_weights']"
	private static String MoveDate_txt_xpath	= "//input[@id ='move_date']"
	private static String ViewMoveRate_btn_xpath	= "//button[@id ='submit-exactly']"
	private static String TotalWeight_vlue_xpath	= "//span[@id ='total-weight-furniture-landing']"
	private static String InventoryWeight_btn_xpath	= "//div[@id ='estimate_button']"
	private static String FromAddrValidation_xpath	= "//div[@id = 'root-from_addr']/..//ul[contains(@class ,'validation')]"
	private static String ToAddrValidation_xpath	= "//ul[@id ='validation_to_addr']"
	private static String WeightValidation_xpath	= "//ul[@id ='validation_custom_weights']"
	private static String ContLastMove_btn_xpath = "//a[@onclick = 'window.setDataMovePlanFromStorage()']"
	private static String ContinueCookie_btn_xpath = "//a[text() ='Continue']"
	private static String Cookie_msg_xpath = "//div[@id = 'popupPolicy']"




	public LandingPage() {
	}

	//WebElement Declare

	public static WebElement Cookie_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Cookie_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement ContinueCookie_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContinueCookie_btn_xpath));
			return temp_element;
		}
		catch(Exception e)
		{return null}
	}

	public static WebElement ContinueLastMove_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContLastMove_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Login_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Login_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Howitwork_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Howitwork_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MovingResource_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MovingResource_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MovingPartner_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MovingPartner_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AboutUs_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AboutUs_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement FAQ_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FAQ_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement BecomeMover_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BecomeMover_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement Logo_img() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Logo_img_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement FromAddress_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FromAddress_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ToAddress_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ToAddress_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ExactWeight_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ExactWeight_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ExactWeight_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ExactWeight_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoveDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ViewMoveRate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ViewMoveRate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TotalWeight_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TotalWeight_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement InventoryWeight_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(InventoryWeight_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement FromAddrValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FromAddrValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ToAddrValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ToAddrValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement WeightValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(WeightValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	//===============================================FUNCTION=============================================
	//=============================================
	//=============================================
	public void TestValidationExactWeight_func(String input_str, String Expected_msg)
	{
		if(input_str.toString().contains("-"))
		{
			WebDriver webDriver = DriverFactory.getWebDriver();
			Actions actions = new Actions(webDriver);
			Action action = actions.sendKeys(this.ExactWeight_txt(),input_str).build();
			action.perform();
			this.ViewMoveRate_btn().click()

			if(this.ExactWeight_txt.getText()==input_str)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Negative number should not allow to input."
			}
		}

		else
		{

			Common cm = new Common()
			Boolean status = this.ExactWeight_opt().isSelected()
			if(!status)
				cm.HandleElementClick(this.ExactWeight_opt())

			WebUI.delay(1)


			if(input_str!="")
			{
				this.ExactWeight_txt().clear()
				this.ExactWeight_txt().sendKeys(input_str)
			}


			cm.HandleElementClick(this.ViewMoveRate_btn())
			WebUI.delay(5)
		}


		if(Expected_msg!="")
		{
			if(this.WeightValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			String observed_msg = this.WeightValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[ExactWeight='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.WeightValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid Exact Weight."
			}
		}
	}

	public void TestValidationFrom_func(String input_str, String Expected_msg)
	{
		if(Expected_msg!="")
		{
			if(this.FromAddrValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			else
			{
				String observed_msg = this.FromAddrValidation().getText()
				println(observed_msg)

				if(observed_msg!=Expected_msg)
				{
					GlobalVariable.glb_TCStatus=false
					GlobalVariable.glb_TCFailedMessage +="[FromAddr='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

				}

			}
		}

		else
		{
			if(this.FromAddrValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid AddressFrom."
			}
		}
	}
	public void TestValidationInventoryWeight_func(String input_str, String Expected_msg)
	{
		Inventorymodal modal = new Inventorymodal()

		if(Expected_msg!="")
		{
			if(modal.WeightValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = modal.WeightValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[InventoryWeight='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else if(modal.WeightValidation()!=null)

		{
			if(modal.WeightValidation().getText()!="")
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation msg'"+modal.WeightValidation().getText()+"' should not display when input valid Custom Weight."
			}
		}
	}

	public void TestValidationTo_func(String input_str, String Expected_msg)
	{
		if(Expected_msg!="")
		{
			if(this.ToAddrValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="To Address Validation message does not displays."
				return
			}

			String observed_msg = this.ToAddrValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[ToAddr='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.ToAddrValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="To Address Validation message should not display when input valid Email."
			}
		}
	}//end func
}//end class
