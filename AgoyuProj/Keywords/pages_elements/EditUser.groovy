package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException

public class EditUser {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private static ConfirmNo_btn_xpath
	private static ErrorMessage_xpath
	private static Submit_btn_xpath
	private static UserEmail_txt_xpath
	private static UserName_txt_xpath
	private static UserPassword_xpath
	private static ConfirmNewPassword_txt_xpath
	private static UserRole_cbbox_xpath
	private static Firstname_txt_xpath
	private static Lastname_txt_xpath
	private static Phone_txt_xpath
	private static Birthday_txt_xpath
	private static Passpord_txt_xpath
	private static Address1_txt_xpath
	private static Address2_txt_xpath
	private static City_txt_xpath
	private static State_txt_xpath
	private static PostalCode_txt_xpath
	private static Country_txt_xpath
	private static ValidationFirstName_msg_xpath
	private static ValidationLastName_msg_xpath
	private static ValidationPhone_msg_xpath
	private static ValidationPassport_msg_xpath
	private static ValidationPostal_msg_xpath
	private static  ValidationNewPass_msg_xpath
	private static  ValidationConfirmPass_msg_xpath
	private static  EditGear_icon_xpath
	private static  Cancel_btn_xpath


	public EditUser() {
		webDriver = DriverFactory.getWebDriver();


		ConfirmNo_btn_xpath	= "//h1[contains(text(),'Whoops, looks like something went wrong.')]"
		ErrorMessage_xpath	= "//div[@class = 'exception-message-wrapper']"
		Submit_btn_xpath	= "//button[@data-control='submit']"
		UserEmail_txt_xpath	= "//*[@name = 'email']"
		UserName_txt_xpath	= "//label[text() = 'Username']//../input"
		UserPassword_xpath	= "//*[@id = 'password']"
		UserRole_cbbox_xpath	= "//*[@id = 'roles[]']"
		Firstname_txt_xpath	= "//input[@name='first_name']"
		Lastname_txt_xpath	= "//input[@name='last_name']"
		Phone_txt_xpath	= "//input[@name='phone']"
		Birthday_txt_xpath	= "//input[@id='datepicker']"
		Passpord_txt_xpath	= "//input[@name='passport']"
		Address1_txt_xpath	= "//input[@name='address']"
		Address2_txt_xpath	= "//input[@name='address2']"
		City_txt_xpath	= "//input[@name='city']"
		State_txt_xpath	= "//input[@name='state']"
		PostalCode_txt_xpath	= "//input[@name='postal_code']"
		Country_txt_xpath	= "//label[text()='Country']/../select"
		ValidationFirstName_msg_xpath	= "//ul[@data-field='first_name']/li"
		ValidationLastName_msg_xpath	= "//ul[@data-field='last_name']/li"
		ValidationPhone_msg_xpath	= "//ul[@data-field='phone']/li"
		ValidationPassport_msg_xpath	= "//ul[@data-field='passport']/li"
		ValidationPostal_msg_xpath = "//ul[@data-field='postal_code']/li"
		EditGear_icon_xpath = "//a[@class ='actions__item fas fa-cog switch-panel-mode']"
		Cancel_btn_xpath = "//button[@data-control = 'cancel']"
	}//end edituser()


	public static WebElement EditGear_icon() {

		try{

			WebElement temp_element = webDriver.findElement(By.xpath(EditGear_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationNewPass_msg() {

		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationNewPass_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationConfirmPass_msg() {

		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationConfirmPass_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ConfirmNewPassword_txt() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ConfirmNewPassword_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ValidationPostal_msg() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationPostal_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationFirstName_msg() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationFirstName_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationLastName_msg() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationLastName_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationPhone_msg() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationPhone_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationPassport_msg() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ValidationPassport_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Firstname_txt() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(Firstname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Lastname_txt() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(Lastname_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Phone_txt() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(Phone_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Birthday_txt() {
		try{

			WebElement temp_element = webDriver.findElement(By.xpath(Birthday_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Passpord_txt() {
		try {

			WebElement temp_element = webDriver.findElement(By.xpath(Passpord_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Address1_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(Address1_txt_xpath));
		return temp_element;
	}

	public static WebElement Address2_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(Address2_txt_xpath));
		return temp_element;
	}

	public static WebElement City_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(City_txt_xpath));
		return temp_element;
	}

	public static WebElement State_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(State_txt_xpath));
		return temp_element;
	}

	public static WebElement PostalCode_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(PostalCode_txt_xpath));
		return temp_element;
	}

	public static WebElement Country_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(Country_txt_xpath));
		return temp_element;
	}

	public static WebElement ConfirmNo_btn() {

		WebElement temp_element = webDriver.findElement(By.xpath(ConfirmNo_btn_xpath));
		return temp_element;
	}

	public static WebElement ErrorMessage() {

		try{

			WebElement temp_element = webDriver.findElement(By.xpath(ErrorMessage_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Submit_btn() {

		WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
		return temp_element;
	}

	public static WebElement Cancel_btn() {

		WebElement temp_element = webDriver.findElement(By.xpath(Cancel_btn_xpath));
		return temp_element;
	}

	public static WebElement UserEmail_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(UserEmail_txt_xpath));
		return temp_element;
	}

	public static WebElement UserName_txt() {

		WebElement temp_element = webDriver.findElement(By.xpath(UserName_txt_xpath));
		return temp_element;
	}

	public static WebElement UserPassword() {

		WebElement temp_element = webDriver.findElement(By.xpath(UserPassword_xpath));
		return temp_element;
	}

	public static WebElement UserRole_cbbox() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(UserRole_cbbox_xpath));
		return temp_element;
	}


}