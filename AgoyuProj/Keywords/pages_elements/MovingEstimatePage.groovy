package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import functions.Common

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.Alert
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.util.List
import java.util.Arrays
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException
import pages_elements.Inventorymodal


public class MovingEstimatePage {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private static Step1_lbl_xpath="//div[@class = 'wizard-tabs-pills justify-content-lg-center']/a[1]";
	private static Step2_lbl_xpath="//div[@class = 'wizard-tabs-pills justify-content-lg-center']/a[2]";
	private static Step3_lbl_xpath="//div[@class = 'wizard-tabs-pills justify-content-lg-center']/a[3]";
	private static MoverList_xpath= "//div[@class = 'card card-mover-list']"
	private static MoverName_vlue_xpath= ".//div[@class ='card-title h6 mb-1']"
	private static MoveReviewNumb_vlue_xpath= ".//div[@data-original-title ='Number of reviewed moves']"
	private static ClientReviewNumb_vlue_xpath= ".//div[@data-original-title ='Number of client reviews']"
	private static MoverPrice_vlue_xpath= ".//div[@class ='col-lg-5 col-xl-3 p-4 mover-right']//div[@class ='h5']"
	private static MoverRate_numb_xpath= ".//svg"
	private static MoverSelect_btn_xpath= ".//button"

	private static AddressEdit_lnk_xpath= "//div[@class = 'sidebar-content-item'][2]//a[@data-action = 'edit']"
	private static AddressExpand_icon_xpath= "//div[@class = 'sidebar-content-item'][2]//i[@class ='fas fa-minus']"
	private static AddrFrom_txt_xpath= "//input[@id = ''moving_from']"
	private static AddrTo_txt_xpath= "//input[@id = ''moving_to']"
	private static MoveDate_txt_xpath= "//div[@class = 'sidebar-content-item'][2]//i[@class ='fas fa-minus']"
	private static MoveUpdate_btn_xpath= "//div[@class = 'sidebar-content-item'][2]//button"
	private static MoveCancel_btn_xpath= "//div[@class = 'sidebar-content-item'][2]//a[text() ='Cancel']"
	private static TotalMile_vlue_xpath= "//div[@class = 'sidebar-content-item'][2]//span[@class ='span-miles-content']"
	private static ExactWeight_opt_xpath= "//div[@class = 'sidebar-content-item'][3]//input[@id ='check-input-weight']"
	private static WeightEdit_lnk_xpath= "//div[@class = 'sidebar-content-item'][3]//a[@data-action = 'edit']"
	private static TotalWeight_vlue_xpath= "//div[@class = 'h6 text-custom']"
	private static WeightExpand_icon_xpath= "//div[@class = 'sidebar-content-item'][3]//a[@class ='title-control ml-auto']//i[@class ='fas fa-minus']"
	private static WeightUpdate_btn_xpath= "//div[@class = 'sidebar-content-item'][3]//button"
	private static WeightCancel_btn_xpath= "//div[@class = 'sidebar-content-item'][3]//a[text() ='Cancel']"
	private static AddrFrom_vlue_xpath= "//div[@id = 'moving-estimate-direction-map-from']"
	private static AddrTo_vlue_xpath= "//div[@id = 'moving-estimate-direction-map-to']"
	private static MoveDate_vlue_xpath= "//div[@id = 'move-date-view']"
	private static ExactWeight_txt_xpath= "//input[@id ='custom_weights']"
	private static roomlist = new Inventorymodal()



	private static FromAddrValidation_xpath = "//ul[@id ='validation_edit_from']"

	private static ToAddrValidation_xpath = "//ul[@id ='validation_edit_to']"

	private static WeightValidation_xpath = "//ul[contains(@id, 'validation_weight_edit') and @class='validation display']"

	private static MoveExpand_icon_xpath = "//div[@class ='sidebar-content-item'][2]//i[@class = 'fas fa-minus']"


	public MovingEstimatePage() {
	}

	//WebElement Declare

	public static WebElement MoveExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement WeightValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(WeightValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ToAddrValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ToAddrValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement FromAddrValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FromAddrValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AddressEdit_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddressEdit_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AddressExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddressExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AddrFrom_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddrFrom_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AddrTo_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddrTo_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoveDate_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveDate_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoveUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoveCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TotalMile_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TotalMile_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ExactWeight_opt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ExactWeight_opt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement WeightEdit_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(WeightEdit_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement TotalWeight_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(TotalWeight_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement WeightExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(WeightExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement WeightUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(WeightUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement WeightCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(WeightCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AddrFrom_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddrFrom_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement AddrTo_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AddrTo_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoveDate_vlue() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoveDate_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ExactWeight_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ExactWeight_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}




	public static List<WebElement> MoverList() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> temp_element = webDriver.findElements(By.xpath(MoverList_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverName_vlue(int mover_index) {
		try{


			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = MoverList().get(mover_index).findElement(By.xpath(MoverName_vlue_xpath))

			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoveReviewNumb_vlue(int mover_index) {
		try{

			WebElement temp_element = MoverList().get(mover_index).findElement(By.xpath(MoveReviewNumb_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement ClientReviewNumb_vlue(int mover_index) {
		try{
			WebElement temp_element = MoverList().get(mover_index).findElement(By.xpath(ClientReviewNumb_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverPrice_vlue(int mover_index) {
		try{
			WebElement temp_element = MoverList().get(mover_index).findElement(By.xpath(MoverPrice_vlue_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static List<WebElement> MoverRate_numb(int mover_index) {
		try{
			List<WebElement> temp_element = MoverList().get(mover_index).findElements(By.xpath(MoverRate_numb_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}

	public static WebElement MoverSelect_btn(int mover_index) {
		try{
			WebElement temp_element = MoverList().get(mover_index).findElement(By.xpath(MoverSelect_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e)
		{return null}
	}


	public static WebElement Step1_lbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Step1_lbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Step2_lbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Step2_lbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Step3_lbl() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Step3_lbl_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	//===METHOD
	public void TestValidationExactWeight_func(String input_str, String Expected_msg)
	{
		if(input_str.toString().contains("-"))
		{
			Common.ActionSendkey(this.ExactWeight_txt(), input_str)
			if(this.ExactWeight_txt.getText()==input_str)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Negative number should not allow to input."
			}
		}

		else
		{

			Common cm = new Common()
			Boolean status = this.ExactWeight_opt().isSelected()
			if(!status)
				cm.HandleElementClick(this.ExactWeight_opt())

			WebUI.delay(1)

			if(input_str!="")
			{
				this.ExactWeight_txt().clear()
				this.ExactWeight_txt().sendKeys(input_str)
			}


			this.WeightUpdate_btn().click()
			WebUI.delay(5)
		}


		if(Expected_msg!="")
		{
			if(this.WeightValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			String observed_msg = this.WeightValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[ExactWeight='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.WeightValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid Exact Weight."
			}
		}
	}

	public void TestValidationInventoryWeight_func(String input_str, String Expected_msg)
	{
		Inventorymodal modal = new Inventorymodal()

		if(Expected_msg!="")
		{
			if(modal.WeightValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = modal.WeightValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[InventoryWeight='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else if(modal.WeightValidation()!=null)

		{
			if(modal.WeightValidation().getText()!="")
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation msg'"+modal.WeightValidation().getText()+"' should not display when input valid Custom Weight."
			}
		}
	}

	public void TestValidationFrom_func(String input_str, String Expected_msg)
	{
		if(Expected_msg!="")
		{
			if(this.FromAddrValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			else
			{
				String observed_msg = this.FromAddrValidation().getText()
				println(observed_msg)

				if(observed_msg!=Expected_msg)
				{
					GlobalVariable.glb_TCStatus=false
					GlobalVariable.glb_TCFailedMessage +="[FromAddr='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

				}

			}
		}

		else
		{
			if(this.FromAddrValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid AddressFrom."
			}
		}
	}


	public void TestValidationTo_func(String input_str, String Expected_msg)
	{
		if(Expected_msg!="")
		{
			if(this.ToAddrValidation()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="To Address Validation message does not displays."
				return
			}

			String observed_msg = this.ToAddrValidation().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[ToAddr='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.ToAddrValidation()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="To Address Validation message should not display when input valid Email."
			}
		}
	}//end void

}