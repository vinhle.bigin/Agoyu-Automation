package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject

public class OutLook {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private  static String Username_xpath;
	private  static String Password_xpath;
	private  static String Signin_xpath;
	private  static String Signup_link_xpath;

	public OutLook() {
		webDriver = DriverFactory.getWebDriver();


		if(GlobalVariable.glb_SiteName=='Bec') {
			Username_xpath="//*[@name = 'login']";
			Password_xpath="//*[@name = 'password']";
			Signin_xpath="//button[@type='submit']";
			Signup_link_xpath = "//a[contains(text(),'Sign up')]"
		}

		if(GlobalVariable.glb_SiteName=='Vinestate'||GlobalVariable.glb_SiteName=='CLP') {
			Username_xpath="//*[@id = 'emailaddress']";
			Password_xpath="//*[@id = 'password']";
			Signin_xpath="//a[text()='Sign In']";
			Signup_link_xpath = "//a[contains(text(),'Sign up')]"
		}
	}


	//WebElement Declare


	public static WebElement Signup_link()
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Signup_link_xpath));
		return temp_element;

	}

	public static WebElement Username_txt()
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Username_xpath));
		return temp_element;

	}

	public static WebElement Password_txt()
	{

		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Password_xpath));
		return temp_element;

	}

	public static WebElement SignIn_btn()
	{
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Signin_xpath));
		return temp_element;
	}

}