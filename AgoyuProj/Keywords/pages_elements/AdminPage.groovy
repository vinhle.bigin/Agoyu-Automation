package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import functions.Common

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.Alert
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String

import com.kms.katalon.core.webui.exception.BrowserNotOpenedException
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException
import com.kms.katalon.core.webui.exception.BrowserNotOpenedException
import pages_elements.Usersetting_menu
import org.openqa.selenium.JavascriptExecutor
import org.openqa.selenium.WebDriver
import org.openqa.selenium.ElementClickInterceptedException

public class AdminPage {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private static Username_xpath="//*[@name = 'email']";
	private static Password_xpath="//*[@name = 'password']";
	private static Signin_xpath="//button[@id ='btn-login-agoyu']"
	private static Signup_link_xpath = "//form[@id ='form-login']//a[contains(@href,'register/consumer')]"
	private static ValidationEmail_msg_xpath	= "//ul[@data-field ='email']"
	private static PasswordValidation_xpath	= "//ul[@data-field ='password']"
	private static ForgotPass_lnk_xpath = "//a[contains(text(),'auth/reset')]"
	private static ResetEmail_txt_xpath = "//*[@name='email']"
	private static SendPassReset_btn_xpath = "//*[@type ='submit']"
	private static MoverSignin_lnk_xpath = "//a[contains(@href,'login-mover')]"
	private static FacebookLogin_btn_xpath = "//button[contains(@onclick,'facebook')]"
	private static GmailLogin_btn_xpath = "//button[contains(@onclick,'google')]"


	public AdminPage() {
	}

	//WebElement Declare
	public static WebElement GmailLogin_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GmailLogin_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement FacebookLogin_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(FacebookLogin_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ValidationEmail_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ValidationEmail_msg_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Signin_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(MoverSignin_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement SendPassReset_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(SendPassReset_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ResetEmail_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ResetEmail_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ForgotPass_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ForgotPass_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ValidationPass_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PasswordValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Signup_link()
	{
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Signup_link_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}


	}

	public static WebElement Username_txt()
	{
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Username_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}

	}

	public static WebElement Password_txt()
	{
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Password_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}

	}

	public static WebElement SignIn_btn()
	{
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Signin_xpath));
			return temp_element;
		}
		catch(Exception e) {
			return null
		}
	}

	public static void Acceptalert() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			Alert alert = webDriver.switchTo().alert();
			alert.accept();
		}
		catch (Exception e){
			return null
		}
	}


	//===============================Function================
	public static void TestValidationPass_func(String input_str, String Expected_msg)
	{
		this.Password_txt().clear()
		this.Password_txt().sendKeys(input_str)

		this.SignIn_btn().click()
		WebUI.delay(2)

		if(Expected_msg!="")
		{
			if(this.ValidationPass_msg()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
				return
			}

			String observed_msg = this.ValidationPass_msg().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Pass='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.ValidationPass_msg()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid Pass"
			}
		}
	}
	public static void TestValidationUsername_func(String input_str, String Expected_msg)
	{
		this.Username_txt().clear()
		this.Username_txt().sendKeys(input_str)

		this.SendPassReset_btn().click()
		WebUI.delay(1)

		if(Expected_msg!="")
		{
			if(this.ValidationEmail_msg()==null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message does not displays."
			}

			String observed_msg = this.ValidationEmail_msg().getText()

			if(observed_msg!=Expected_msg)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="[Email='"+input_str+"']. Observed: "+ observed_msg+"-Expected: "+Expected_msg+". "

			}
		}

		else
		{
			if(this.ValidationEmail_msg()!=null)
			{
				GlobalVariable.glb_TCStatus=false
				GlobalVariable.glb_TCFailedMessage +="Validation message should not display when input valid Email."
			}
		}
	}

}