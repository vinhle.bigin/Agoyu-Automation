package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException

public class LeftMenu {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private  static String AdminPage_mnuitem_xpath;
	private  static String ManageCoin_mnu_xpath;
	private  static String ManagePrice_mnu_xpath;
	private  static String ManageUser_mnu_xpath;
	private  static String PageTitle_xpath;
	private  static String PresalePage_mnu_xpath;
	private  static String UserPage_mnu_xpath;
	private  static username_lbl_xpath
	private  static email_lbl_xpath
	private  static logo_xpath
	private  static wallets_mnu_xpath
	private  static bitcoinwallet_mnuitem_xpath
	private  static tokenwallet_mnuitem_xpath
	private  static usdwallet_mnuitem_xpath
	private  static Bitcoin_ballance_vlue_xpath
	private  static Token_ballance_vlue_xpath
	private  static Usd_ballance_vlue_xpath
	private  static MyNetwork_mnu_xpath
	private  static Referrals_mnuitem_xpath
	private  static MyBonus_menu_xpath
	private  static DicrectBonus_mnuitem_xpath
	private  static Ethwallet_mnuitem_xpath
	private  static Users_mnu_xpath
	private static ETH_ballance_vlue_xpath
	private static Holding_ballance_vlue_xpath
	private static BuyPackage_mnu_xpath
	private static LeftBuyPackage_btn_xpath
	private static Report_mnu_xpath
	private static PackageReport_mnuitem_xpath
	private static BinaryBonus_mnuitem_xpath
	private static Holding_mnuitem_xpath
	private static BinaryTree_mnuitem_xpath
	private static Rankbonus_mnuitem_xpath
	private static AdminNews_xpath



	public LeftMenu() {
		webDriver = DriverFactory.getWebDriver()


		AdminPage_mnuitem_xpath="//span[contains(text(),'Admin Page')]";
	}



	public static WebElement AdminPage_mnuitem() {

		try{
			WebDriver webDriver = DriverFactory.getWebDriver();
			WebElement temp_element = webDriver.findElement(By.xpath(AdminPage_mnuitem_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}
}