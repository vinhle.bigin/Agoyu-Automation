package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.Alert
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException



public class ResetPasswordPage {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;

		private static Password_txt_xpath = "//*[@name = 'password']"
		private static PasswordValidation_xpath = "//span[@class ='help-block'][1]";
		private static ConfirmPassValidation_xpath="//span[@class ='help-block'][2]";
		private static ConfirmPass_txt_xpath="//*[@name = 'password_confirmation']";
		private static Submit_btn_xpath= "//button[@type = 'submit']"


	public ResetPasswordPage() {

	}

	//WebElement Declare

	public static WebElement Submit_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Submit_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ConfirmPassValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ConfirmPassValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ConfirmPass_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ConfirmPass_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}


	public static WebElement ValidationPass_msg() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PasswordValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	public static WebElement Password_txt()
	{
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Password_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}

	}

	//===============FUNCTION =====================================================
	public void ResetNewPass_func(String newpass_str)
	{
		this.Password_txt().clear()

		this.Password_txt().sendKeys(newpass_str)

		this.ConfirmPass_txt().clear()

		this.ConfirmPass_txt().sendKeys(newpass_str)

		this.Submit_btn().click()

		WebUI.delay(5)
		//WebUI.delay(10)

	}
	




}