package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.By

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ManageConsumers {
	private static WebDriver webDriver
	private static ManageConsumerTitle = "//h3[contains(text(),'Manage Agoyu Consumer')]"
	private static SearchContactName_xpath = "//input[@id='username']"
	private static SearchStatus_xpath = "//select[@name='status']"
	private static SearchType_xpath = "//select[@id='consumer_type']"
	private static Searchbtn_xpath = "//span[contains(text(),'Search')]"
	private static Clearbtn_xpath = "//span[contains(text(),'Clear')]"
	private static AddNewConsumer_xpath = "//span[contains(text(),'Add New Consumer')]"
	private static NewConsumer_xpath = "//h5[@class='modal-title']"
	private static NewTypeConsumer_xpath = "//select[@id='consumer-type']"
	private static NewBusinessName_xpath = "//input[@id='business_name']"
	private static NewBusinessContactName_xpath = "//input[@id='business_contact_name']"
	private static NewBusinessContactNumber_xpath = "//input[@id='business_contact_number']"
	private static NewEmailAddress_xpath = "//input[@id='company_email']"
	private static NewBusinessDescription_xpath = "//textarea[@id='business_description']"
	private static CreateBusinessbtn_xpath = "//button[@id='create-new-business']"
	private static CancelBusinessbtn_xpath = "//button[@id='cancel-create-new-business']"
	private static NewIndividualContactName_xpath = "//input[@id='contact_name']"
	private static NewIndividualContactNumber_xpath = " //input[@id='contact_number']"
	private static NewIndividualEmailAddress_xpath = "//input[@id='email_address']"
	private static CreateIndividualbtn_xpath = "//button[@id='create-new-personal']"
	private static CancelIndividualbtn_xpath = "//button[@id='cancel-create-new-personal']"
	private static CloseAddNewConsumer_xpath = "//span[@class='la la-close']"
	private static DataTableEmpty = "//td[contains(text(),'No data available in table')]"
	private static SearchResultName = "//tr[1]//td[1]"


	public static WebElement verifyManageConsumerTitle(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageAgoyuUserTitle = webDriver.findElement(By.xpath(ManageConsumerTitle))
			return ManageAgoyuUserTitle;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement checkDataTableEmpty(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement DataTableEmpty = webDriver.findElement(By.xpath(DataTableEmpty))
			return DataTableEmpty;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement searchContactName(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement SearchContactName = webDriver.findElement(By.xpath(SearchContactName_xpath))
			return SearchContactName;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement clickSearchbtn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement Searchbtn = webDriver.findElement(By.xpath(Searchbtn_xpath))
			return Searchbtn;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement SearchResultName(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement SearchResultName = webDriver.findElement(By.xpath(SearchResultName))
			return SearchResultName;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
}
