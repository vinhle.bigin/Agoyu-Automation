package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI
import functions.Common

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By
import org.openqa.selenium.JavascriptExecutor
import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject
import org.openqa.selenium.NoSuchElementException
import org.openqa.selenium.interactions.Actions
import org.openqa.selenium.support.ui.Select
import org.openqa.selenium.interactions.Action

public class ConsumerProfilePage {
	WebDriver webDriver;
	private static String AvatarUpload_btn_xpath= "//form[@id = 'form-personal']//div[@class ='btn-upload']"
	private static String ContactName_txt_xpath= "//input[@name = 'username']"
	private static String ContactEmail_txt_xpath= "//form[@id = 'form-personal']//input[@name = 'email']"
	private static String OldPass_txt_xpath= "//input[@name ='password']"
	private static String NewPass_txt_xpath= "//input[@name ='new_password']"
	private static String ContactPhone_txt_xpath= "//input[@name ='phone']"
	private static String ConfirmPass_txt_xpath= "//input[@name ='new_password_confirmation']"
	private static String GeneralEditGear_icon_xpath= "//div[@class ='panel-title'][1]//a[@data-action ='edit']"
	private static String PassEditGear_icon_xpath= "//div[@class ='panel eden-card'][2]//a[@data-action ='edit']"
	private static String GeneralUpdate_btn_xpath= "//form[@id = 'form-personal']//button[@data-control ='submit']"
	private static String GeneralCancel_btn_xpath= "//form[@id = 'form-personal']//button[@data-control ='cancel']"
	private static String PasswordUpdate_btn_xpath= "//form[@id = 'form-password']//button[@data-control ='submit']"
	private static String PassCancel_btn_xpath= "//form[@id = 'form-password']//button[@data-control ='cancel']"

	private static String LogoUpload_btn_xpath= "//form[@id = 'form-company']//div[@class ='btn-upload']"
	private static String BsinessName_txt_xpath= "//input[@name = 'business_name']"
	private static String BsinessPhone_txt_xpath= "//input[@name = 'contact_number']"
	private static String BsinessEmail_txt_xpath= "//input[@name = 'company_email']"
	private static String BsinessWeb_txt_xpath= "//input[@name = 'website']"
	private static String BsinessDescr_txt_xpath= "//textarea[@name ='description']"
	private static String Address_txt_xpath= "//input[@name = 'company_address']"
	private static String Apartment_txt_xpath= "//input[@name = 'company_apartment']"
	private static String City_txt_xpath= "//input[@name = 'company_city']"
	private static String State_txt_xpath= "//select[@name = 'company_state']"
	private static String Zip_txt_xpath= "//input[@name = 'company_zip']"

	private static String BsinessNameValidation_xpath= "//ul[@data-field ='business_name']"
	private static String BsinessMailValidation_xpath= "//ul[@data-field ='company_email']"
	private static String BsinessLogoValidation_xpath= "//form[@id = 'form-company']//ul[@data-field ='file']"
	private static String ContactNameValdation_xpath= "//ul[@data-field ='username']"
	private static String ContactMailValdation_xpath= "//ul[@data-field ='email']"
	private static String AvatarValidation_xpath= "//form[@id = 'form-personal']//ul[@data-field ='file']"
	private static String BsinessEdit_lnk_xpath= "//div[@id ='business-profile']//a[@data-action ='edit']"
	private static String BsinessUpdate_btn_xpath= "//form[@id = 'form-company']//button[@data-control ='submit']"
	private static String BsinessCancel_btn_xpath= "//form[@id = 'form-company']//button[@data-control ='cancel']"
	private static String OldPassValidation_xpath = "//ul[@data-field ='password']"
	private static String NewPassValidation_xpath = "//ul[@data-field ='new_password']"

	private static String UserProfile_Tab_xpath = "//a[@href = '#personal-profile']"
	private static String BsinessProfile_Tab_xpath = "//a[@href = '#business-profile']"

	private static String GeneralExpand_icon_xpath = "//div[@class ='panel-title'][1]//i[@class = 'fas fa-minus']"

	private static String PassExpand_icon_xpath = "//div[@class ='panel eden-card'][2]//i[@class = 'fas fa-minus']"

	private static String BsinessExpand_icon_xpath = "//div[@id ='business-profile']//i[@class = 'fas fa-minus']"

	private static String Booking_mnu_xpath = "//a[contains(@href, 'historical-booking')]"

	private static String Profile_mnu_xpath = "//ul[@class = 'sidebar-pane-item']//a[contains(@href, 'historical-booking')]"




	public ConsumerProfilePage() {
	}

	public static WebElement Profile_mnu() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Profile_mnu_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Booking_mnu() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Booking_mnu_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PassExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PassExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralExpand_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GeneralExpand_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement UserProfile_Tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(UserProfile_Tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessProfile_Tab() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessProfile_Tab_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement LogoUpload_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(LogoUpload_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewPassValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewPassValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement OldPassValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OldPassValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement AvatarUpload_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AvatarUpload_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContactName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactEmail_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContactEmail_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement OldPass_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(OldPass_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement NewPass_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(NewPass_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactPhone_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContactPhone_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ConfirmPass_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ConfirmPass_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralEditGear_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GeneralEditGear_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PassEditGear_icon() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PassEditGear_icon_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GeneralUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement GeneralCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(GeneralCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PasswordUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PasswordUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement PassCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(PassCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessName_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessName_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessPhone_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessPhone_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessEmail_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessEmail_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessWeb_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessWeb_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessDescr_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessDescr_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Address_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Address_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Apartment_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Apartment_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement City_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(City_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static Select State_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			Select temp_element = (Select) webDriver.findElement(By.xpath(State_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement Zip_txt() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(Zip_txt_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessNameValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessNameValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessMailValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessMailValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessLogoValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessLogoValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactNameValdation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContactNameValdation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement ContactMailValdation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(ContactMailValdation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement AvatarValidation() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(AvatarValidation_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessEdit_lnk() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessEdit_lnk_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessUpdate_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessUpdate_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}

	public static WebElement BsinessCancel_btn() {
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement temp_element = webDriver.findElement(By.xpath(BsinessCancel_btn_xpath));
			return temp_element;
		}
		catch(NoSuchElementException e) {
			return null
		}
	}



	////==============================METHOD
	//=======================
	//==================

	public static void TestValidationLogo_func(String input_str, String Expected_msg)
	{

		Common.UploadFile_func(this.LogoUpload_btn(), input_str)

		this.BsinessUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.BsinessLogoValidation()

		Common.VerifyFieldValidation_func("Logo", element, input_str, Expected_msg)

	}

	public static void TestValidationBsinessName_func(String input_str, String Expected_msg)
	{
		this.BsinessName_txt().clear()
		this.BsinessName_txt().sendKeys(input_str)

		this.BsinessUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.BsinessNameValidation()

		Common.VerifyFieldValidation_func("BsniessName", element, input_str, Expected_msg)

	}

	public static void TestValidationBsinessMail_func(String input_str, String Expected_msg)
	{

		this.BsinessEmail_txt().clear()
		this.BsinessEmail_txt().sendKeys(input_str)

		this.BsinessUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.BsinessMailValidation()

		Common.VerifyFieldValidation_func("BusinessMail", element, input_str, Expected_msg)

	}

	public static void TestValidationAvatar_func(String input_str, String Expected_msg)
	{

		Common.UploadFile_func(this.AvatarUpload_btn(), input_str)

		this.GeneralUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.AvatarValidation()

		Common.VerifyFieldValidation_func("Avatar", element, input_str, Expected_msg)

	}


	public static void TestValidationNewPass_func(String newpass_str,String confirmpass_str, String Expected_msg)
	{
		this.NewPass_txt().clear()
		this.NewPass_txt().sendKeys(newpass_str)

		this.ConfirmPass_txt().clear()
		this.ConfirmPass_txt().sendKeys(confirmpass_str)

		this.PasswordUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.NewPassValidation()

		Common.VerifyFieldValidation_func("NewPass", element, newpass_str, Expected_msg)

	}



	public static void TestValidationOldPass_func(String input_str, String Expected_msg)
	{
		this.OldPass_txt().clear()
		this.OldPass_txt().sendKeys(input_str)

		this.PasswordUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.OldPassValidation()

		Common.VerifyFieldValidation_func("OldPass", element, input_str, Expected_msg)

	}



	public static void TestValidationContactName_func(String input_str, String Expected_msg)
	{
		this.ContactName_txt().clear()
		this.ContactName_txt().sendKeys(input_str)

		this.GeneralUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.ContactNameValdation()

		Common.VerifyFieldValidation_func("ContactName", element, input_str, Expected_msg)

	}


	public static void TestValidationContactMail_func(String input_str, String Expected_msg)
	{

		this.ContactEmail_txt().clear()
		this.ContactEmail_txt().sendKeys(input_str)

		this.GeneralUpdate_btn().click()
		WebUI.delay(2)

		WebElement element = this.ContactMailValdation()

		Common.VerifyFieldValidation_func("ContactMail", element, input_str, Expected_msg)

	}


}
