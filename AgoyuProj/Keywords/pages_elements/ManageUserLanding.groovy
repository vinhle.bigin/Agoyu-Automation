package pages_elements
import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.checkpoint.CheckpointFactory
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testcase.TestCaseFactory
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testdata.TestDataFactory
import com.kms.katalon.core.testobject.ObjectRepository
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords
import internal.GlobalVariable
import MobileBuiltInKeywords as Mobile
import WSBuiltInKeywords as WS
import WebUiBuiltInKeywords as WebUI

import org.openqa.selenium.WebElement
import org.openqa.selenium.WebDriver
import org.openqa.selenium.By

import com.kms.katalon.core.mobile.keyword.internal.MobileDriverFactory
import com.kms.katalon.core.webui.driver.DriverFactory

import com.kms.katalon.core.testobject.RequestObject
import com.kms.katalon.core.testobject.ResponseObject
import com.kms.katalon.core.testobject.ConditionType
import com.kms.katalon.core.testobject.TestObjectProperty as ObjectProperty

import com.kms.katalon.core.mobile.helper.MobileElementCommonHelper
import com.kms.katalon.core.util.KeywordUtil
import java.lang.String
import com.kms.katalon.core.webui.exception.WebElementNotFoundException
import org.openqa.selenium.WebDriver as WebDriver
import org.openqa.selenium.WebElement as WebElement
import com.kms.katalon.core.webui.driver.DriverFactory as DriverFactory
import com.kms.katalon.core.util.KeywordUtil as KeywordUtil
import org.openqa.selenium.By as By
import java.lang.Math as Math
import com.kms.katalon.core.testobject.TestObject as TestObject

public class ManageUserLanding {
	/**
	 * Refresh browser
	 */
	private static WebDriver webDriver;
	private  static String CreateUser_btn_xpath;
	private  static String DeleteErrorMessage_xpath;
	private  static String Delete_btn_xpath;
	private  static String Edit_btn_xpath;
	private  static String Lock_unLock_btn_xpath;
	private  static String OppsOk_btn_xpath;
	private  static String Reset2FA_btn_xpath;
	private  static String SearchUserName_txt_xpath;
	private  static String UserList_tbl_xpath;

	public ManageUserLanding() {
		webDriver = DriverFactory.getWebDriver();

		if(GlobalVariable.glb_SiteName=='Bec') {
			CreateUser_btn_xpath  = "//a[@href='http://bec.bigin.top/users/create']";
			Delete_btn_xpath  = "//form[contains(@onsubmit,'delete')]/button";
			Edit_btn_xpath = "//form[contains(@onsubmit,'delete')]/button";
			Lock_unLock_btn_xpath = "//form[contains(@action,'lock')]/button";
			OppsOk_btn_xpath = "//form[contains(@action,'lock')]/button"
			Reset2FA_btn_xpath = "//button[contains(text(),'Reset 2FA')]";
			SearchUserName_txt_xpath = "//table[@id='tbl-manage-users']/thead[2]/tr[1]/th[2]/input"
			UserList_tbl_xpath = "//table[@id='tbl-manage-users']//tbody"
		}

		if(GlobalVariable.glb_SiteName=='Vinestate'||GlobalVariable.glb_SiteName=='CLP'
		||GlobalVariable.glb_SiteName=='WWR') {
			CreateUser_btn_xpath  = "//a[@href='http://bec.bigin.top/users/create']";
			DeleteErrorMessage_xpath  = "//div[contains(text(),'User does not exist')]";
			Delete_btn_xpath  = "//button[@id = 'btn-delete']";
			Edit_btn_xpath = "//a[@title = 'Edit']";
			Lock_unLock_btn_xpath = "//form[contains(@action,'lock')]/button";
			OppsOk_btn_xpath = "//button[@id = 'btn-lock']"
			Reset2FA_btn_xpath = "//button[contains(text(),'Reset 2FA')]";
			SearchUserName_txt_xpath = "//table[@id='tbl-manage-users']/thead[2]/tr[1]/th[2]/input"
			UserList_tbl_xpath = "//table[@id='manage-users-tbl']//tbody"
		}
	}

	public static WebElement CreateUser_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(CreateUser_btn_xpath));
		return temp_element;
	}


	public static WebElement Delete_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Delete_btn_xpath));
		return temp_element;
	}

	public static WebElement Edit_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Edit_btn_xpath));
		return temp_element;
	}

	public static WebElement Lock_unLock_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Lock_unLock_btn_xpath));
		return temp_element;
	}


	public static WebElement OppsOk_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(OppsOk_btn_xpath));
		return temp_element;
	}

	public static WebElement Reset2FA_btn() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(Reset2FA_btn_xpath));
		return temp_element;
	}

	public static WebElement SearchUserName_txt() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(SearchUserName_txt_xpath));
		return temp_element;
	}

	public static WebElement UserList_tbl() {
		WebDriver webDriver = DriverFactory.getWebDriver()
		WebElement temp_element = webDriver.findElement(By.xpath(UserList_tbl_xpath));
		return temp_element;
	}
}