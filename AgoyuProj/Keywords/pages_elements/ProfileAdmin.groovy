package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.By

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ProfileAdmin {
	
	private static ProfileTitle = "//h3[contains(text(),'My Profile')]"
	private static EditAdminInfoMode = "//span[contains(@onclick,'#form-agoyu-user-info')]"
	
	public static WebElement verifyProfileTile(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ProfileTitle = webDriver.findElement(By.xpath(ProfileTitle))
			return ProfileTitle;
		}
		catch(NoSuchElementException e){
			return null
		}
		
	}
	public static WebElement editAdminProfileMode(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement EditMode = webDriver.findElement(By.xpath(EditAdminInfoMode))
			return EditMode;
		}
		catch(NoSuchElementException e){
			return null
		}
		
	}
}
