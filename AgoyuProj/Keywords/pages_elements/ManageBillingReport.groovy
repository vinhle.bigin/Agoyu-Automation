package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ManageBillingReport {
	private static WebDriver webDriver
	private static SearchContactName_xpath = "//input[@id='search_contact']"
	private static SearchStatus_xpath = "//select[@id='search_status']"
	private static SearchSelectMover_xpath = "//select[@id='search_mover']"
	private static Searchbtn_xpath = "//span[contains(text(),'Search')]"
	private static Clearbtn_xpath = "//span[contains(text(),'Clear')]"
	private static AddNewConsumer_xpath = "//button[@id='mover-create-user-button']"
	private static NewMover_xpath = "//h5[@class='modal-title']"
	private static NewMoverName_xpath = "//input[@id='create-bussiness-name']"
	private static NewMoverContactName_xpath = "//input[@id='create-contact-name']"
	private static NewBusinessContactNumber_xpath = "//input[@id='business_contact_number']"
	private static CheckAgent_xpath = "//input[@id='check-company-1']"
	private static NewEmailAddress_xpath = "//input[@name='email']"
	private static NewEmailCompany_xpath = "//input[@name='email_company']"
	private static NewBusinessDescription_xpath = "//textarea[@id='create-description']"
	private static NewPhoneNumber_xpath = "//input[@id='create-phone-number']"
	private static YearCompanyFounded_xpath = "//input[@id='create-year']"
	private static SubmitMoverbtn_xpath = "//button[@id='create-sub-mover-next-button']"
	private static CancelMoverbtn_xpath = "//button[@id='create-sub-mover-cancel-button']"
	private static CloseAddNewMover_xpath = "//span[@class='la la-close']"
}
