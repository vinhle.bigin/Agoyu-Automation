package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.By

import com.google.common.util.concurrent.AbstractCatchingFuture.CatchingFuture
import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class LeftMenuAgoyuPortal {
	private static WebDriver webDriver
	private static NavLinkProfile = "//a[@class='nav-link']"
	private static ManageAccount = "//a[contains(@class,'hasAppend')]"
	private static ManageAgoyuUsers = "//a[contains(@class,'dropdown-item')]//span[contains(@class,'ks-icon la la-user')]"
	private static ManageConsumers = "//span[@class='ks-icon la la-briefcase']"
	private static ManageMovers = "//span[contains(@class,'ks-icon la la-truck')]"
	private static ManageBillingReport = "//span[contains(text(),'Manage Billing Report')]"
	private static ManageFAQs = "//span[contains(text(),'Manage FAQs')]"
	private static ManagePolices = "//span[contains(text(),'Manage Policies')]"


	public LeftMenuAgoyuPortal(){}

	public static WebElement goToProfile(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement NavLink = webDriver.findElement(By.xpath(NavLinkProfile))
			return NavLink;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManageAccount(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageAccount = webDriver.findElement(By.xpath(ManageAccount))
			return ManageAccount;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManageAgoyuUsers(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageAgoyuUsers = webDriver.findElement(By.xpath(ManageAgoyuUsers))
			return ManageAgoyuUsers;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManageConsumers(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageConsumers = webDriver.findElement(By.xpath(ManageConsumers))
			return ManageConsumers;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManageMovers(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageMovers = webDriver.findElement(By.xpath(ManageMovers))
			return ManageMovers;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManageBillingReport(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageConsumers = webDriver.findElement(By.xpath(ManageBillingReport))
			return ManageBillingReport;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManageFAQs(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManageFAQs = webDriver.findElement(By.xpath(ManageFAQs))
			return ManageFAQs;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement openManagePolices(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ManagePolices = webDriver.findElement(By.xpath(ManagePolices))
			return ManagePolices;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
}
