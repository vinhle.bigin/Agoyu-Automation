package pages_elements

import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject

import org.openqa.selenium.WebDriver
import org.openqa.selenium.WebElement
import org.openqa.selenium.By

import com.kms.katalon.core.annotation.Keyword
import com.kms.katalon.core.checkpoint.Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling
import com.kms.katalon.core.testcase.TestCase
import com.kms.katalon.core.testdata.TestData
import com.kms.katalon.core.testobject.TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.driver.DriverFactory
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI

import internal.GlobalVariable

public class ManageAgoyuUser {
	private static WebDriver webDriver
	private static ManageAgoyuUserTitle = "//h3[contains(text(),'Manage Agoyu User')]"
	private static SearchContactName_xpath = "//input[@id='username']"
	private static SearchStatus_xpath = "//select[@id='status']"
	private static SearchRole_xpath = "//fieldset[@class='form-group col-sm-3']//select[@id='role']"
	private static Searchbtn_xpath = "//button[@id='btn-search-standard']"
	private static SearchClearbtn_xpath = "//button[@id='btn-clear-standard']"
	private static AddNewUser_xpath = "//button[@id='mover-create-user-button']"
	private static DataTableEmpty = "//td[contains(text(),'No data available in table')]"
	private static SearchResultName = "//td[@class='sorting_1']//following-sibling::td[1]"
	private static ActiveStatusList ="//td[@class='sorting_1']"
	private static AllStatus = "//select[@id='status']//option[contains(text(),'All')]"
	private static AllRole = "//select[@id='role']//option[contains(text(),'Select role')]"
	private static ActiveStatus = "//select[@id='status']//option[contains(text(),'Active')]"
	private static filterRole = "//tr[1]//td[4]"
	private static filterStatus = "//tr[1]//span[@data-placement='top']"
	private static ActiveIcon = "//td[contains(text(),'em.tran@bigin.vn')]//following-sibling::td[3]//i[contains(@class,'la-check-circle')]"
	private static InactiveIcon = "//td[contains(text(),'em.tran@bigin.vn')]//following-sibling::td[3]//i[contains(@class,'la-minus-circle')]"
	private static ConfirmPopupStatus = "//div[@class='text-center']"
	private static ConfirmStatus = "//button[contains(text(),'Yes')]"
	private static CancelStatus = "//button[contains(text(),'Cancel')]"
	private static Active = "//td[contains(text(),'em.tran@bigin.vn')]//following-sibling::td[2]//span[contains(text(),'Active')]"
	private static Inactive = "//td[contains(text(),'em.tran@bigin.vn')]//following-sibling::td[2]//span[contains(text(),'Inactive')]"
	private static ConfirmPopupResetPassword = ""
	private static ConfirmResetPassword = "//button[contains(text(),'Yes')]"
	private static CancelResetPassword = "//button[contains(text(),'Cancel')]"
	private static ResetPasswordIcon = "//td[contains(text(),'em.tran@bigin.vn')]//following-sibling::td[3]//a[contains(@onclick,'resetPassword')]"


	public static WebElement verifyManageAgoyuUserTitle(){
		try{
			WebDriver webDriver =  DriverFactory.getWebDriver()
			WebElement ManageAgoyuUserTitle = webDriver.findElement(By.xpath(ManageAgoyuUserTitle))
			return ManageAgoyuUserTitle;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement checkDataTableEmpty(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement DataTableEmpty = webDriver.findElement(By.xpath(DataTableEmpty))
			return DataTableEmpty;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement searchContactName(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement SearchContactName = webDriver.findElement(By.xpath(SearchContactName_xpath))
			return SearchContactName;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement clickSearchbtn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement Searchbtn = webDriver.findElement(By.xpath(Searchbtn_xpath))
			return Searchbtn;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement clickClearbtn(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement Clearbtn = webDriver.findElement(By.xpath(SearchClearbtn_xpath))
			return Clearbtn;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement SearchResultName(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement SearchResultName = webDriver.findElement(By.xpath(SearchResultName))
			return SearchResultName;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement selectStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement selectStatus = webDriver.findElement(By.xpath(SearchStatus_xpath))
			return selectStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement activeStatusList(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			List<WebElement> activeStatusList = webDriver.findElements(By.xpath(ActiveStatusList))
			return activeStatusList;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement selectType(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement selectType = webDriver.findElement(By.xpath(SearchRole_xpath))
			return selectType;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement AllStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement AllStatus = webDriver.findElement(By.xpath(AllStatus))
			return AllStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement AllRole(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement AllRole = webDriver.findElement(By.xpath(AllRole))
			return AllRole;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement activeStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement activeStatus = webDriver.findElement(By.xpath(ActiveStatus))
			return activeStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement filterRole(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement filterRole = webDriver.findElement(By.xpath(filterRole))
			return filterRole;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement filterStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement filterStatus = webDriver.findElement(By.xpath(filterStatus))
			return filterStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement changeToInactive(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement changeToInactive = webDriver.findElement(By.xpath(InactiveIcon))
			return changeToInactive;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement changeToActive(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement changeToActive = webDriver.findElement(By.xpath(ActiveIcon))
			return changeToActive;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement confirmPopupStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ConfirmPopupStatus = webDriver.findElement(By.xpath(ConfirmPopupStatus))
			return ConfirmPopupStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement confirmStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ConfirmStatus = webDriver.findElement(By.xpath(ConfirmStatus))
			return ConfirmStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement cancelStatus(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement cancelStatus = webDriver.findElement(By.xpath(CancelStatus))
			return cancelStatus;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement Inactive(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement Inactive = webDriver.findElement(By.xpath(Inactive))
			return Inactive;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement Active(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement Active = webDriver.findElement(By.xpath(Active))
			return Active;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement ActiveIcon(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ActiveIcon = webDriver.findElement(By.xpath(ActiveIcon))
			return ActiveIcon;
		}
		catch(NoSuchElementException e){
			return null
		}
	}

	public static WebElement InactiveIcon(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement InactiveIcon = webDriver.findElement(By.xpath(InactiveIcon))
			return InactiveIcon;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
	
	public static WebElement ResetPasswordIcon(){
		try{
			WebDriver webDriver = DriverFactory.getWebDriver()
			WebElement ResetPasswordIcon = webDriver.findElement(By.xpath(ResetPasswordIcon))
			return ResetPasswordIcon;
		}
		catch(NoSuchElementException e){
			return null
		}
	}
}