package internal

import com.kms.katalon.core.configuration.RunConfiguration
import com.kms.katalon.core.testobject.ObjectRepository as ObjectRepository
import com.kms.katalon.core.testdata.TestDataFactory as TestDataFactory
import com.kms.katalon.core.testcase.TestCaseFactory as TestCaseFactory
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase

/**
 * This class is generated automatically by Katalon Studio and should not be modified or deleted.
 */
public class GlobalVariable {
     
    /**
     * <p></p>
     */
    public static Object glb_URL
     
    /**
     * <p></p>
     */
    public static Object glb_username
     
    /**
     * <p></p>
     */
    public static Object glb_password
     
    /**
     * <p></p>
     */
    public static Object glb_adminuser
     
    /**
     * <p></p>
     */
    public static Object glb_adminpassword
     
    /**
     * <p></p>
     */
    public static Object glb_usermail
     
    /**
     * <p></p>
     */
    public static Object glb_gmailPass
     
    /**
     * <p></p>
     */
    public static Object glb_sshhostname
     
    /**
     * <p></p>
     */
    public static Object glb_sshlogin
     
    /**
     * <p></p>
     */
    public static Object glb_sshpassword
     
    /**
     * <p></p>
     */
    public static Object glb_DbBHost
     
    /**
     * <p></p>
     */
    public static Object glb_DbUser
     
    /**
     * <p></p>
     */
    public static Object glb_DbPassword
     
    /**
     * <p></p>
     */
    public static Object glb_DbPort
     
    /**
     * <p></p>
     */
    public static Object glb_TCStatus
     
    /**
     * <p></p>
     */
    public static Object glb_TCFailedMessage
     
    /**
     * <p></p>
     */
    public static Object glb_Serverhomefolder
     
    /**
     * <p></p>
     */
    public static Object glb_dbName
     

    static {
        def allVariables = [:]        
        allVariables.put('default', [:])
        allVariables.put('AgoyuTest', allVariables['default'] + ['glb_URL' : 'https://agoyu-test.bigin.top', 'glb_username' : 'bigintesting@gmail.com', 'glb_password' : '12345678', 'glb_adminuser' : 'agoyu.admin@bigin.vn', 'glb_adminpassword' : '123456', 'glb_usermail' : 'bigintesting@gmail.com', 'glb_gmailPass' : 'Password@1234', 'glb_sshhostname' : '167.99.65.20', 'glb_sshlogin' : 'root', 'glb_sshpassword' : '375fa0ier394', 'glb_DbBHost' : '174.23.0.1', 'glb_DbUser' : 'eden_db', 'glb_DbPassword' : '987', 'glb_DbPort' : 5447, 'glb_TCStatus' : true, 'glb_TCFailedMessage' : '', 'glb_Serverhomefolder' : '', 'glb_dbName' : 'eden_db'])
        
        String profileName = RunConfiguration.getExecutionProfile()
        
        def selectedVariables = allVariables[profileName]
        glb_URL = selectedVariables['glb_URL']
        glb_username = selectedVariables['glb_username']
        glb_password = selectedVariables['glb_password']
        glb_adminuser = selectedVariables['glb_adminuser']
        glb_adminpassword = selectedVariables['glb_adminpassword']
        glb_usermail = selectedVariables['glb_usermail']
        glb_gmailPass = selectedVariables['glb_gmailPass']
        glb_sshhostname = selectedVariables['glb_sshhostname']
        glb_sshlogin = selectedVariables['glb_sshlogin']
        glb_sshpassword = selectedVariables['glb_sshpassword']
        glb_DbBHost = selectedVariables['glb_DbBHost']
        glb_DbUser = selectedVariables['glb_DbUser']
        glb_DbPassword = selectedVariables['glb_DbPassword']
        glb_DbPort = selectedVariables['glb_DbPort']
        glb_TCStatus = selectedVariables['glb_TCStatus']
        glb_TCFailedMessage = selectedVariables['glb_TCFailedMessage']
        glb_Serverhomefolder = selectedVariables['glb_Serverhomefolder']
        glb_dbName = selectedVariables['glb_dbName']
        
    }
}
